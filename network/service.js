import store from "../stores";
import { showAlert,closeAlert } from "react-native-customisable-alert";
import { setToken } from "../stores/actions";

export const ApiService = async (url,formdata,method) => {
    
    return new Promise(function(resolve, reject) {
            return fetch(url,{
            method: method,
            headers: {
                'Authorization': 'Bearer '+ store.getState().token,
                'accept': 'application/json',
                'content-type': 'multipart/form-data',
            },
            body: formdata
        })    
        .then((response) => response.json())
        .then((result) => {
            if(result.code == '402'){
                showAlert({
                    title: `Oops!`,
                    message: 'Session Expired',
                    alertType: 'error',
                    dismissable: false,
                    onPress: () => {
                        closeAlert();
                        store.dispatch(setToken(null)); 
                    }
                });
            }else{
                resolve(result);
            }
        })
        .catch((error) => {
            reject(error);
        })
    })
};


  

