import React, {useState,useEffect} from "react";
import { whiteTheme,blackTheme } from "../configs/theme";
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const AppContext = React.createContext(null)

export const AppContextProvider = ({children}) => {
    const [theme,changeTheme] = useState(whiteTheme)
    const [netPrinterConnected, setNetPrinterConnected] = useState(false);
    const [usbPrinterConnected, setUsbPrinterConnected] = useState(false);
    const [profileData, setProfileData] = useState();

    const context = {
        usbPrinterConnected,
        netPrinterConnected,
        profileData,
        theme,
        changeNetPrinterConnected: val => {
            setNetPrinterConnected(val);
        },
        changeUsbConnected: val => {
            setUsbPrinterConnected(val);
        },
        changeTheme: val => {
            changeTheme(val);
        },
        changeProfileData: val => {
            setProfileData(val);
        }
    };

    useEffect(() => {
        // AsyncStorage.getItem('THEME').then((value) => {
        //   if (value) {
        //     changeTheme(JSON.parse(value));
        //   }
        // });
      }, []);
      
    useEffect(() => {
        // AsyncStorage.setItem('THEME',  JSON.stringify(theme));
    }, [theme])

    return(
        <AppContext.Provider value={context}>
            <NavigationContainer theme={theme}>
                {children}
            </NavigationContainer>
        </AppContext.Provider>
    )
}   


