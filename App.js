import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { useFonts } from 'expo-font';
import Route from './screens/App/Route';
import { Provider } from 'react-redux';
import store from './stores';
import { AppContextProvider } from './context/AppContextProvider';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import en from './translations/en';
import my from './translations/my';
import * as SplashScreen from 'expo-splash-screen';
import CustomisableAlert, {showAlert} from 'react-native-customisable-alert';
import OfflineNotice from './screens/Components/OfflineNotice';

i18next.init({
  compatibilityJSON: 'v3',
  interpolation: { escapeValue: false },
  lng: 'my',
  resources: {
    en: {
      common: en,
    },
    my: {
      common: my,
    },
  },
});

export default function App() {
  const [fontsLoaded] = useFonts({
    'Lexend': require('./assets/fonts/Lexend.ttf'),
    'Lexend-Light': require('./assets/fonts/Lexend-Light.ttf'),
    'Lexend-Medium': require('./assets/fonts/Lexend-Medium.ttf'),
    'Lexend-Bold': require('./assets/fonts/Lexend-Bold.ttf'),
    'Pyidaungsu': require('./assets/fonts/Pyidaungsu.ttf'),
    'Pyidaungsu-Bold': require('./assets/fonts/Pyidaungsu-Bold.ttf'),
  });

  useEffect(() => {
    showAlert({
      title: `Oops!`,
      message: "Error",
      alertType: 'error',
      dismissable: true,
    });
  }, []);

  if (fontsLoaded) {
    setTimeout(() => {
      SplashScreen.hideAsync();
    }, 1000);
  }

  if (!fontsLoaded) {
    return null;
  }

  return (
    <I18nextProvider i18n={i18next}>
      <Provider store={store}>
        <AppContextProvider>
          <StatusBar style="dark" backgroundColor="white" />
          {/* <OfflineNotice /> */}
          <CustomisableAlert
            titleStyle={{ fontSize: 18, fontFamily: 'Lexend-Bold' }}
            textStyle={{ fontSize: 16, fontFamily: 'Pyidaungsu' }}
            btnLabelStyle={{ fontFamily: 'Lexend-Medium', color: '#2A318B' }}
            btnLeftStyle={{ fontFamily: 'Lexend-Medium', backgroundColor: '#fff' }}
            btnRightStyle={{ fontFamily: 'Lexend-Medium', backgroundColor: '#F7EE25' }}
            btnStyle={{ backgroundColor: '#F7EE25', borderRadius: 30 }}
          />
          <Route />
        </AppContextProvider>
      </Provider>
    </I18nextProvider>
  );
}
