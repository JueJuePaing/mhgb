import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function Menu(props) {
  return (
    <Svg height="23" width="23" viewBox="0 0 24 24" {...props}>
      <Path
        stroke="#000"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M11 17h8M5 12h14m-8-5h8"
      />
    </Svg>
  );
}