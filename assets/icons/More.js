import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function More(props) {
  return (
    <Svg height="20" width="20" viewBox="0 0 24 24" {...props}>
      <Path fill='#000' d="M8 12a2 2 0 1 1-2-2 2 2 0 0 1 2 2Zm10-2a2 2 0 1 0 2 2 2 2 0 0 0-2-2Zm-6 0a2 2 0 1 0 2 2 2 2 0 0 0-2-2Z" />
    </Svg>
  );
}