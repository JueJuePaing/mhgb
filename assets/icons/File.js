import * as React from 'react';
import Svg, { Path, G } from 'react-native-svg';

export default function File({color}) {
  return (
    <Svg height="20" width="20" viewBox="0 0 64 64">
     <G fill={color}>
      <Path d="M45 .586V14h13.414z" />
      <Path d="M44 16a1 1 0 0 1-1-1V0H7C4.789 0 3 1.789 3 4v56c0 2.211 1.789 4 4 4h48c2.211 0 4-1.789 4-4V16H44zm-30 2h16a1 1 0 1 1 0 2H14a1 1 0 1 1 0-2zm34 33H14a1 1 0 1 1 0-2h34a1 1 0 1 1 0 2zm0-6H14a1 1 0 1 1 0-2h34a1 1 0 1 1 0 2zm0-6H14a1 1 0 1 1 0-2h34a1 1 0 1 1 0 2zm0-6H14a1 1 0 1 1 0-2h34a1 1 0 1 1 0 2zm0-6H14a1 1 0 1 1 0-2h34a1 1 0 1 1 0 2z" />
    </G>
    </Svg>
  );
}