import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function AllProduct(props) {
  return (
    <Svg height="30" width="30" viewBox="0 0 24 24" {...props}>
        <Path
      stroke="#fff"
      strokeWidth={2}
      d="M4 7.414c0-.204 0-.306.038-.398.038-.092.11-.164.255-.309l1.535-1.535c.578-.578.868-.868 1.235-1.02C7.431 4 7.84 4 8.657 4h6.686c.818 0 1.226 0 1.594.152.367.152.656.442 1.235 1.02l1.535 1.535c.145.145.217.217.255.309.038.092.038.194.038.398V8H4v-.586Z"
    />
    <Path
      fill="#fff"
      fillRule="evenodd"
      d="M21 8H3v9c0 1.886 0 2.828.586 3.414C4.172 21 5.114 21 7 21h10c1.886 0 2.828 0 3.414-.586C21 19.828 21 18.886 21 17V8Zm-11 3a1 1 0 1 0-2 0 4 4 0 1 0 8 0 1 1 0 1 0-2 0 2 2 0 0 1-4 0Z"
      clipRule="evenodd"
    />
    </Svg>
  );
}