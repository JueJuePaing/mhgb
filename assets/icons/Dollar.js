import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function Dollar(props) {
  return (
    <Svg height="21" width="21" viewBox="0 0 24 24" {...props}>
      <Path
        fill="#e5804a"
        fillRule="evenodd"
        d="M6 11c-2.66 0-4-1.629-4-3.5C2 5.691 3.271 4 6 4v7Zm6 5.5c0 1.809-1.27 3.5-4 3.5v-7c2.661 0 4 1.629 4 3.5Zm2 0c0-2.768-2.025-5.5-6-5.5V4h2a2 2 0 0 1 2 2h2a4 4 0 0 0-4-4H8V0H6v2C2.042 2 0 4.722 0 7.5 0 10.268 2.025 13 6 13v7H4a2 2 0 0 1-2-2H0a4 4 0 0 0 4 4h2v2h2v-2c3.958 0 6-2.722 6-5.5Z"
      />
    </Svg>
  );
}