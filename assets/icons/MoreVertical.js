import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function MoreVertical(props) {
  return (
    <Svg height="20" width="20" viewBox="0 0 24 24" {...props}>
      <Path fill='#000' d="M12 16a2 2 0 1 1-2 2 2 2 0 0 1 2-2ZM10 6a2 2 0 1 0 2-2 2 2 0 0 0-2 2Zm0 6a2 2 0 1 0 2-2 2 2 0 0 0-2 2Z" />
  
    </Svg>
  );
}