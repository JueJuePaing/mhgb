import store from "../stores";
import { showAlert } from "react-native-customisable-alert";

export default function ErrorHandler(props){
    // 400
    if(props.code == 422){ 
        var object = props.errors;
        if(typeof object === 'object' && object != null){
            for (const [index,[key, value]] of Object.entries(Object.entries(object))) {
                if(index == 0){
                    return value.toString();
                }
            }
        }
        else{
            return props.message;
        }
    }
    else if(props.code == 403){

        showAlert({
            title: `Oops!`,
            message: `Session expired`,
            alertType: 'error',
            dismissable: true,
        });

        const setToken=()=> {
            return {
              type: 'BEARER_TOKEN',
              token: null
            }
        }
        store.dispatch(setToken())

    }
    // 401
    else if (props.message == 'Unauthenticated.') {
        return 'Unthorize for this action.';
    }
    // 500 
    else{
        return 'Internal Server Error.';
    }

    // invalid token
}