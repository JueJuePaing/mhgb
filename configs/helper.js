import accounting from 'accounting-js';

export const formatTranMoney = (amount, precision = 0) => {
    amount = parseFloat(amount.toString().replace(/,/g, '')) || 0;
    return accounting.formatMoney(amount, {
      symbol: '',
      precision: precision,
      thousand: ',',
      format: {
        pos: '%v',
        neg: '- %v',
        zero: '0',
      },
    });
};
