export const whiteTheme = {
    colors: {
      mainBackgroundColor: "#fff",
      mainBtnColor: "#2A318B",
      mainTextColor : "#090F47",
      secondaryTextColor : '#091C3F',
      mainLinkColor : '#2A318B',
      inputActiveColor : '#4157FF',
    },
    name:'default'
};
