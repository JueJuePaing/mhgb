import React from 'react';
import { Image, View, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';

const device = Dimensions.get('window');

CButton.defaultProps = {
  width: device.width - 30,
  height: 45,
  disabled: false,
};

export default function CButton(props) {
  return (
    <>
      <TouchableOpacity
        disabled={props.disabled}
        onPress={() => props.action()}
        style={[
          styles.button,
          {
            backgroundColor: props.backgroundColor,
            width: props.width,
            height: props.height,
            opacity: props.disabled ? 0.7 : 1,
          },
        ]}
      >
        <View style={styles.buttonContent}>
          {props.type === 'file' && (
            <Image
              source={require('../../assets/images/upload.png')}
              style={styles.uploadIcon}
            />
          )}
          <Text style={[styles.buttonText, { 
            color: props.color, 
            fontFamily: props.fontFamily,
            fontSize : props.txtSize ? props.txtSize : 15
          }]}>
            {props.title}
          </Text>
        </View>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    borderRadius: 30,
    width: '100%',
    height: '100%',
    padding: 10,
    shadowColor: '#4157FF',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.1,
    shadowRadius: 14,
    elevation: 2,
    marginBottom: 2
  },
  buttonContent: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  uploadIcon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    marginRight: 15,
  },
  buttonText: {
    fontSize: 15,
    textAlign: 'center',
  },
});
