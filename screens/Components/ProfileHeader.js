import {LinearGradient} from 'expo-linear-gradient';
import { Text, View, Image, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const colors = ['#F7EE25', '#A5A377'];
const borderRadius = 20;
const imageWidth = 60;
const imageHeight = 60;
const imageBorderRadius = imageWidth / 2;
const imageBorderColor = '#fff';
const imageBorderWidth = 2;
const headingText = 'Hi';
const bodyText = 'မိုးဟိန်းကမ္ဘာမှကြိုဆိုပါတယ်';
const fontFamily = {
  medium: 'Lexend-Medium',
  regular: 'Pyidaungsu',
};
const headingFontSize = 24;
const bodyFontSize = 16;
const textColor = '#fff';

const ProfileHeader = (props) => (
  <LinearGradient
    colors={colors}
    style={{
      zIndex: 1,
      height: 250,
      width,
      borderBottomLeftRadius: borderRadius,
      borderBottomRightRadius: borderRadius,
    }}>
    <View style={{ marginLeft: 25, marginRight: 25 }}>
      <View
        style={{
          top: 65,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Image
          source={require('../../assets/images/profile.jpeg')}
          style={{
            width: imageWidth,
            height: imageHeight,
            resizeMode: 'cover',
            borderRadius: imageBorderRadius,
            borderColor: imageBorderColor,
            borderWidth: imageBorderWidth,
          }}
        />
      </View>
      <View style={{ top: 140, position: 'absolute' }}>
        <Text
          style={{
            color: textColor,
            fontSize: headingFontSize,
            fontFamily: fontFamily.medium,
            paddingBottom: 10,
          }}>
          {headingText}, {props.name}
        </Text>
        <Text
          style={{
            color: textColor,
            fontSize: bodyFontSize,
            fontFamily: fontFamily.regular,
          }}>
          {bodyText}
        </Text>
      </View>
    </View>
  </LinearGradient>
);

export default ProfileHeader;
