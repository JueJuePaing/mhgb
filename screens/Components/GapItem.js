import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';

const device = Dimensions.get('window');
export default function GapItem() {
  return (
    <>
        <View style={{marginTop:15}}></View>
        <View style={styles.hairline} />
        <View style={{marginTop:15}}></View>
    </>
  );
}

const styles = StyleSheet.create({
    hairline: {
      backgroundColor: '#D4D4D4',
      height: 0.8,
      opacity: 0.4,
      width: device.width-30
    },
  });
