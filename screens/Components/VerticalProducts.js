import React from 'react';
import { FlatList, View, Text, TouchableOpacity, Dimensions } from 'react-native';
import { useTheme } from '@react-navigation/native';
import ImageLoad from 'react-native-image-placeholder';

const VerticalProducts = React.memo((props) => {

  const device = Dimensions.get('window');
  const { items, navigation, header, onEndReached, footer, onRefresh, refreshing } = props;
  const { colors } = useTheme();

  const renderItem = React.useCallback(({ item, index }) => {
    const isLastItem = index === items.length - 1;
    const isOddNumberOfItems = items.length % 2 !== 0;
    
    const itemStyle = [
      {
        flex: 1,
        backgroundColor: '#fff',
        marginLeft: 5,
        marginRight: isOddNumberOfItems && isLastItem ? (device.width-22) / 2  : 5,
        borderWidth: 1,
        borderRadius: 9,
        borderColor: '#F3F4F5',
        overflow: 'hidden',
        marginBottom: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.03,
        shadowRadius: 13,
        elevation: 0.3,

      },
      // isOddNumberOfItems && isLastItem && {
      //   marginTop: 15,
      // },
    ];
  
    return (
      <TouchableOpacity
        activeOpacity={.8}
        onPress={() =>
          navigation.navigate('ProductDetail', {
            productId: item.id,
          })
        }
        style={itemStyle}
      >
        <ImageLoad 
            source={{ uri: item.feature_image }} 
            style={{ width: '100%', height: 134 }} 
            resizeMode={'cover'}
            isShowActivity={false}
         />
        <View style={{ padding: 10 }}>
          <Text style={{ fontFamily: 'Lexend', fontSize: 12.5,color: colors.mainTextColor, marginBottom : 2 }}>{item.name}</Text>
          <Text style={{ fontFamily: 'Lexend-Medium', fontSize: 14, marginBottom : 6, color: '#f2970f' }}>{item.price} MMK</Text>
        </View>
      </TouchableOpacity>
    );
  }, [items, navigation, device.width]);

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={items}
      keyExtractor={(item, index) => index.toString()}
      numColumns={2}
      contentContainerStyle={{ paddingBottom: 20 }}
      ListHeaderComponent={header}
      ListFooterComponent={footer}
      renderItem={renderItem}
      onEndReached={onEndReached}
      onEndReachedThreshold={1}
      onRefresh={onRefresh}
      refreshing={refreshing}
    />
  );
});

export default VerticalProducts;
