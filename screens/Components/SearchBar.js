import React from 'react';
import { Image, View, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';

const {width} = Dimensions.get('window');

export default function SearchBar({placeholder, inputWidth, searchHandler}) {

  return (
    <TouchableOpacity onPress={searchHandler}>
        <View style={{ flexDirection: 'row', marginTop: 3 }}>
            <View style={[styles.searchInput, {
                width : inputWidth ?? width - 32
            }]}>
                <Text style={styles.placeholder}>
                    {placeholder ?? 'ဆေးထုတ်ကုန်များကို ရှာဖွေပါ။'}
                </Text>
            </View>
            <Image
                source={require('../../assets/images/search.png')}
                style={{
                    position: 'absolute',
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    top: 15,
                    marginLeft: 15,
                }}
                />
        </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
    searchInput: {
        justifyContent:'center',
        backgroundColor:'#fff',
        height: 50,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 40,
    },
    placeholder: {
        color: '#999',
        fontFamily:'Pyidaungsu'
    }
});
