import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Plus from '../../assets/icons/Plus';

export default function FooterTab() {

  return <View style={styles.shadowContainer}>
    <View style={styles.footer}>
        <TouchableOpacity 
            activeOpacity={1}
            style={styles.addBtnOuter}>
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.addBtn}>
                <Plus />
            </TouchableOpacity>
        </TouchableOpacity>
    </View>
  </View>
}

const styles = StyleSheet.create({
    shadowContainer: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'transparent',
        elevation: 8
      },
    footer: {
        height : 48,
        backgroundColor: '#fff',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,

        shadowColor: '#000',
        shadowOffset: {
          width: 5,
          height: 5
        },
        shadowOpacity: 1,
        shadowRadius: 6
    },
    addBtnOuter: {
        alignSelf: 'center',
        marginTop: -30,
        width: 59,
        height: 59,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F7FBFF',
        borderRadius: 40,
    },
    addBtn: {
        backgroundColor: '#323790',
        width: 53,
        height: 53,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,

        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        elevation: 4, 

    }
  });
