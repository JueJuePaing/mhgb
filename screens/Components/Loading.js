import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Platform } from 'react-native';

const Loading = ({message}) => {

    const isIos = Platform.OS === 'ios';

    return (
      <View style={styles.container}>
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#fff" size={isIos ? 'large' : 80} />
          <Text style={styles.message}>{message}</Text>
        </View>
      </View>
    );

}

export default Loading;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 11,
  },
  loading: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
    fontFamily: 'Lexend',
    color: '#fff',
    fontSize: 14,
    marginTop : 20
  }
});
