import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useTheme } from '@react-navigation/native';

const Categories = (props) => {
  const { categories, device, navigation } = props;
  const { colors } = useTheme();

  const colorCombinations = [  
    ['#FF9598', '#FF70A7'],
    ['#19E5A5', '#15BD92'],
    ['#FFC06F', '#FF793A'],
    ['#4DB7FF', '#3E7DFF'],
    ['#828282', '#090F47'],
    ['#FFCCE5', '#B56EFF'],
    ['#F5B7B1', '#E74C3C']
  ];

  return (
    <View>
      <ScrollView 
        showsHorizontalScrollIndicator={false}
        horizontal={true}
      >
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
          {categories.map((category, index) => {
            const [color1, color2] = colorCombinations[index % colorCombinations.length];
            return (
              <View 
                key={index}
                style={{
                  backgroundColor: '#fff',
                  marginLeft: 5,
                  marginRight: 5,
                  alignItems: 'center',
                  width: device.width / 5.5,
                  height: 90,
                  borderRadius: 80,
                  paddingTop: 10,
                  elevation: 0.2,
                  shadowColor: '#000',
                  shadowOffset: { width: 0, height: 6 },
                  shadowOpacity: 0.05,
                  shadowRadius: 23
                }}
              >
                <TouchableOpacity onPress={() => navigation.navigate('ProductListStack',{
                  categoryId : category.id,
                  brandId : null,
                  title : category.name
                })} style={{ maxWidth: 80 }}>
                  <LinearGradient 
                    colors={[color1, color2]} 
                    style={{
                      width: 44,
                      height: 44,
                      borderRadius: 22,
                      justifyContent: 'center',
                      alignItems: 'center',
                      alignSelf:'center'
                    }}
                  >
                    <Image 
                      source={{ uri : category.photo}} 
                      style={{width: 34, height: 34, borderRadius: 32}}
                      resizeMode='contain'
                    />
                  </LinearGradient>
                  <Text 
                    style={{
                      fontFamily: 'Lexend-Light', 
                      fontSize: 11, 
                      textAlign: 'center', 
                      marginTop: 5
                    }} 
                    numberOfLines={1} 
                    ellipsizeMode='tail'
                  >
                    {category.name}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Categories;
