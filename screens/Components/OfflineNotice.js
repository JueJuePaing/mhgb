import React,{ useState,useEffect } from 'react';
import { View, Text, Dimensions, StyleSheet, TouchableOpacity, Image } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {useTranslation} from "react-i18next";

const { height } = Dimensions.get('window');

function MiniOfflineSign() {

  const [t] = useTranslation('common');
  return (
    <View style={styles.offlineContainer}>
      <Image source={require('../../assets/images/no_internet.png')} 
            resizeMode={'contain'} 
            style={{width:125,height:125}}
          />
      <Text style={styles.offlineText}>{t('no_internet_connection')}</Text>
      <TouchableOpacity>
      <Text style={styles.tryOfflineText}>ထပ်ကြိုးစားပါ</Text>
      </TouchableOpacity>
    </View>
  );
}

const OfflineNotice = () => {

  const [isConnection, SetIsConnection] = useState(true);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
        handleConnectivityChange(state.isInternetReachable)
    });
    return unsubscribe;
  }, [])

  const handleConnectivityChange = isConnection => {
    SetIsConnection(isConnection);
  };

  return (
    <>
    { !isConnection ?
       <MiniOfflineSign />
      :
      null
    }
    </>
  )
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor:'#fff',
    height:height + getStatusBarHeight(),
    alignItems:'center',
    justifyContent:'center',
  },
  offlineText: { 
    marginTop:50,
    fontFamily: 'Pyidaungsu',
    fontSize : 16
  },
  tryOfflineText: {
    marginTop:10,
    fontFamily: 'Pyidaungsu',
    fontSize : 16,
    color: '#1987FB'
  }
});

export default OfflineNotice;