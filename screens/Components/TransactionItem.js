import React from 'react';
import { useTheme } from '@react-navigation/native';
import { Image, View, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';

import File from '../../assets/icons/File';

const {width, height} = Dimensions.get('window');

export default function TransactionItem({item, index, latest, color, itemHandler}) {
  const { colors } = useTheme();
  return (
      <TouchableOpacity
        onPress={itemHandler}
        style={[
          styles.item,
          {
            backgroundColor: color ? color : '#F7FBFF',
            marginBottom : latest ? 15 : 0
          }
        ]}
      >
        <View style={[styles.iconContainer, {
            backgroundColor: '#dfecf8'
          }]}>
          <File color={'#3669a0'} />
        </View>
        <View style={styles.middleContent}>
          <Text style={styles.name}>
            {`Inv - ${item.invoice_no ? item.invoice_no : index+1}`}
          </Text>
          <Text style={styles.description}>
            {`${item.product_count ? item.product_count : item.products?.length} Products`}
          </Text>
        </View>
        {/* <MoreVertical /> */}
      </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item: {
    width: width * 0.92,
    borderRadius: 10,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 0.2,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 6 },
    shadowOpacity: 0.05,
    shadowRadius: 23,
    borderWidth: .7,
    borderColor: 'lightgray',
    marginTop: 15,
    justifyContent: 'space-between'
  },
  iconContainer: {
    width: width * 0.17,
    alignItems: 'center',
    justifyContent: 'center',
    height: width * 0.17,
    borderRadius: 15,
    marginRight: width * 0.04
  },
  middleContent: {
    width: width * 0.6
  },
  name: {
    fontFamily: 'Lexend-Medium',
    color: '#000',
    fontSize: 15
  },
  description: {
    fontFamily: 'Lexend-Light',
    color: '#4c545d',
    fontSize: 13,
    paddingTop: 4
  }
});
