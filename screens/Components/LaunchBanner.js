import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from "react-native"
import { useTheme } from '@react-navigation/native';

const device = Dimensions.get('window');

export default function LaunchBanner(props) {
  const { colors } = useTheme();
  return (
    <>
        <Image source={props.imgUrl} style={styles.image}/>
        <Text style={[styles.textHeader,{color:colors.mainTextColor}]}>{props.title}</Text>
        <Text style={[styles.textBody]}>{props.body}</Text>
    </>
  );
}

const styles = StyleSheet.create({
    image: {
      width: device.width,
      height: 250,
      resizeMode: 'contain',
      alignSelf: 'center'
    },
    textHeader: {
      fontSize: 18,
      paddingTop: 40,
      textAlign:'center',
      fontFamily: 'Pyidaungsu',
      lineHeight: 32
    },
    textBody: {
      color: "gray",
      fontSize: 15,
      textAlign:'center',
      paddingTop: 16,
      fontFamily: 'Lexend-Light',
      lineHeight: 24
    }
})