import React from 'react';
import { ScrollView, View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { useTheme } from '@react-navigation/native';
import ImageLoad from 'react-native-image-placeholder';
import {
  Placeholder,
  PlaceholderMedia,
  Fade,
} from "rn-placeholder";

const BrandList = (props) => {
  const { brands, device, placeHolderLoading } = props;
  const { colors } = useTheme();

  return (
    <ScrollView showsHorizontalScrollIndicator={false} horizontal>
      <View style={styles.brandListContainer}>

      {
        placeHolderLoading ?
        <Placeholder Animation={Fade} Left={props => (
          <View style={{ flexDirection: 'row' }}>
                <PlaceholderMedia
                    isRound={true}
                    style={[{ height: 80, width: 80, borderRadius:40 }, props.style]}
                />
                <View style={{ width: 10 }} />
                <PlaceholderMedia
                    isRound={true}
                    style={[{ height: 80, width: 80, borderRadius:40 }, props.style]}
                />
            </View>
        )}>
        </Placeholder>
        :
        <>
        {brands.map((brand, index) => (
          <TouchableOpacity
            key={index}
            style={[styles.brandContainer, { width: device.width / 4 }]}
            onPress={() => props.navigation.navigate('ProductListStack', {
              categoryId: null,
              brandId: brand.id,
              title: brand.name
            })}
          >
            <View style={styles.imageContainer}>
              <ImageLoad
                style={styles.image}
                source={{ uri: brand.photo }}
                isShowActivity={false}
                resizeMode={'contain'}
              />
            </View>
            <Text style={[styles.brandName, { color: colors.mainTextColor }]}>
              {brand.name}
            </Text>
          </TouchableOpacity>
        ))}
        </>
        }
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  brandListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  brandContainer: {
    marginHorizontal: 5,
    marginBottom: 10,
  },
  imageContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    overflow: 'hidden',
    borderWidth:1,
    borderColor: 'rgba(200, 200, 200, 0.5)',
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  brandName: {
    alignSelf: 'center',
    fontSize: 12.5,
    fontFamily: 'Lexend',
    marginTop: 5,
  },
});

export default BrandList;
