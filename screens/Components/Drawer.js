import React from 'react';
import { useTheme } from '@react-navigation/native';
import { Modal, View, Image, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';
import { MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';

import Transaction from '../../assets/icons/Transaction';

const {width, height} = Dimensions.get('window');

export default function Drawer({onClose, menuHandler}) {

    const { colors } = useTheme();

    const MENUS = [
        {
            name: 'Home',
            icon: <Feather color={'#2b3186'} name='home' size={18} />
        },
        // {
        //     name: 'Order',
        //     icon: <Feather color={'#2b3186'} name='shopping-cart' size={18} />
        // },
        {
            name: 'Low Stocks',
            icon: <AntDesign color={'#2b3186'} name='barschart' size={18} />
        },
        {
            name: 'All Transaction',
            icon: <Transaction color='#2b3186' size={20} />
        },
        {
            name: 'All Products',
            icon: <AntDesign color={'#2b3186'} name='menu-fold' size={18} />
        },
        {
            name: 'Setting',
            icon: <Feather color={'#2b3186'} name='more-vertical' size={18} />
        }
    ]

  return (
    <Modal
        visible={true}
        onRequestClose={onClose}
        animationType="slideFromLeft"
        transparent={true}>
        <View style={styles.modalBackdrop}>
            <View style={styles.modalContent}>
                <View style={styles.modalBody}>
                    <View style={styles.headerContent}>
                        <Image 
                            source={require('../../assets/images/default_user.png')}
                            style={styles.profile}/>
                        <Text style={styles.name}>Pharmacy Name</Text>
                        <Text style={styles.phone}>+959 793385339</Text>
                    </View>

                    <View style={styles.separator} />

                    {MENUS.map((menu, index) => {
                        return <TouchableOpacity
                            key={index}
                            activeOpacity={0.8}
                            style={styles.menu}
                            onPress={() => menuHandler(menu)}>
                            {menu.icon}
                            <Text style={styles.menuName}>{ menu.name }</Text>
                        </TouchableOpacity>
                    })}

                    <View style={styles.footerContent}>
                        <View style={styles.separator} />
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.menu}
                            onPress={() => menuHandler('policy')}>
                            <Feather
                                name='share-2'
                                size={18}
                                color='#4aa35a' />
                            <Text style={styles.menuName}>Privacy Policy</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.menu}
                            onPress={() => menuHandler('contactus')}>
                            <MaterialIcons
                                name='chat'
                                size={18}
                                color='#4aa35a' />
                            <Text style={styles.menuName}>Contact Us</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
    modalBackdrop: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    modalContent: {
        backgroundColor: '#fff',
        width: '80%',
        height: '100%'
    },
    modalBody: {
        height: '100%',
        width: '100%'
    },
    headerContent: {
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.25
    },
    profile : {
        width: height * 0.09,
        height: height * 0.09,
        resizeMode:'cover',
        borderRadius : height * 0.1,
        marginBottom: 15
    },
    name: {
        fontSize: 15,
        fontFamily: 'Lexend',
        color : '#000',
        paddingBottom: 3
    },
    phone: {
        fontSize: 13,
        fontFamily: 'Lexend-Light',
        color : 'gray',
    },
    separator: {
        width: '100%',
        height: .8,
        backgroundColor: 'lightgray',
        marginBottom: 10
    },
    menu: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 15,
        paddingVertical: 13
    },
    menuName: {
        fontFamily: 'Lexend-Light',
        fontSize: 14,
        color: '#7f7c7c',
        paddingLeft: 15
    },
    footerContent: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        paddingBottom: 10
    }
});