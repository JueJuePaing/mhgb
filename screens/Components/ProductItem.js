import React from 'react';
import { View, Text, Image, Dimensions,
  TouchableOpacity } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { formatTranMoney } from '../../configs/helper';
import GapItem from './GapItem';
import { Ionicons } from '@expo/vector-icons';

const device = Dimensions.get('window');
const secondColumnWidth = device.width - 170;

const ProductItem = ({item, selected, requestAddCart}) => {
    
  const { colors } = useTheme();

    return (
      <View key={item.id}>
        <View style={{flexDirection:'row',justifyContent:'space-between', paddingHorizontal: 20, alignItems: 'center'}}>
            <TouchableOpacity 
              style={{paddingRight:6, marginRight : 5}}
              onPress={() => !selected ? requestAddCart() : null}>
                <Image 
                  source={item.feature_image ? {uri : item.feature_image} : require('../../assets/images/product6.png')}
                  style={{width:50,height:50,borderRadius:12}}
                  resizeMode={'contain'}
                  />
                {!selected && <TouchableOpacity
                  activeOpacity={0.8}
                  style={{
                    position : 'absolute',
                    bottom : -5,
                    right : -5
                  }}
                  onPress={() => requestAddCart()}>
                  <Ionicons
                    name='add-circle-sharp'
                    size={25}
                    color='#2A318B' />
                </TouchableOpacity>}
            </TouchableOpacity>
            <TouchableOpacity 
              activeOpacity={0.8}
              style={{width:secondColumnWidth, paddingLeft: 9}}
              onPress={() => requestAddCart()}>
              <Text style={{fontFamily:'Lexend',fontSize:13,color:colors.mainTextColor}}>
                  {item.name}
              </Text>
              <Text style={{fontFamily:'Lexend-Light',fontSize:12,color:'gray'}}>
                  {item.remain_quantity ? item.remain_quantity : 0} - {item.size}
              </Text>
            </TouchableOpacity>

            <View >
              <Text style={{fontFamily:'Lexend-Medium',fontSize:13,color:colors.mainTextColor, paddingLeft: 8}}>
                {formatTranMoney(item.sale_price ? item.sale_price : 0)} MMK
              </Text>  
            
            </View>
          </View>
        <GapItem/>
    </View>
  );
};

export default ProductItem;
