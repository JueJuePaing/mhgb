import React from 'react';
import { TouchableOpacity, Image } from 'react-native';

function PasswordShowHide({ secureTextEntry, toggleSecureTextEntry, tintColor }) {
  return (
    <TouchableOpacity
      onPress={toggleSecureTextEntry}
      style={{ position: 'absolute', right: 10 }}
    >
      <Image
        source={
          secureTextEntry
            ? require('../../assets/images/eye-off.png')
            : require('../../assets/images/eye.png')
        }
        tintColor={tintColor}
        style={{ width: 23, height: 23, right: 5, marginTop: 5 }}
      />
    </TouchableOpacity>
  );
}

export default PasswordShowHide;
