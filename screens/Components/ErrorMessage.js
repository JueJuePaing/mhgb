import React from 'react';
import { Text } from 'react-native';

const ErrorMessage = ({ message }) => {
  return (
    <Text style={{ color: 'red', fontFamily: 'Lexend', fontSize: 12 }}>
      {message}
    </Text>
  );
};

export default ErrorMessage;
