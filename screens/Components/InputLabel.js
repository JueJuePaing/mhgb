import React from 'react';
import { Text} from 'react-native';
import { useTheme } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';

export default function InputLabel (props) {
  const { colors } = useTheme();

  return (
    <>
    <Animatable.Text animation="fadeIn">
        <Text style={{
            color:'gray',
            fontFamily:'Pyidaungsu',
            fontSize:14,
            alignSelf:'flex-start',
          }}>
            {props.title}
        </Text>
    </Animatable.Text>
    </>
  );
}


