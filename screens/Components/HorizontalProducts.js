import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { useTheme } from '@react-navigation/native';
import ImageLoad from 'react-native-image-placeholder';
import {
  Placeholder,
  PlaceholderMedia,
  Fade,
} from "rn-placeholder";

const HorizontalProducts = (props) => {
  const { products, device, placeHolderLoading } = props;

  const { colors } = useTheme();

  return (
    <ScrollView 
      showsHorizontalScrollIndicator={false}
      horizontal={true}
    >
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>

      {
        placeHolderLoading ?
        <Placeholder Animation={Fade} Left={props => (
          <View style={{ flexDirection: 'row' }}>
                <PlaceholderMedia
                    isRound={true}
                    style={[{ height: 200, width: device.width / 2.3 }, props.style]}
                />
                <View style={{ width: 10 }} />
                <PlaceholderMedia
                    isRound={true}
                    style={[{ height: 200, width: device.width / 2.3 }, props.style]}
                />
            </View>
        )}>
        </Placeholder>
        :
        <>
        {products.map((product, index) => {
          return (
            <TouchableOpacity 
              onPress={() => props.navigation.navigate('ProductDetail',{
                productId : product.id
              })}
              key={index}
              style={{
                width: device.width / 2.3,
                height: 200,
                backgroundColor: '#fff',
               // marginLeft: 5,
                marginRight: 10,
                borderWidth: 1,
                borderRadius: 9,
                borderColor: '#F3F4F5',
                overflow: 'hidden',
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 3 },
                shadowOpacity: 0.03,
                shadowRadius: 13,
                elevation: 0.3,

              }}
            >
              <ImageLoad 
                source={{ uri: product.feature_image }}
                style={{width: '100%', height: 134}}
                isShowActivity={false}
                resizeMode={'cover'}
              />
              <View style={{padding: 10}}>
                <Text numberOfLines={1} style={{fontFamily: 'Lexend', marginBottom : 1, fontSize: 12.5, color: colors.mainTextColor}}>
                  {product.name}
                </Text>
                <Text style={{fontFamily: 'Lexend-Medium', marginBottom : 6, fontSize: 14, color:'#f2970f'}}>
                  {product.price} MMK
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
        </>
        }
      </View>
    </ScrollView>
  );
};

export default HorizontalProducts;
