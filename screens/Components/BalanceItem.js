import React from 'react';
import { Image, View, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';
import {AntDesign} from '@expo/vector-icons';
import { formatTranMoney } from '../../configs/helper';
import { useTheme } from '@react-navigation/native';

const width = Dimensions.get('window').width;

export default function BalanceItem({
    product,
    itemHandler
}) {

    const { colors } = useTheme();

  return (
    <TouchableOpacity
        activeOpacity={0.8}
        onPress={ () => itemHandler(product) }
        style={styles.item}>
        {product.feature_image ?
            <Image
                source={{uri : product.feature_image}}
                style={styles.medicineImg} /> :
            <View style={styles.profileBg}>
                <Text style={styles.profileTxt}>M</Text>
            </View>
        }
        
        <View style={styles.infoContent}>
            <Text style={styles.name}>{product.name}</Text>
            <Text style={styles.pid}>{`PID - ${product.id}`}</Text>
            <View style={styles.row}>
                <AntDesign
                    name="caretup"
                    size={13}
                    color='#72c87b' />
                <Text style={[styles.number, {color: '#72c87b', marginRight: 8}]}>
                    {product.total_quantity ? product.total_quantity : 0}
                </Text>
                <AntDesign
                    name="caretdown"
                    size={13}
                    color='#e05b33' />
                    <Text style={[styles.number, {color: '#e05b33', marginRight: 3}]}>
                    {product.used_quantity ? product.used_quantity : 0}
                    </Text>
                <Text style={styles.number2}>
                    {` = ${product.remain_quantity ? product.remain_quantity : 0}`}
                </Text>
            </View>
            <Text style={styles.number3}>
                {`${product.remain_quantity ? product.remain_quantity : 0} x ${product.sale_price ? product.sale_price : 0} = ${product.remain_quantity && product.sale_price ? formatTranMoney(product.remain_quantity * product.sale_price) : 0} MMK`}
            </Text>
        </View>

        <Image 
            source={require('../../assets/images/right-arrow.png')} 
            style={{width:22,height:22}}
            tintColor={colors.mainTextColor}/>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
    item: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 25,
        borderBottomWidth: .6,
        borderBottomColor: 'lightgray'
    },
    profileBg: {
        width: width * 0.12,
        height: width * 0.12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f7d0b9',
        borderRadius: 50,
        marginRight: 10
    },
    medicineImg: {
        width: width * 0.12,
        height: width * 0.12,
    },
    profileTxt:{
        color: '#f3a843',
        fontFamily: 'Lexend-Medium',
        fontSize: 20
    },
    infoContent: {
        width: width * 0.67
    },
    name: {
        fontSize: 15,
        fontFamily: 'Lexend',
        color : '#000',
        paddingBottom: 1
    },
    pid: {
        fontSize: 12,
        fontFamily: 'Lexend',
        color : 'gray',
        paddingBottom: 1.5
    },
    row:  {
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingBottom: 1.5,
        paddingTop: 1.5
    },
    number: {
        fontSize: 12,
        fontFamily: 'Lexend-Light',
        paddingLeft: 2
    },
    number2: {
        fontSize: 12,
        fontFamily: 'Lexend-Light',
        color:'darkgray'
    },
    number3: {
        fontSize: 12,
        fontFamily: 'Lexend-Light',
        color:'darkgray',
        paddingTop: 1.5
    },
});

