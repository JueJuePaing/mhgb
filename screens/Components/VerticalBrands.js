import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, StyleSheet } from 'react-native';

const VerticalBrands = (props) => {
  const { items, navigation, header, onEndReached, footer } = props;

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() =>
          navigation.navigate('ProductListStack', {
            categoryId: null,
            brandId: item.id,
            title: item.name
          })
        }
      >
        <Image
          source={{ uri: item.photo }}
          style={styles.itemImage}
          isShowActivity={false}
          resizeMode={'contain'}
        />
        <Text style={styles.itemTitle} numberOfLines={2}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  const keyExtractor = (item, index) => index.toString();

  return (
    <View style={styles.container}>
      <FlatList
        numColumns={3}
        data={items}
        keyExtractor={keyExtractor}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
        ListHeaderComponent={header}
        ListFooterComponent={footer}
        renderItem={renderItem}
        onEndReached={onEndReached}
        onEndReachedThreshold={1}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
  },
  contentContainer: {
    paddingBottom: 10,
  },
  itemContainer: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 20,
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  itemTitle: {
    fontFamily: 'Lexend-Light',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 10,
  },
});

export default VerticalBrands;
