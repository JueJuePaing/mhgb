import React from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { Dimensions } from 'react-native';

export default function DropDown(props) {

    const device = Dimensions.get('window');

  return (
    <>
        <DropDownPicker
          searchable={props.searchable}
                open={props.open}
                value={props.value}
                items={props.items}
                setOpen={props.setOpen}
                setValue={props.setValue}
                setItems={props.setItems}
                labelStyle={{color:'#000'}}
                placeholder={'တစ်ခုခုရွေးချယ်ပေးပါ'}
                placeholderStyle={{color:'gray'}}
                textStyle={{ fontFamily: "Pyidaungsu",fontSize:14,color: '#000' }}
                style={{
                    borderBottomWidth: 1,
                    borderTopWidth: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    borderColor: 'rgba(9, 15, 71, 0.1)',
                    borderRadius:8,
                    fontSize:15,
                    zIndex:props.zIndex
                }}
                listMode="SCROLLVIEW" 
                onSelectItem={(item) => props.onSelectItem(item)}
            />
    </>
  );
}
