import React from 'react';
import { Text } from 'react-native';
import {useTranslation} from "react-i18next";

const VerificationTimer = ({ minutes, seconds }) => {

  const [t] = useTranslation('common');
  
  function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
  }
  return (
    <Text style={styles.secondText}>
      {seconds !== 0 && `${t('otp_expire_text1')} ${pad(minutes)}:${pad(seconds)} ${t('otp_expire_text2')}`}
    </Text>
  );
};

const styles = {
  secondText: {
    fontFamily:'Pyidaungsu',
    fontSize:14,
    color:'gray',
    paddingTop:25,
    alignSelf:'center'
  }
};

export default VerificationTimer;
