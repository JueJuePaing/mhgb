import React from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { formatTranMoney } from '../../configs/helper';
import IndeButton from './IndeButton';
import GapItem from './GapItem';

const device = Dimensions.get('window');
const secondColumnWidth = device.width - 130;

const CartItem = ({
    item,
    increment,
    decrement,
    updateQty,
    removeCartHandler
}) => {
    
    const { colors } = useTheme();

    return (
      <View key={item.id}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View style={{paddingRight:5}}>
                <Image 
                    source={item.feature_image ? {uri: item.feature_image} : require('../../assets/images/product6.png')} 
                    style={{width:70,height:70,borderRadius:12}}
                    resizeMode={'contain'}
                />
            </View>
            <View style={{width:secondColumnWidth}}>
                <View style={{
                        flexDirection:'row',
                        justifyContent:'space-between'
                    }}>
                    <View style={{width:secondColumnWidth/1.3}}>
                        <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                            {item.name}
                        </Text>
                        <Text style={{fontFamily:'Lexend-Light',fontSize:13,color:'gray'}}>
                            {item.remain_quantity} - {item.size}
                        </Text>
                    </View>
                    <TouchableOpacity onPress={() => removeCartHandler(item) }>
                        <Image 
                            source={require('../../assets/images/close.png')} 
                            style={{width:22,height:22,resizeMode:'contain',borderRadius:11}}/>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection:'row',marginTop:8,justifyContent:'space-between'}}>
                    <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                        {formatTranMoney(item.sale_price ? item.sale_price : 0)} MMK
                    </Text>
                    <IndeButton 
                        increment={() => increment(item) } 
                        decrement={()=> decrement(item)}
                        qty={item.count}
                        onBlur={() => console.log("")}
                        updateQty={(value) => updateQty(item, value) }
                        />
                </View>
                <View style={{alignSelf:'flex-end',marginTop:3}}>
                {/* {
                formErrors.product_id === item.id ? (
                    <ErrorMessage message={`max to ${item.limit}`} />
                )
                :
                (
                    item.limit != null &&
                    (
                        <Text style={{fontFamily:'Lexend-Light',fontSize:12,color:'gray'}}>
                            max to {item.limit}
                        </Text>
                    )
                )
                } */}
                </View>
            </View>
        </View>
        <GapItem/>
    </View>
  );
};

export default CartItem;
