import React from 'react';
import { View, StyleSheet } from 'react-native';

const DottedLine = () => {
  return (
    <View style={styles.dottedLine}></View>
  );
};

const styles = StyleSheet.create({
  dottedLine: {
    borderWidth: 1,
    borderRadius: 1,
    borderColor: 'rgba(196, 196, 196, 0.5)', // change the color and opacity of the line here
    borderStyle: 'dashed',
    marginVertical: 10,
    borderDashPattern: [5, 5], // add space between the dots here
    opacity: 0.7, // increase the opacity of the line here
  },
});

export default DottedLine;
