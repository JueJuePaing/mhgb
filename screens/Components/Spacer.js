import React from 'react';
import { View } from 'react-native';

const Spacer = ({ marginTop }) => {
  return <View style={{ marginTop }} />;
};

export default Spacer;
