import React, { useState } from 'react';
import { StyleSheet, Text, View, Dimensions, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native';

const device = Dimensions.get('window');

export default function IndeButton(props) {

  return (
    <View style={{ flexDirection: 'row', backgroundColor: '#F2F4FF', borderRadius: 16 }}>
      <TouchableOpacity
        onPress={() => props.decrement()}
        style={{
          width: 28,
          height: 28,
          borderRadius: 14,
          justifyContent: 'center',
          backgroundColor: '#DFE3FF',
        }}
      >
        <Text style={{ fontSize: 16, textAlign: 'center', color: '#4157FF', lineHeight: 28, height: 28 }}>-</Text>
      </TouchableOpacity>
      <TextInput
        maxLength={4}
        style={{ width: 46, height: 28, textAlign: 'center', fontFamily: 'Lexend', fontSize: 14 }}
        onChangeText={(text) => props.updateQty(text)}
        onBlur={() => {
          props.onBlur();
        }}
        value={props.qty && props.qty.toString()}
        keyboardType="numeric"
      />
      <TouchableOpacity
        onPress={() => props.increment()}
        style={{
          width: 28,
          height: 28,
          borderRadius: 14,
          justifyContent: 'center',
          backgroundColor: '#2A318B',
        }}
      >
        <Text style={{ fontSize: 16, textAlign: 'center', color: '#fff', lineHeight: 28, height: 28 }}>+</Text>
      </TouchableOpacity>
    </View>
  );
}
