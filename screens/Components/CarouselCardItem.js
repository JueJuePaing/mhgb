import React from 'react'
import { View, StyleSheet, Dimensions } from "react-native"
import LaunchBanner from './LaunchBanner';
const device = Dimensions.get('window');

const CarouselCardItem = ({ item, index }) => {
  return (
    <View style={{alignItems:'center',flex:1,justifyContent:'center'}}>
      <View style={styles.container} key={index}>
        <LaunchBanner 
          imgUrl={item.imgUrl} 
          title={item.title}
          body={item.body}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: device.width - 32,
  },
})

export default CarouselCardItem
