
import React from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions } from 'react-native';
import { showAlert } from 'react-native-customisable-alert';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import {useTranslation} from "react-i18next";

const { width: deviceWidth } = Dimensions.get('window');

const AddressBox = (props) => {

    const {datas, onSelectInfo, navigation,loading } = props;
    const [t] = useTranslation('common');

    const deleteAddress = (id) => {
      showAlert({
        title: `Are You sure?`,
        message: 'ဒီလိပ်စာကိုဖျတ်မှာသေချာပါသလား',
        alertType: 'warning',
        dismissable: true,
        onPress: () => {
          requestDeleteAddress(id);
        }
      });
    }

    const requestDeleteAddress = async(id) => {
        let formdata = new FormData();
        formdata.append('shipping_address', id);

        try {
            const response = await ApiService(url.delete_address, formdata, 'POST');
            if (response.code == '200') {

            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
        }
    }

    return (
    <>
      {datas && datas.map((item, index) => (
        <View key={index} style={{ flexDirection: 'row', marginTop: 10 }}>
          <View
            style={{
              marginTop: 1,
              width: deviceWidth - 32,
              borderColor: 'rgba(9, 15, 71, 0.1)',
              borderWidth: 1,
              borderRadius: 6,
              padding: 15,
            }}
          >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={() => onSelectInfo(item.id)} style={{ flexDirection: 'row' }}>
                <View>
                  {item.default ? (
                    <Image
                      source={require('../../assets/images/selected-checkbox.png')}
                      style={{ width: 18, height: 18, resizeMode: 'contain' }}
                    />
                  ) : (
                    <Image
                      source={require('../../assets/images/checkbox.png')}
                      style={{ width: 18, height: 18, resizeMode: 'contain' }}
                    />
                  )}
                </View>
                <View style={{ width: 200, marginLeft: 10 }}>
                  <Text style={{ fontFamily: 'Pyidaungsu', color: 'black', fontSize: 14 }}>
                    {item.name}
                  </Text>
                  <Text style={{ fontFamily: 'Lexend-Light', color: 'gray', marginTop: 5, fontSize: 14 }}>
                    {item.phone_number}
                  </Text>
                  <Text style={{ fontFamily: 'Pyidaungsu', color: 'gray', marginTop: 5, fontSize: 14 }}>
                    {item.ship_address}
                  </Text>
                </View>
              </TouchableOpacity>
              
              <View style={{ flexDirection: 'column',justifyContent:'space-between' }}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('Address', {
                      'lable': t('edit_address'),
                      'btnText': t('edit_profile'),
                      'addressCount' : datas.length,
                      'isEdit' : true,
                      'editData' : {
                        id : item.id,
                        name : item.name,
                        phoneNumber : item.phone_number,
                        address : item.ship_address,
                        city:item.city,
                        township:item.township,
                        default : item.default
                      }
                    })
                  }>
                  <Image
                    source={require('../../assets/images/pencil.png')}
                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                  />
                </TouchableOpacity>

                {/* <TouchableOpacity
                  onPress={() =>
                    deleteAddress(item.id)
                  }>
                  <Image
                    source={require('../../assets/images/trash.png')}
                    style={{ width: 20, height: 20, resizeMode: 'contain',opacity:0.6,tintColor:'#090F47' }}
                  />
                </TouchableOpacity> */}
              </View>
            </View>
          </View>
        </View>
      ))}
    </>
  );
};

export default AddressBox;
