import React, {useContext} from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {
    Placeholder,
    PlaceholderMedia,
    PlaceholderLine,
    Fade
  } from "rn-placeholder";
import {useTranslation} from "react-i18next";
import { AppContext } from '../../context/AppContextProvider';
const device = Dimensions.get('window');

const DashboardHeader = (props) => {
    const [t] = useTranslation('common');
    const {profileData} = useContext(AppContext);

  return (
    <View style={styles.parent}>
        {
            props.placeHolderLoading ?
            <Placeholder
                Animation={Fade}
                Left={props => (
                    <PlaceholderMedia
                        isRound={true}
                        style={[{ height: 60, width: 60, borderRadius:40 }, props.style]}
                    />
                )}>
                <PlaceholderLine style={{width:180,marginTop:10}} />
                <PlaceholderLine width={30} />
            </Placeholder>

            :

            <View style={styles.container}>
                <TouchableOpacity onPress={() => props.navigation.navigate('Profile')} style={styles.profileContainer}>
                {
                    profileData && profileData.logo ? 
                    <Image
                        resizeMode='contain'
                        source={{ uri: profileData.logo }}
                        style={styles.profilePhoto} />
                    :
                    <Image 
                        resizeMode='contain'
                        source={require('../../assets/images/default_user.png')}
                        style={styles.profilePhoto} />
                }
                </TouchableOpacity>
                <View style={styles.shopContainer}>
                    <Text style={styles.shopName}>{profileData?.name}</Text>
                    <Text style={styles.phoneNumber}>{profileData?.phone}</Text>
                </View>
            </View>
        }
       
        <TouchableOpacity onPress={() => props.navigation.navigate('Search')}>
        <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View
            style={[styles.searchInput,]}
            >
            <Text style={{ 
                    color: '#999',
                    fontFamily:'Pyidaungsu',
                    fontSize : 13
                }}>
            {t('search_products')}
            </Text>
            </View>
            <Image
            source={require('../../assets/images/search.png')}
            style={{
                position: 'absolute',
                width: 15,
                height: 15,
                resizeMode: 'contain',
                top: 15,
                marginLeft: 15,
            }}
            />
        </View>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    parent: {
        flex:1,
        backgroundColor: '#F7FBFF',
        height: '33.33%',
        paddingHorizontal: 16,
        marginTop:getStatusBarHeight() + 10
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    profileContainer: {
        width: 60,
        height: 60,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 30,
        overflow: 'hidden',
        marginTop:10
    },
    profilePhoto: {
        width: 60,
        height: 60,
        borderRadius: 30
    },
    shopContainer: {
        marginLeft: 16,
        flex: 1,
        marginTop:10
    },
    shopName: {
        fontSize: 14,
        fontFamily: 'Lexend-Medium',
        color: '#090F47', //090F47
        marginBottom: .3
    },
    phoneNumber: {
        fontSize: 13,
        fontFamily: 'Lexend-Light',
        color: '#090F47',
        marginTop: 3
    },
    searchInput: {
        width:device.width - 32, //50
        justifyContent:'center',
        backgroundColor:'#fff',
        height: 45,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 40,
    }
});

export default DashboardHeader;
