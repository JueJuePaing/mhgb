import React, { useState,useEffect } from 'react'
import { View, Dimensions, TouchableOpacity, StyleSheet, Text } from "react-native"
import Carousel, { Pagination } from 'react-native-snap-carousel'
import CarouselCardItem from './CarouselCardItem'
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";

const device = Dimensions.get('window');

const data = [
  {
    id : 1,
    title: "ဆေးများကိုအွန်လိုင်းတွင်ကြည့်ရှုပြီးဝယ်ယူပါ",
    // body: "Ut tincidunt tincidunt erat. Sed cursus turpis vitae tortor. Quisque malesuada placerat nisl.",
    body : "ဆေးနှင့်ဆေးပစ္စည်းများအားစျေးသက်သာစွာဝယ်ယူနိုင်ပါသည်။",
    imgUrl: require('../../assets/images/banner1.png'),
  },
  {
    id : 2,
    title: "ဆေးများကိုApplicationမှဝယ်ယူပါ", // "အွန်လိုင်းဆေးဘက်ဆိုင်ရာ & ကျန်းမားရေးစောင့်ရှောက်မှု",
    body: "ဆေးနှင့်ဆေးပစ္စည်းများအားApplicationမှတစ်ဆင့်လွယ်ကူလျင်မြန်စွာဝယ်ယူနိုင်ပါသည်။",
    imgUrl: require('../../assets/images/online.jpg'),
  },
  {
    id : 3,
    title: "POS System အဖြစ်အသုံးပြုနိုင်ပါသည်။",
    body: "Customerကြီးများ၏ဆေးဆိုင်အတွက်အခမဲ့ POS System အားApplicationတွင်အသုံးပြုနိုင်ပါသည်။",
    imgUrl: require('../../assets/images/pos_banner.png'),
  },
];

const CarouselCards = (props) => {
  const [t] = useTranslation('common');
  const [index, setIndex] = useState(0)
  const [goLoginScreen, setGoLoginScreen] = useState(false);
  const isCarousel = React.useRef(null)
  const { colors } = useTheme();

  const nextSlide = () => {
    setIndex(index+1);
    isCarousel.current.snapToNext();
  }

  useEffect(() => {
    if(index == 2){
      setGoLoginScreen(true);
    }
  }, [index])

  return (
    <View style={styles.container}>
      <Carousel
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        onSnapToItem={(index) => setIndex(index)}
        scrollEnabled={true}
        loop={false}
      />

      <TouchableOpacity onPress={()=>{ props.navigation.replace('Welcome') }} style={styles.skipButton}>
          <Text style={{fontFamily:'Lexend',color:'gray'}}>{t('skip')}</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={()=>{ goLoginScreen ? props.navigation.replace('Welcome') : nextSlide() }} 
            style={styles.nextButton}>
          <Text style={{fontFamily:'Lexend-Medium',color:colors.mainBtnColor}}>{t('next')}</Text>
      </TouchableOpacity>

      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        dotStyle={{
          width: 6,
          height: 6,
          borderRadius: 3,
          marginHorizontal: 0,
          backgroundColor: 'rgba(0, 0, 0, 0.92)'
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
        tappableDots={true}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
      flex: 1
  },
  skipButton : {
    bottom: 25,
    left: 35,
    position: 'absolute',
  },
  nextButton: {
      bottom: 25,
      right: 35,
      position: 'absolute',
  },
});

export default CarouselCards