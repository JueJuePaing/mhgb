import React, { useEffect, useState } from 'react';
import { View, Dimensions, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { connect } from 'react-redux';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const TabBottom = (props) => {

  const {state,descriptors,navigation } = props;
  const { colors } = useTheme();

  useEffect(() => {
    
  }, [])

  return (
    <View
      style={{
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        paddingBottom: isIos ? 15 : 0,
      }}
    >
      <View
        style={{
          marginTop: 10,
          marginBottom: 10,
          width: device.width - 32,
        }}
      >
        <View style={{ height: 40, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
          {state.routes.map((route, index) => {
            const { options } = descriptors[route.key];
            const label = options.tabBarLabel !== undefined ? options.tabBarLabel : options.title !== undefined ? options.title : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate({ name: route.name, merge: true, params: { 'hide_back_btn': true } });
              }
            };

            return (
              <TouchableOpacity
                accessibilityState={isFocused ? { selected: true } : {}}
                onPress={onPress}
                key={index}
                style={isFocused ? [styles.active, { borderColor: colors.mainColor }] : styles.inActive}
              >
                {label == 'Dashboard' ? (
                  <View style={{ width: device.width / 5 }}>
                    <Image
                      source={require('../../assets/images/home.png')}
                      style={{
                        width: 24,
                        height: 24,
                        alignSelf: 'center',
                        resizeMode: 'contain',
                      }}
                      tintColor={isFocused ? '#2A328B' : '#9093AC'}
                    />
                  </View>
                ) : label == 'Notification' ? (
                  <View style={{ width: device.width / 5 }}>
                    <Image
                      source={require('../../assets/images/notification.png')}
                      style={{
                        width: 24,
                        height: 24,
                        alignSelf: 'center',
                        resizeMode: 'contain',
                        tintColor: isFocused ? '#2A318B' : '#9093AC',
                      }}
                    />
                    {props.noti_count > 0 && (
                      <View
                        style={{
                          position: 'absolute',
                          right: 14,
                          top: -6,
                          backgroundColor: '#2A318B',
                          borderRadius: 10,
                          minWidth: 20,
                          height: 20,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Text style={{ color: 'white', fontSize: 10, fontFamily: 'Lexend' }}>{props.noti_count}</Text>
                      </View>
                    )}
                  </View>
                ) : label == 'CategoryList' ? (
                  <View style={{ width: device.width / 5 }}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        backgroundColor: isFocused ? '#2A318B' : '#9093AC',
                        borderRadius: 6,
                      }}
                    >
                      <Image
                        source={require('../../assets/images/plus.png')}
                        style={{
                          width: 20,
                          height: 20,
                          alignSelf: 'center',
                          resizeMode: 'contain',
                        }}
                        tintColor="#fff"
                      />
                    </View>
                  </View>
                ) : label == 'Cart' ? (
                  <View style={{ width: device.width / 5 }}>
                    <Image
                      source={require('../../assets/images/cart.png')}
                      style={{
                        width: 24,
                        height: 24,
                        alignSelf: 'center',
                        resizeMode: 'contain',
                      }}
                      tintColor={isFocused ? '#2A318B' : '#9093AC'}
                    />
                    {props.cart_count > 0 && (
                      <View
                        style={{
                          position: 'absolute',
                          right: 14,
                          top: -6,
                          backgroundColor: '#2A318B',
                          borderRadius: 10,
                          minWidth: 20,
                          height: 20,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Text style={{ color: 'white', fontSize: 10, fontFamily: 'Lexend' }}>{props.cart_count}</Text>
                      </View>
                    )}
                  </View>
                ) : label == 'Setting' ? (
                  <View style={{ width: device.width / 5 }}>
                    <Image
                      source={require('../../assets/images/user.png')}
                      style={{
                        width: 24,
                        height: 24,
                        alignSelf: 'center',
                        resizeMode: 'contain',
                      }}
                      tintColor={isFocused ? '#2A328B' : '#9093AC'}
                    />
                  </View>
                ) : (
                  <></>
                )}
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  active: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 46,
    height: 46,
  },
  inActive: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 46,
    height: 46,
  },
});

const mapstateToProps = state => {
  return {
      noti_count : state.noti_count,
      cart_count : state.cart_count
  }
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

export default connect(mapstateToProps,mapDispatchToProps)((TabBottom));

