import React, { useState } from 'react';
import { Modal, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';

const BottomSheet = ({ visible, onClose, children }) => {
    const handleClose = () => {
        onClose();
    };
  
    const handleModalPress = (event) => {
      event.stopPropagation();
    };
  
    const handleOverlayPress = () => {
      handleClose();
    };

    return (
      <Modal visible={visible} 
            transparent={true} 
            onRequestClose={handleClose}
            animationType={'slide'}
      >
        <TouchableWithoutFeedback onPress={handleOverlayPress}>
          <View style={styles.overlay} />
        </TouchableWithoutFeedback>
        <View style={styles.modal} onPress={handleModalPress}>
          {children}
        </View>
      </Modal>
    );
  };
  

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modal: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    padding: 20,
  },
});

export default BottomSheet;
