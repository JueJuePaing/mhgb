import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import { useNavigation, useTheme } from '@react-navigation/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { connect } from 'react-redux';

const device = Dimensions.get('window');

const NavigationHeader = (props) => {

  const { title = '',hideBackButton = false,skipBackScreens,action,showCart = false,cartAction, filterHandler, searchHandler } = props;
  const navigation = useNavigation();
  const canGoBack = navigation.canGoBack();

  const renderBackButton = () => {
    if (hideBackButton || !canGoBack) {
      return <View style={styles.emptyView} />;
    }

    return (
      <TouchableOpacity onPress={() => 
                          skipBackScreens ?
                          navigation.pop(skipBackScreens)
                          :
                          navigation.goBack(null)
                        }>
        <Image
          source={require('../../assets/images/back.png')}
          tintColor={'#090F47'}
          style={styles.backButton}
        />
      </TouchableOpacity>
    );
  };

  const renderCartButton = () => {
    if (!showCart) {
      return null;
    }

    return (
      <TouchableOpacity
        style={styles.cartButton}
        onPress={cartAction}
      >
        <Image
          source={require('../../assets/images/cart.png')}
          tintColor={'#090F47'}
          style={styles.cartIcon}
        />
        <>
        {props.cart_count > 0 && (
            <View
              style={{
                position: 'absolute',
                left: 14,
                top: -6,
                backgroundColor: '#2A318B',
                borderRadius: 10,
                minWidth: 20,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ color: 'white', fontSize: 10, fontFamily: 'Lexend' }}>{props.cart_count}</Text>
            </View>
          )}
        </>
      </TouchableOpacity>
    );
  };

  return (
    <View style={[styles.container, {backgroundColor: title === 'All Product' ? '#f7f5fe' : '#fff'}]}>
      {renderBackButton()}
      <View style={styles.titleContainer}>
        <Text style={[styles.textTitle, { color: '#000', fontSize : hideBackButton ? 16 : 15 }]}>{title}</Text>
      </View>
      {renderCartButton()}
      
      {/* {title === 'All Product' && <View style={styles.btnRow}>
        <TouchableOpacity
          style={styles.btn}
          onPress={filterHandler}>
          <FontAwesome
            name='filter'
            size={23}
            color='#000' />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btn, {marginLeft: 13}]}
          onPress={filterHandler}>
          <Feather
            name='search'
            size={23}
            color='#000' />
        </TouchableOpacity>
      </View>} */}

      {/* {title === 'Transactions' && <View style={styles.btnRow}>
        <TouchableOpacity
          style={styles.btn}
          onPress={filterHandler}>
          <FontAwesome
            name='filter'
            size={23}
            color='#000' />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btn, {marginLeft: 13}]}
          onPress={filterHandler}>
          <Feather
            name='search'
            size={23}
            color='#000' />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btn, {marginLeft: 13}]}>
         <Menu />
        </TouchableOpacity>
      </View>} */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: device.width,
    backgroundColor: '#fff',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: getStatusBarHeight()
  },
  emptyView: {
    width: 10,
  },
  backButton: {
    width: 20,
    height: 20,
    marginLeft: 20,
    marginRight: 10,
  },
  titleContainer: {
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textTitle: {
    marginLeft: 3,
    fontFamily: 'Lexend-Medium',
    fontSize: 15
  },
  cartButton: {
    right: 20, 
    position: 'absolute',
    alignSelf: 'center',
  },
  cartIcon: {
    width: 24,
    height: 24,
  },
  btnRow: {
    right: 20, 
    position: 'absolute',
    flexDirection:'row',
    alignItems: 'center'
  }
});

const mapstateToProps = state => {
  return {
      cart_count : state.cart_count
  }
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

export default connect(mapstateToProps,mapDispatchToProps)((NavigationHeader));

