import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default function HeaderLabel(props) {
  return (
    <>
    <Text style={[styles.header,{
      fontSize : props.size ? props.size : 16
    }]}>
        { props.title }
    </Text>
    </>
  );
}

const styles = StyleSheet.create({
    header: {
      color: "#222",
      textAlign:'left',
      fontFamily:'Pyidaungsu-Bold',
      color:'#090F47'
    },
  })