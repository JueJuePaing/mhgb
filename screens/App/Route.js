import React from 'react';
import LaunchScreen from '../Pages/LaunchScreen';
import Welcome from '../Pages/Welcome';
import Login from '../Pages/Login';
import Register from '../Pages/Register';
import VerifyOtp from '../Pages/VerifyOtp';
import OK from '../Pages/OK';
import Success from '../Pages/Success';
import ProductDetail from '../Pages/ProductDetail';
import Cart from '../Pages/Cart';
import Checkout from '../Pages/Checkout';
import SlipUpload from '../Pages/SlipUpload';
import Dashboard from '../Pages/Dashboard';
import Setting from '../Pages/Setting';
import Notification from '../Pages/Notification';
import ReturnItem from '../Pages/ReturnItem';
import RequestItem from '../Pages/RequestItem';
import ProductList from '../Pages/ProductList';
import Address from '../Pages/Address';
import Profile from '../Pages/Profile';
import SaveOrder from '../Pages/SaveOrder';
import StockKit from '../Pages/StockKit';
import AddStock from '../Pages/AddStock';
import RemainStock from '../Pages/RemainStock';
import ForgotPassword from '../Pages/ForgotPassword';
import Scanner from '../Pages/Scanner';

import { connect } from 'react-redux';

import { createStackNavigator,TransitionPresets } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBottom from '../Components/TabBottom';
import ForgotPasswordVerifyOtp from '../Pages/ForgotPasswordVerifyOtp';
import UpdatePassword from '../Pages/UpdatePassword';
import RemainStockOut from '../Pages/RemainStockOut';
import Search from '../Pages/Search';
import ProductListStack from '../Pages/ProductListStack';
import StockList from '../Pages/StockList';
import CartDetail from '../Pages/CartDetail';
import CategoryList from '../Pages/CategoryList';
import BrandList from '../Pages/BrandList';
import EditProfile from '../Pages/EditProfile';

// new
import Home from '../Pages/new/home';
import NewDashboard from '../Pages/new/dashboard';
import AllProducts from '../Pages/new/allproducts';
import NewRemainStockOut from '../Pages/new/RemainStockOut';
import LatestTransactions from '../Pages/new/transactions';
import Receipt from '../Pages/new/receipt';
import AddTransaction from '../Pages/new/addTransaction';
import TransactionDetail from '../Pages/new/transactionsDetail';
import Language from '../Pages/new/language';
import ProfitMargin from '../Pages/new/profitMargin';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

const Route = (props) => {
    // console.log("token >> ", props.token);
    return(
        <Stack.Navigator screenOptions={{ headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
            }} >

            { props && props.token != null ? (
            <>
                <Stack.Screen name="TabNav" component={TabNav}/>
                <Stack.Screen name="Dashboard" component={Dashboard} />
                <Stack.Screen name="ProductDetail" component={ProductDetail} />
                <Stack.Screen name="Cart" component={Cart} />
                <Stack.Screen name="Checkout" component={Checkout} />
                <Stack.Screen name="SlipUpload" component={SlipUpload} />
                <Stack.Screen name="Profile" component={Profile} />
                <Stack.Screen name="ReturnItem" component={ReturnItem} />
                <Stack.Screen name="RequestItem" component={RequestItem} />
                <Stack.Screen name="ProductList" component={ProductList} />
                <Stack.Screen name="ProductListStack" component={ProductListStack} />
                <Stack.Screen name="Address" component={Address} />
                <Stack.Screen name="SaveOrder" component={SaveOrder} />
                <Stack.Screen name="StockKit" component={StockKit} />
                <Stack.Screen name="AddStock" component={AddStock} />
                <Stack.Screen name="RemainStock" component={RemainStock} />
                <Stack.Screen name="RemainStockOut" component={RemainStockOut} />
                <Stack.Screen name="Scanner" component={Scanner} />
                <Stack.Screen name="StockList" component={StockList} />
                <Stack.Screen name="OK" component={OK} />
                <Stack.Screen name="Search" component={Search} />
                <Stack.Screen name="CartDetail" component={CartDetail} />
                <Stack.Screen name="CategoryList" component={CategoryList} />
                <Stack.Screen name="BrandList" component={BrandList} />
                <Stack.Screen name="EditProfile" component={EditProfile} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="NewDashboard" component={NewDashboard} />
                <Stack.Screen name="AllProducts" component={AllProducts} />
                <Stack.Screen name="NewRemainStockOut" component={NewRemainStockOut} />
                <Stack.Screen name="LatestTransactions" component={LatestTransactions} />
                <Stack.Screen name="TransactionDetail" component={TransactionDetail} />
                <Stack.Screen name="ProfitMargin" component={ProfitMargin} />
                <Stack.Screen name="Receipt" component={Receipt} />
                <Stack.Screen name="AddTransaction" component={AddTransaction} />
                <Stack.Screen name="Language" component={Language} />
            </> )
            :
            ( 
            <>
                <Stack.Screen name="LaunchScreen" component={LaunchScreen} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
                <Stack.Screen name="ForgotPasswordVerifyOtp" component={ForgotPasswordVerifyOtp} />
                <Stack.Screen name="UpdatePassword" component={UpdatePassword} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="Welcome" component={Welcome} />
                <Stack.Screen name="VerifyOtp" component={VerifyOtp} />
                <Stack.Screen name="Success" component={Success} />
            </>
            )
            }
        </Stack.Navigator>
    )
}

function TabNav() {    
    return (
        <BottomTab.Navigator 
            screenOptions={{ 
                tabBarHideOnKeyboard: true,
                headerShown: false,
                ...TransitionPresets.SlideFromRightIOS,
            }} 
            tabBar={props => <TabBottom {...props} />}>

        
          <BottomTab.Screen name="Dashboard" component={Dashboard} />
          <BottomTab.Screen name="Notification" component={Notification} />
          <BottomTab.Screen name="CategoryList" component={CategoryList} />
          <BottomTab.Screen name="Cart" component={Cart} />
          <BottomTab.Screen name="Setting" component={Setting} />
        </BottomTab.Navigator>
      )
}

const mapstateToProps = state => {
    return {
        token:state.token
    };
};
  
const mapDispatchToProps = dispatch => {
    return {

    };
};

export default connect(mapstateToProps,mapDispatchToProps)((Route));