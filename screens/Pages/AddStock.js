import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { ActivityIndicator, FlatList, StyleSheet, Text, View, Dimensions,DeviceEventEmitter, TextInput, TouchableOpacity, ScrollView, Modal } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import { setToken } from '../../stores/actions';
import { connect } from 'react-redux';
import InputLabel from '../Components/InputLabel';
import CButton from '../Components/CButton';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import ErrorMessage from '../Components/ErrorMessage';
import DottedLine from '../Components/DottedLine';
import { showAlert } from 'react-native-customisable-alert';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import IndeButton from '../Components/IndeButton';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const Setting = ({ navigation, route }) => {
    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const {scanProducts} = route.params;

  const [
    currentProduct,
    setCurrentProduct
  ] = useState(scanProducts?.[0]);
  const [
    currentIndex,
    setCurrentIndex
  ] = useState(0);
  const [
    profitMargin,
    setProfitMargin
] = useState();

    const [size, setSize] = useState('');
    const [purchasePrice, setPurchasePrice] = useState('');
    const [salePrice, setSalePrice] = useState('');
    const [expiryDate, setExpiryDate] = useState(new Date());

    const [sizeFocused,setSizeFocused] = useState(false);
    const [purchasePriceFocused,setPurchasePriceFocused] = useState(false);
    const [salePriceFocused,setSalePriceFocused] = useState(false);

    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [searchLoading, setSearchLoading] = useState(false);
    
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
    const [medicineModalVisible, setMedicineModalVisible] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState({});
    const [products, setProduct] = useState([]);

    const [searchText, setSearchText] = useState('');
    const [showPicker, setShowPicker] = useState(false);
    const [lowStockQty, setLowStockQty] = useState(1);

    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);

    let sizeRef = React.createRef();
    let salePriceRef = React.createRef();
    let purchasePriceRef = React.createRef();


    const onCalendarChange = (event, selectedDate) => {
      const currentDate = selectedDate || expiryDate;
      setShowPicker(false);
      setExpiryDate(currentDate);
    };

    useEffect(() => {
      getProfitMargin()
    }, []);

    useEffect(() => {
      if (!scanProducts) {
        setLoading(true);
        getProduct(1)
      }
    }, [scanProducts])

    useEffect(() => {
      if(page > 1) {
          getProduct(page)
      }
    }, [page]);

  const getProfitMargin = async () => {
      try {
          const response = await ApiService(url.profit_margin, [] , 'POST');

          if (response.code == '200') {
              setProfitMargin(response.profit_margin);
          }
      } catch (error) {
          console.error(error);
      }
  }

    useEffect(() => {
      setConfirmBtnDisble(!((selectedProduct || currentProduct) && size && purchasePrice && salePrice));
    }, [selectedProduct,size,purchasePrice,salePrice, currentProduct]); 

    const clearFormFields = () => {
      setSelectedProduct('');
      setSize('');
      setPurchasePrice('');
      setSalePrice('');
      setExpiryDate('');
      setExpiryDate('');
    }

    const addNextProduct = () => {
      setCurrentProduct(scanProducts[currentIndex + 1]);
      setCurrentIndex(currentIndex + 1);
    }

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        if (!scanProducts) {
          setPage(1);
          setLoadMore(true);
          setSearchLoading(true);
          getProduct(1);
        }
      }, 150)
      return () => clearTimeout(delayDebounceFn)
    }, [searchText, scanProducts])
    
    const getProduct = async (pageNo) => {
      let formdata = new FormData();
      searchText &&  formdata.append('search',searchText)

      formdata.append('page',pageNo);
      formdata.append('per_page',5);

      try {
          const response = await ApiService(url.products, formdata, 'POST');
          
          if (response.code == '200') {
      
            if(response.next_pages != ''){
              setProduct(products.concat(response.products));
              setLoadMore(true);
            }else{
              setProduct(products.concat(response.products));
              setLoadMore(false);
            }

            // setProduct(response.products);

            // if(response.next_pages != ''){
            //   setLoadMore(true);
            // }else{
            //   setLoadMore(false);
            // }
            
          }else if (response.code == '422') {
            setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false); 
          setSearchLoading(false);
      }
    }

    const searchProduct = (value) => {
      setSearchText(value);
      setProduct([]);
    }

    const closeModal = () => {
      setMedicineModalVisible(false);
      setSearchText('');
    }

    const requestEditStock = async () => {

      if (size < currentProduct?.used_quantity) {
        showAlert({
          title: `Oops!`,
          message: t('edit_balance_quantity_error'),
          alertType: 'error',
          dismissable: true,
        });
        return;
      }
      setLoading(true); 
      setFormErrors({});
  
      let formdata = new FormData();
      formdata.append('transaction_id', currentProduct?.transaction_id);
      formdata.append('product_id', currentProduct?.id);
      formdata.append('quantity', size);
      formdata.append('purchase_price', purchasePrice);
      formdata.append('sale_price', salePrice);

      try {
        const response = await ApiService(url.edit_balance, formdata, 'POST');

        if (response.code == '200') {
          DeviceEventEmitter.emit('all_products_change', 'true');
          DeviceEventEmitter.emit('dashboard_products_change', 'true');
          clearFormFields();
          navigation.replace('OK',{
            'image' : require('../../assets/images/return_confirm.png'),
            'title' : t('ok'),
            'btnTitle' : t('back_to_home'),
            'description' : t('edit_balance_successful'),
            'isBack' : true
          })
         
        }else if (response.code == '422') {
          setFormErrors(response.errors);
        }else if (response.code == '401' || response.code == '400') {
          showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: true,
          });
        }
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false); 
      }
    }

    const requestStockAdd = async () => {
        setLoading(true); 
        setFormErrors({});
    
        let formdata = new FormData();

        formdata.append('products[0][product_id]', scanProducts ? currentProduct.table_id : selectedProduct.table_id);
        formdata.append('products[0][quantity]',size);
        formdata.append('products[0][purchase_price]',purchasePrice);
        formdata.append('products[0][sale_price]',salePrice);
        formdata.append('products[0][expiry_date]',moment(expiryDate).format('DD-MM-YYYY'));
        formdata.append('products[0][low_stock_warning_quantity]',lowStockQty);

        try {
          const response = await ApiService(url.new_balance, formdata, 'POST');

          if (response.code == '200') {
            DeviceEventEmitter.emit('all_products_change', 'true');
            DeviceEventEmitter.emit('dashboard_products_change', 'true');
            if (!scanProducts || currentIndex + 1 === scanProducts?.length) {
              clearFormFields();
              navigation.replace('OK',{
                'image' : require('../../assets/images/return_confirm.png'),
                'title' : t('ok'),
                'btnTitle' : t('back_to_home'),
                'description' : t('add_balance_successful'),
                'isBack' : true
              })
            } else {
              addNextProduct();
            }
          }else if (response.code == '422') {
            setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false); 
        }
    }

    const increment = () => {
      setLowStockQty(prev => prev+1);
    }

  const decrement = () => {
      if (lowStockQty > 1) {
        setLowStockQty(prev => prev-1);
      }
  }

  const updateQty = (val) => {
    if (val > 0) {
      setLowStockQty(val);
    }
  }

  useEffect(() => {
    if (currentProduct && currentProduct.transaction_id) {
      setSize(currentProduct.total_quantity+'');
      setPurchasePrice(currentProduct.purchase_price+'');
      setSalePrice(currentProduct.sale_price+'')
    } else if (currentProduct) {
      setSize(currentProduct.quantity+'');
      setPurchasePrice(currentProduct.original_price+'');
      if (profitMargin && currentProduct.original_price) {
        let rate = parseInt(currentProduct.original_price) * (parseInt(profitMargin) / 100) + parseInt(currentProduct.original_price);
        setSalePrice(Math.floor(rate)+'');
      } else if (currentProduct.price) {
        setSalePrice(currentProduct.price+'');
      }

    }
  }, [currentProduct, profitMargin]);

  const pattern = /^[0-9\s]+$/;

  const changeSalePrice = (purchase) => {
    if (profitMargin && purchase) {
      let rate = parseInt(purchase) * (parseInt(profitMargin) / 100) + parseInt(purchase);
      setSalePrice(Math.floor(rate)+'');
    } else if (purchase) {
      setSalePrice(purchase+'');
    }
  }

  const onEndReached = () => {
    if(loadMore){
        setPage(page + 1)
    }
  }

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        key={item.id}
        onPress={() => {
          setMedicineModalVisible(false);
          setSelectedProduct(item);
          setSearchText('');
        }}
      >
        <Text style={{paddingVertical : 7, color:'#000',fontFamily:'Lexend',paddingHorizontal:10, fontSize : 13}}>{item.name}</Text>
        <DottedLine/>
      </TouchableOpacity>
    )
  };

  return (
   <View
        style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loading && <Loading/> }
        <NavigationHeader title={currentProduct?.transaction_id ? t('edit_listing') : t('new_listing')}  action={() => navigation.goBack()} />
        <ScrollView 
          keyboardShouldPersistTaps={'handled'}  
          showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
            <View style={styles.container}> 
                <View style={{marginTop:10}}/>
                <InputLabel title={t('medicine_name')} />
                  <TouchableOpacity 
                    disabled={scanProducts ? true : false}
                    onPress={() => setMedicineModalVisible(true) }>
                      <View style={{paddingTop:15}}>
                          <Text style={styles.modalInput}>
                            {scanProducts ? currentProduct?.name : selectedProduct?.name}
                          </Text>
                          <View style={{
                            borderBottomColor: 'rgba(9, 15, 71, 0.1)',
                            borderBottomWidth: 1,
                            color:'#000',
                          }} />
                      </View>
                      {formErrors.city && (
                          <ErrorMessage message={formErrors.city[0]} />
                      )}
                    </TouchableOpacity>
                    
                    {formErrors['products.0.product_id'] && (
                        <ErrorMessage message={formErrors['products.0.product_id']} />
                    )}

                    <Modal
                          visible={medicineModalVisible}
                          onRequestClose={() => closeModal()}
                          animationType="slide"
                          transparent={true}>
                        <View style={styles.modalBackdrop}>
                        <View style={styles.modalContent}>
                            <View style={styles.modalHeader}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                              <Text style={styles.modalHeaderText}>
                               { t('medicine_name')}
                              </Text>
                              <TouchableOpacity onPress={() => closeModal()}>
                              <Text style={{fontFamily:'Lexend',color:'gray',fontSize:22}}>
                                X
                              </Text>
                            </TouchableOpacity>
                            </View>
                            <TextInput
                                style={styles.searchInput}
                                placeholder="Search"
                                value={searchText}
                                onChangeText={text => searchProduct(text) }
                            />
                            </View>
                            <View style={styles.modalBody}>
                            {/* <ScrollView
                              keyboardShouldPersistTaps={'handled'}> */}
                            <View>
                              {searchLoading && <View 
                                style={{
                                  position: 'absolute',
                                  alignSelf: 'center',
                                  marginTop: 50
                                }}>
                                <ActivityIndicator size="large" color="#b5ae0d" size={40} />
                              </View>}

                              <FlatList
                                showsVerticalScrollIndicator={false}
                                data={products}
                                keyExtractor={(item, index) => index.toString()}
                                // contentContainerStyle={{ paddingBottom: 20 }}
                                // ListHeaderComponent={header}
                                // ListFooterComponent={footer}
                                renderItem={renderItem}
                                onEndReached={onEndReached}
                                onEndReachedThreshold={1}
                              />
                              
                           {/*    {products && products.map((product) => (
                                <TouchableOpacity
                                  key={product.id}
                                  onPress={() => {
                                    setMedicineModalVisible(false);
                                    setSelectedProduct(product);
                                    setSearchText('');
                                  }}
                                >
                                  <Text style={{paddingVertical : 7, color:'#000',fontFamily:'Lexend',paddingHorizontal:10, fontSize : 13}}>{product.name}</Text>
                                  <DottedLine/>
                                </TouchableOpacity>
                              ))} */}

                            </View>
                            </View>
                        </View>
                        </View>
                    </Modal>

                <View style={{marginTop:25}}/>  
                    <InputLabel title={t('size')} />
                    <TextInput keyboardType="numeric"
                        ref={sizeRef}
                        maxLength={50}
                        editable={scanProducts ? currentProduct?.transaction_id ? true : false : true}
                        onChangeText={(size) => setSize(size) }
                        onFocus={() => setSizeFocused(true) }
                        onBlur={() => setSizeFocused(false) }
                        value={size}
                        selectionColor={colors.inputActiveColor}
                        onSubmitEditing={() =>  purchasePriceRef.current.focus()}
                        style={[styles.input,{
                            color:'#000',
                            borderColor:'#000',
                            borderBottomColor: sizeFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                     {formErrors['products.0.quantity'] && (
                        <ErrorMessage message={formErrors['products.0.quantity']} />
                    )}
                <View style={{marginTop:25}}/>  
                    <InputLabel title={t('purchase_price')} />
                    <TextInput keyboardType="numeric"
                        ref={purchasePriceRef}
                        maxLength={50}
                        editable={scanProducts ? currentProduct?.transaction_id ? true : false : true}
                        onChangeText={(value) => {
                          if (!value || value === "") {
                            setPurchasePrice(value)
                            changeSalePrice(value);
                          } else if (pattern.test(value)) {
                            setPurchasePrice(value)
                            changeSalePrice(value);
                          }
                        } }
                        onFocus={() => setPurchasePriceFocused(true) }
                        onBlur={() => setPurchasePriceFocused(false) }
                        value={purchasePrice}
                        selectionColor={colors.inputActiveColor}
                        onSubmitEditing={() =>  salePriceRef.current.focus()}
                        style={[styles.input,{
                            color:'#000',
                            borderColor:'#000',
                            borderBottomColor: purchasePriceFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                    {formErrors['products.0.purchased_price'] && (
                        <ErrorMessage message={formErrors['products.0.purchased_price']} />
                    )}
                <View style={{marginTop:25}}/>  
                    <InputLabel title={t('sale_price')} />
                     <TextInput keyboardType="numeric"
                        ref={salePriceRef}
                        maxLength={50}
                        // editable={false}
                        editable={scanProducts ? currentProduct?.transaction_id ? true : false : true}
                        onChangeText={(salePrice) => setSalePrice(salePrice) }
                        onFocus={() => setSalePriceFocused(true) }
                        onBlur={() => setSalePriceFocused(false) }
                        value={salePrice}
                        selectionColor={colors.inputActiveColor}
                        style={[styles.input,{
                            color:'#000',
                            borderColor:'#000',
                            borderBottomColor: salePriceFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                    {formErrors['products.0.sale_price'] && (
                        <ErrorMessage message={formErrors['products.0.sale_price']} />
                    )}
                <View style={{marginTop:25}}/>  
                    <InputLabel title={t('expire_date')} />

                    {showPicker && (

                    <DateTimePicker
                      value={expiryDate}
                      mode="date"
                      display="default"
                      minimumDate={new Date()}
                      onChange={onCalendarChange}
                    />
                  )}

                <TouchableOpacity 
                  disabled={currentProduct?.transaction_id ? true : false}
                  onPress={() => setShowPicker(true) }>
                      <View style={{paddingTop:15}}>
                          <Text style={styles.modalInput}>
                            {moment(expiryDate).format('DD-MM-YYYY')}
                          </Text>
                          <View style={{
                            borderBottomColor: 'rgba(9, 15, 71, 0.1)',
                            borderBottomWidth: 1,
                            color:'#000',
                          }} />
                      </View>
                      {formErrors.expiry_date && (
                          <ErrorMessage message={formErrors.expiry_date[0]} />
                      )}
                </TouchableOpacity>

                  <View style={{marginTop:25}}/>  
                  <InputLabel title={t('low_stock_warning')} />


                  <View
                    style={{marginTop: 5, flexDirection: 'row', 
                    
                    }}>
                    {currentProduct && currentProduct.transaction_id && <TouchableOpacity 
                      activeOpacity={1} 
                      disabled={true} 
                      style={styles.overflow}/>}
                    <IndeButton 
                        qty={lowStockQty}
                        onBlur={() => console.log("")}
                        increment={increment}
                        decrement={decrement}
                        updateQty={updateQty} 
                        />
                  </View>
                        
                    {/* <TextInput keyboardType="numeric"
                        ref={lowStockRef}
                        maxLength={50}
                        onChangeText={(lowStock) => setLowStock(lowStock) }
                        onFocus={() => setLowStockFocused(true) }
                        onBlur={() => setLowStockFocused(false) }
                        value={lowStock}
                        selectionColor={colors.inputActiveColor}
                        style={[styles.input,{
                            color:'#000',
                            borderColor:'#000',
                            borderBottomColor: lowStockFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} /> */}
                {formErrors['products.0.low_stock_warning'] && (
                        <ErrorMessage message={formErrors['products.0.low_stock_warning']} />
                    )}
            </View>
        </ScrollView>
        <View style={{flexDirection:'row',marginTop:15,marginBottom:isIos ? 15 : 0}}>
            <CButton title={currentProduct && currentProduct.transaction_id ? t('edit_profile') : t('add_to_stock')} 
                    color={colors.mainLinkColor}
                    fontFamily={'Pyidaungsu-Bold'}
                    width={device.width - 32}
                    height={50}
                    disabled={confirmBtnDisble}
                    backgroundColor={'#F7EE25'}
                    action={() =>  currentProduct?.transaction_id ? requestEditStock() : requestStockAdd() }
            />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    footer: {
      marginTop: 15,
      marginBottom: 15,
      alignSelf: 'center'
    },
    input: {
      height: 35,
      borderBottomWidth: 1,
      borderRadius:8,
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
    modalInput : {
      borderRadius:8,
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
    modalBackdrop: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 10,
    },
    modalHeader: {
      borderBottomWidth: 1,
      borderColor: '#e0e0e0',
      paddingBottom: 10,
      marginBottom: 10,
    },
    modalHeaderText: {
      fontSize: 15,
      fontFamily: 'Lexend',
      paddingVertical : 5
    },
    modalBody: {
      marginBottom: 10,
      height: 400
    },
    searchInput: {
      height: 40,
      borderWidth: 1,
      borderColor: 'gray',
      borderRadius: 5,
      paddingHorizontal: 10,
      marginTop: 10,
      fontFamily: 'Lexend'
    },
    overflow: {
      position: 'absolute',
      zIndex: 5,
      height : 50,
      width: device.width
    }
  })

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((Setting));
