import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import LoadingView from '../Components/LoadingView';

import { ApiService } from '../../network/service';
import url from '../../network/url';
import { showAlert } from 'react-native-customisable-alert';
import VerticalCategories from '../Components/VerticalCategories';
import VerticalBrands from '../Components/VerticalBrands';

const device = Dimensions.get('window');

const BrandList = (props) => {

    const [brands, setBrands] = useState([])
    const [initialLoading,setInitalLoading] = useState(false);

    useEffect(() => {
        setInitalLoading(true);
        getCategories()
    }, []); 

    const getCategories = async () => {
        try {
            const response = await ApiService(url.brand_list, [] , 'POST');

            if (response.code == '200') {
                setBrands(response.brands);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setInitalLoading(false);
        }
    }
    
    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { initialLoading && <LoadingView/> }
            <NavigationHeader title={'Brands'} 
                            hideBackButton={false}
                            action={() => props.navigation.goBack()}/>
            <View style={styles.container}>
                  <VerticalBrands 
                        items={brands} 
                        navigation={props.navigation}
                        header={null}
                        footer={null}
                        onEndReached={null}
                  />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    lable : {
        fontFamily:'Lexend-Medium',
        fontSize:16,
        marginBottom:20,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:16,
    }
})

export default BrandList; 