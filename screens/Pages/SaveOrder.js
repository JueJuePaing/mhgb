import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import {useTranslation} from "react-i18next";
import { useTheme } from '@react-navigation/native';

import { setCartCount } from '../../stores/actions';
import { connect } from 'react-redux';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import { closeAlert, showAlert } from 'react-native-customisable-alert';
import LoadingView from '../Components/LoadingView';
import { FlatList } from 'react-native-gesture-handler';
import Loading from '../Components/Loading';

const device = Dimensions.get('window');

const Setting = (props) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const [viewLoading,setViewLoading] = useState(false);
    const [loading,setLoading] = useState(false);
    const [orderHistories,setOrderHistories] = useState([]);
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    
    useEffect(() => {
        setLoading(true);
        getOrderHistory();
    }, [])

    useEffect(() => {
        if(page > 1){
            getOrderHistory()
        }
    }, [page]);

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const confirmReOrder = (orderID) => {
        showAlert({
            title: t('reorder'),
            message: t('reorder_confirm_msg'),
            alertType: 'warning',
            onPress: () => requestReOrder(orderID)
      });
    }

    const getCartCount = async() => {
        try {
          const response = await ApiService(url.cart_count, [], 'POST');
          if (response.code == '200') {
            props.setCartCount(response.cart_count);
          } else if (response.code == '422') {
            setFormErrors(response.errors);
          } else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        }
        finally {
          setLoading(false); 
        }
    }

    const requestOrderDetail = async (orderID) => {
        props.navigation.navigate('CartDetail',{
            orderID : orderID
        });
    }

    const requestReOrder = async (orderID) => {
        closeAlert();
        
        let formdata = new FormData();
        formdata.append('order_id',orderID);

        setLoading(true);
        try {
            const response = await ApiService(url.order_reorder, formdata, 'POST');
                await getCartCount();
                props.navigation.navigate('Cart');
            if (response.code == '200') {
            }else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const getOrderHistory = async () => {
        setViewLoading(true);
        let formdata = new FormData();
        formdata.append('page',page);
        formdata.append('per_page',16);

        try {
            const response = await ApiService(url.order_history, formdata, 'POST');

            if (response.code == '200') {
                if(response.next_pages != ''){
                    setOrderHistories(orderHistories.concat(response.orders));
                    setLoadMore(true);
                }else{
                    setOrderHistories(orderHistories.concat(response.orders));
                    setLoadMore(false);
                }

            }else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
            setLoading(false);
        }
    }

  return (
   <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loading && <Loading/> }
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
            <View style={styles.container}> 
                <HeaderLabel size={14} title={t('order_and_reorder')} />
                <View style={{marginTop:15}}/>
                <FlatList
                    data={orderHistories}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={{ paddingBottom: 20 }}
                    ListFooterComponent={viewLoading && <LoadingView/>}
                    onEndReached={handleLoadMore}
                    onEndReachedThreshold={1}
                    renderItem={({ item, index}) => (
                    <TouchableOpacity 
                        onPress={() => requestOrderDetail(item.order_id)}
                        key={index} 
                        style={styles.orderCard}>

                        <View style={{flexDirection:'row',justifyContent:'space-between', marginTop: 10, alignItems: 'center'}}>
                            <Text style={{ fontFamily: 'Lexend-Medium', fontSize: 13, color: '#000'}}>
                                {`${t('order_id')} - ${item.order_id}`}
                            </Text>
                            <Text style={{ fontSize: 11, color: '#9e9b9b' }}>{item.ordered_at}</Text>
                        </View>
                        {/* <View style={{flexDirection:'row',justifyContent:'space-between', marginTop: 10}}>
                            <Text style={{ fontFamily: 'Lexend-Medium', fontSize: 14, color: '#000'}}>
                            {`${t('order_id')} - ${item.order_id}`}
                            </Text>
                            <TouchableOpacity 
                                onPress={() => confirmReOrder(item.order_id)}
                                style={{ width: 150, height: 30, backgroundColor: '#F7EE25', borderRadius: 16, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize:12, color: '#2A318B', textAlign: 'center', textAlignVertical: 'center',fontFamily:'Lexend' }}>{t('reorder')}</Text>
                            </TouchableOpacity>
                        </View> */}
                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 4, marginBottom: 2 }}>
                            <Text style={{ fontFamily: 'Lexend', fontSize: 14, color: '#878484' }}>{item.ordered_at}</Text>
                        </View> */}
                        <Text 
                            numberOfLines={1}
                            style={{
                                marginTop: 6, 
                                marginBottom: 14,
                                color: '#000',
                                fontFamily: 'Lexend', 
                                fontSize: 13,
                            }}>
                            {item.medicines}
                        </Text>
                        <TouchableOpacity 
                            onPress={() => confirmReOrder(item.order_id)}
                            style={{ alignSelf: 'center', marginTop : 6, marginBottom : 6, width: device.width - 60, height: 38, backgroundColor: '#F7EE25', borderRadius: 30, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize:12, color: colors.mainLinkColor, textAlign: 'center', textAlignVertical: 'center',fontFamily:'Lexend' }}>{t('reorder')}</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                )}
                />
            </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    orderCard: {
        marginLeft: 1,
        marginRight: 1,
        paddingHorizontal: 12,
        marginBottom: 20,
        backgroundColor: '#F6F7F9',
        borderRadius: 9,
        shadowColor: '#000',
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.23,
        shadowRadius: 1.62,
        elevation: 1,
        paddingVertical: 9
    }
})

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setCartCount: cart_count => {
          dispatch(setCartCount(cart_count))
        },
      };
};

export default connect(mapstateToProps,mapDispatchToProps)((Setting));
