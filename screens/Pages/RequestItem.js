import React, { useState,useEffect } from 'react';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, View, Dimensions, TextInput, ScrollView } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import ErrorMessage from '../Components/ErrorMessage';
import Loading from '../Components/Loading';
import * as ImagePicker from 'expo-image-picker';
import { showAlert } from 'react-native-customisable-alert';
import ImageLoad from 'react-native-image-placeholder';
import mime from "mime";

const device = Dimensions.get('window');

const RequestItem = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [medicine, setMedicine] = useState('');
    const [brandName, setBrandName] = useState('');
    const [brandType, setBrandType] = useState('');
    const [size, setSize] = useState('');

    const [medicineFocused,setMedicineFocused] = useState(false);
    const [brandNameFocused,setBrandNameFocused] = useState(false);
    const [brandTypeFocused,setBrandTypeFocused] = useState(false);
    const [sizeFocused,setSizeFocused] = useState(false);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);

    let brandNameRef = React.createRef();
    let brandTypeRef = React.createRef();
    let sizeRef = React.createRef();

    const [loading, setLoading] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const [image, setImage] = useState(null);

    useEffect(() => {
      setConfirmBtnDisble(!(image && medicine && brandName && brandType && size));
    }, [image,medicine,brandName,brandType,size]); 

    const handleImagePicker = async () => {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          console.log('Permission denied');
          return;
        }
    
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: false,
          quality: 1,
        });
    
        if (!result.canceled) {
            const { uri } = result.assets[0];
            setImage(uri);
        }
    };

    const generateRandomString = () => {
        return Math.random().toString(36).substring(2, 8);
    }

    const clearFormFields = () => {
      setImage('');
      setMedicine('');
      setBrandName('');
      setBrandType('');
      setSize('');
    }

    const requestRequestItem = async () => {
        setLoading(true); 
        setFormErrors({});
        
        let formdata = new FormData();

        formdata.append('medicine_name',medicine);
        formdata.append('brand_name',brandName);
        formdata.append('type',brandType);
        formdata.append('size',size);
        formdata.append('photo', {type: mime.getType(image), uri:image, name: `${generateRandomString()}.${mime.getType(image).split("/")[1]}`});

        try {
            const response = await ApiService(url.request_medicine, formdata, 'POST');
            if (response.code == '200') {
              clearFormFields();
              props.navigation.push('OK',{
                'image' : require('../../assets/images/return_confirm.png'),
                'title' : t('request_receive'),
                'btnTitle' : t('back_to_home'),
                'description' : t('thank_for_request_item')
              })
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
              showAlert({
                title: `Oops!`,
                message: 'Network requested failed.Please try again',
                alertType: 'error',
                dismissable: false,
              });
          } finally {
            setLoading(false); 
        }
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
            { loading && <Loading/> }
            <NavigationHeader  action={() => props.navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps='handled'
                    style={{ flex: 1 }}
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}>
                <View style={styles.container}> 
                <View style={styles.body}>
                    <HeaderLabel title={t('request_item')} />
                    <View style={{marginTop:30}}/>
                    <CButton title={t('add_medicine_photo')} 
                                type={'file'}
                                color={colors.mainLinkColor}
                                fontFamily={'Pyidaungsu-Bold'}
                                width={device.width - 32}
                                height={50}
                                backgroundColor={'#FFF'}
                                action={() => handleImagePicker() }/>
                    {image && <ImageLoad source={{ uri: image }} style={{ width: '100%', height: 200,marginTop:20,resizeMode:'cover'}} />}
                     {formErrors.photo && (
                        <ErrorMessage message={formErrors.photo[0]} />
                    )}        
                    <View style={{marginTop:25}}/>
                    <InputLabel title={t('medicine_name')} />
                    <TextInput keyboardType="default"
                            maxLength={50}
                            onChangeText={(medicine) => setMedicine(medicine) }
                            onFocus={() => setMedicineFocused(true) }
                            onBlur={() => setMedicineFocused(false) }
                            value={medicine}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() =>  brandNameRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: medicineFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                    {formErrors.medicine_name && (
                        <ErrorMessage message={formErrors.medicine_name[0]} />
                    )}
                <View style={{marginTop:25}}/>    
                    <InputLabel title={t('brand_name')} />
                    <TextInput keyboardType="default"
                            ref={brandNameRef}
                            maxLength={50}
                            onChangeText={(brandName) => setBrandName(brandName) }
                            onFocus={() => setBrandNameFocused(true) }
                            onBlur={() => setBrandNameFocused(false) }
                            value={brandName}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() =>  brandTypeRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: brandNameFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                     {formErrors.brand_name && (
                        <ErrorMessage message={formErrors.brand_name[0]} />
                    )}
                    <View style={{marginTop:25}}/>  
                        <InputLabel title={t('brand_type')} />
                        <TextInput keyboardType="default"
                            ref={brandTypeRef}
                            maxLength={50}
                            onChangeText={(brandType) => setBrandType(brandType) }
                            onFocus={() => setBrandTypeFocused(true) }
                            onBlur={() => setBrandTypeFocused(false) }
                            value={brandType}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() => sizeRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: brandTypeFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                        {formErrors.type && (
                            <ErrorMessage message={formErrors.type[0]} />
                        )}
                    <View style={{marginTop:25}}/>  
                        <InputLabel title={t('size')} />
                        <TextInput keyboardType="default"
                            ref={sizeRef}
                            maxLength={50}
                            onChangeText={(size) => setSize(size) }
                            onFocus={() => setSizeFocused(true) }
                            onBlur={() => setSizeFocused(false) }
                            value={size}
                            selectionColor={colors.inputActiveColor}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: sizeFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                        {formErrors.size && (
                            <ErrorMessage message={formErrors.size[0]} />
                        )}
                </View>
            </View>
            </ScrollView>
            
            <View style={{flexDirection:'row',marginTop:15,marginBottom: 15}}>
              <CButton title={t('will_give_send')} 
                      color={colors.mainLinkColor}
                      fontFamily={'Pyidaungsu-Bold'}
                      width={device.width - 32}
                      height={50}
                      backgroundColor={'#F7EE25'}
                      disabled={confirmBtnDisble}
                      action={() =>  requestRequestItem() }
              />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    footer: {
      marginTop: 15,
      marginBottom: 15,
      alignSelf: 'center'
    },
    input: {
      height: 35,
      borderBottomWidth: 1,
      borderRadius:8,
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
  })

export default RequestItem;
