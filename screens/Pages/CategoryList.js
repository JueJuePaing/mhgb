import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, View, Text, Dimensions, TouchableOpacity, Image } from 'react-native';
import LoadingView from '../Components/LoadingView';

import { ApiService } from '../../network/service';
import url from '../../network/url';
import { showAlert } from 'react-native-customisable-alert';
import VerticalCategories from '../Components/VerticalCategories';
import {useTranslation} from "react-i18next";

const device = Dimensions.get('window');

const CategoryList = (props) => {
    const [t] = useTranslation('common');
    const [categories, setCategories] = useState([])
    const [initialLoading,setInitalLoading] = useState(false);

    useEffect(() => {
        setInitalLoading(true);
        getCategories()
    }, []); 

    const getCategories = async () => {
        try {
            const response = await ApiService(url.category_list, [] , 'POST');

            if (response.code == '200') {
               setCategories(response.categories);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setInitalLoading(false);
        }
    }
    
    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { initialLoading && <LoadingView/> }

            <NavigationHeader title={t('top_categories')} 
                            hideBackButton={!props.back}
                            action={() => props.navigation.goBack()}/>

            <TouchableOpacity 
                style={{marginTop : -15}}
                onPress={() => props.navigation.navigate('Search')}>
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <View style={styles.searchInput}>
                        <Text style={{ 
                                color: '#999',
                                fontFamily:'Pyidaungsu',
                                fontSize : 13
                            }}>
                            {t('search_products')}
                        </Text>
                    </View>
                    <Image
                        source={require('../../assets/images/search.png')}
                        style={{
                            position: 'absolute',
                            width: 15,
                            height: 15,
                            resizeMode: 'contain',
                            top: 15,
                            marginLeft: 15,
                        }} />
                </View>
            </TouchableOpacity>




            <View style={styles.container}>
                  <VerticalCategories 
                        items={categories} 
                        navigation={props.navigation}
                        header={null}
                        footer={null}
                        onEndReached={null}
                  />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
        marginTop : 10
    },
    lable : {
        fontFamily:'Lexend-Medium',
        fontSize:16,
        marginBottom:20,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:16,
    },
    searchInput: {
        width:device.width - 32, //50
        justifyContent:'center',
        backgroundColor:'#fff',
        height: 45,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 40,
    }
})

export default CategoryList; 