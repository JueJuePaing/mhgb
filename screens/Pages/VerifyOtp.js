import React, { useContext, useState,useEffect,useRef } from 'react'
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity,KeyboardAvoidingView, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import HeaderLabel from '../Components/HeaderLabel';
import CButton from '../Components/CButton';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { ApiService } from '../../network/service';
import url from '../../network/url';
import ErrorMessage from '../Components/ErrorMessage';
import Loading from '../Components/Loading';
import VerificationTimer from '../Components/VerificationTimer';
import { showAlert } from 'react-native-customisable-alert';
import { setProfile } from '../../stores/actions';
import { isDevice } from 'expo-device';
import { Platform } from 'react-native';
import * as Notifications from 'expo-notifications';
import { AppContext } from '../../context/AppContextProvider';

const device = Dimensions.get('window');

const VerifyOtp = (props) => {

  const {changeProfileData} = useContext(AppContext);

  const { registerUserData } = props.route.params;
  const { colors } = useTheme();
  const [t] = useTranslation('common');
  const otpRef = useRef(null);
  const [code, setCode] = useState('');
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(60);
  const [formErrors, setFormErrors] = useState({});
  const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
  const [otpToken, setOtpToken] = useState(null);
  const [loading, setLoading] = useState(false);
  const [firebaseToken, setFirebaseToken] = useState('');

  useEffect(() => {

    setTimeout(() => {
      if (otpRef?.current) {
        otpRef.current.focusField(0)
      }
    }, 250);
    setOtpToken(props.route.params.token);
    registerForPushNotificationsAsync().then((token) => setFirebaseToken(token));

  }, [otpRef]);

  async function registerForPushNotificationsAsync() {
    let token;
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    if (isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
      token = (await Notifications.getDevicePushTokenAsync()).data;
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    return token;
  }
  
  useEffect(() => {

    if(code.length >= 6){
      setConfirmBtnDisble(false);
    }else{
      setConfirmBtnDisble(true);
    }
  }, [code])

  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
  
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(interval);
        } else {
          setSeconds(59);
          setMinutes(minutes - 1);
        }
      }
    }, 1000);
  
    return () => {
      clearInterval(interval);
    };
  }, [seconds]);

  const resendCode = async() => {
    setSeconds(60);
    
    setLoading(true); 
    setFormErrors({});

    try {
      const response = await ApiService(url.register, registerUserData, 'POST');
      if (response.code == '200') {
        setOtpToken(response.token);
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false); 
    }
  }

  const submitRegisterOTP = async () => {

    setLoading(true); 
    setFormErrors({});

    registerUserData.append('otp',code);
    registerUserData.append('token',otpToken);
    registerUserData.append('firebase_token',firebaseToken);

    try {
      const response = await ApiService(url.register_verify, registerUserData, 'POST');
      if (response.code == '200') {
        props.navigation.replace('Success',{
          'image' : require('../../assets/images/account_confirm.png'),
          'title' : t('confirmed_phone_number'),
          'btnTitle' : t('continue'),
          'description' : t('confirmed_phone_number_desc'),
          'token' : response.token
        })
        props.setProfile(response.shop);
        changeProfileData(response.shop);
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false); 
    }
  }

  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
        { loading && <Loading/> }
        <NavigationHeader  action={() => props.navigation.goBack()} />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
           <View style={styles.container}>
            <HeaderLabel title={t('create_account')} />

            <View style={{marginTop:30}}/>

              <Text style={styles.noticeText}>
                {t('verify_otp_text1')} +959785189591 {t('verify_otp_text2')}
              </Text>

            <View style={{marginTop:35}}/>

            <OTPInputView
                style={{width: '100%', height:20}}
                ref={otpRef}
                pinCount={6}
                keyboardType='phone-pad'
                autoFocusOnLoad={false}
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeChanged = {(code => {
                  setCode(code);
                })}
            />
            {formErrors.shop_name && (
                <ErrorMessage message={formErrors.otp[0]} />
            )}

            <View style={{marginTop:30}}/>
            
            <CButton 
                title={t('will_send')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                disabled={confirmBtnDisble}
                action={() =>  submitRegisterOTP() }
            />

            <VerificationTimer minutes={minutes} seconds={seconds} />
            
            <TouchableOpacity onPress={ () => resendCode() }>
              <Text style={{
                    fontFamily:'Pyidaungsu',
                    fontSize:15,
                    color:colors.mainLinkColor,
                    paddingTop:15,
                    alignSelf:'center'
                  }}>
                    {t('resend_code')}
                </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width:device.width - 32,
  },
  noticeText: {
    fontFamily:'Pyidaungsu',
    fontSize:16,
    color:'gray'
  },
  borderStyleBase: {
    width: 30,
    height: 45
  },
  borderStyleHighLighted: {
    borderColor: "grey",
  },
  underlineStyleBase: {
    width: 40,
    height: 35,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'grey',
    fontFamily: 'Lexend-Light',
    fontSize: 21
  },
  underlineStyleHighLighted: {
    borderColor: "#4157FF",
  },
})

const mapstateToProps = state => {
  return {
      
  }
};

const mapDispatchToProps = dispatch => {
  return {
      setProfile: profile => {
        dispatch(setProfile(profile))
    },
  };
};

export default connect(mapstateToProps,mapDispatchToProps)((VerifyOtp));
