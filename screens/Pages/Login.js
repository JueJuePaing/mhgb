import React, { useContext, useState,useEffect } from 'react';
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { setProfile, setToken } from '../../stores/actions';
import PasswordShowHide from '../Components/PasswordShowHide';
import ErrorMessage from '../Components/ErrorMessage';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import { showAlert } from "react-native-customisable-alert";
import { isDevice } from 'expo-device';
import { Platform } from 'react-native';
import * as Notifications from 'expo-notifications';
import { AppContext } from '../../context/AppContextProvider';

const device = Dimensions.get('window');

const Login = (props) => {

  const {changeProfileData} = useContext(AppContext);
  
  const { colors } = useTheme();
  const [t] = useTranslation('common');
  const [phoneNumber,setPhoneNumber] = useState('');
  const [password,setPassword] = useState('');

  const [phoneNumberFocused,setPhoneNumberFocused] = useState(false);
  const [passwordFocused,setPasswordFocused] = useState(false);
  const [secureTextEntry,setSecureTextEntry] = useState(true);
  const [formErrors, setFormErrors] = useState({});
  const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
  const [loading, setLoading] = useState(false);
  const [firebaseToken, setFirebaseToken] = useState('');

  let phoneNumberRef = React.createRef();
  let passwordRef = React.createRef();

  const toggleSecureTextEntry = () => {
    setSecureTextEntry(prevState => !prevState);
  };

  useEffect(() => {
    registerForPushNotificationsAsync().then((token) => setFirebaseToken(token));
  }, [])

  async function registerForPushNotificationsAsync() {
    let token;
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
    if (isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
      token = (await Notifications.getDevicePushTokenAsync()).data;
    } else {
      alert('Must use physical device for Push Notifications');
    }
    return token;
  }
  
  useEffect(() => {
    setConfirmBtnDisble(!(phoneNumber && password));
  }, [phoneNumber,password]); 

  const requestLogin = async () => {
    setLoading(true); 
    setFormErrors({});

    let formdata = new FormData();
    formdata.append('phone_number',phoneNumber);
    formdata.append('password',password);
    formdata.append('firebase_token',firebaseToken);

    try {
      const response = await ApiService(url.login, formdata, 'POST');

      if (response.code == '200') {
        props.setToken(response.token);
        props.setProfile(response.shop);
        changeProfileData(response.shop);
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false); 
    }
  }
  
  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
        { loading && <Loading/> }
        <NavigationHeader action={() => props.navigation.goBack()} />
          <ScrollView showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
            style={{ flex: 1 }}
            contentContainerStyle={{
              flexGrow: 1,
            }}>
            <View style={styles.container}>
              <View style={styles.body}>

                <HeaderLabel title={t('welcome')} />
                <View style={{marginTop:30}}/>

                <InputLabel title={t('phone_number')} /> 
                <TextInput 
                    keyboardType="numeric"
                    ref={phoneNumberRef}
                    maxLength={50}
                    onChangeText={(value) => setPhoneNumber(value) }
                    onFocus={() => setPhoneNumberFocused(true)}
                    onBlur={() => setPhoneNumberFocused(false)}
                    value={phoneNumber}
                    selectionColor={colors.inputActiveColor}
                    onSubmitEditing={() =>  passwordRef.current.focus()}
                    style={[styles.input,{
                      borderBottomColor: phoneNumberFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                    }]} 
                  />
                {formErrors.phone_number && (
                    <ErrorMessage message={formErrors.phone_number[0]} />
                )}

                <View style={{marginTop:25}}/>    

                <InputLabel title={t('password')} />
                <View>
                  <TextInput 
                      keyboardType="default"
                      ref={passwordRef}
                      maxLength={50}
                      onChangeText={(value) => setPassword(value) }
                      onFocus={() => setPasswordFocused(true)}
                      onBlur={() => setPasswordFocused(false)}
                      value={password}
                      secureTextEntry={secureTextEntry}
                      selectionColor={colors.inputActiveColor}
                      style={[styles.input,{
                        borderBottomColor: passwordFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                      }]} 
                    />
                    {
                      password != '' && 
                      <PasswordShowHide
                          secureTextEntry={secureTextEntry}
                          toggleSecureTextEntry={toggleSecureTextEntry}
                          tintColor={colors.textColor}
                      />
                    }
                </View>
                {formErrors.password && (
                    <ErrorMessage message={formErrors.password[0]} />
                )}

                <View style={{marginTop:25}}/>
                  <CButton 
                      title={t('sign_in')} 
                      color={colors.mainLinkColor}
                      fontFamily={'Pyidaungsu-Bold'}
                      width={device.width - 32}
                      height={50}
                      disabled={confirmBtnDisble}
                      backgroundColor={'#F7EE25'}
                      action={() => requestLogin() }
                  />
                   <TouchableOpacity onPress={()=> props.navigation.navigate('ForgotPassword') }>
                      <Text style={{fontSize:14,color:'gray',fontFamily:'Pyidaungsu',alignSelf:'center',marginTop:15,textDecorationLine:'underline'}}>
                        {t('forgot_password_login')}
                      </Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.footer}>
                  <TouchableOpacity onPress={()=> props.navigation.navigate('Register') }>
                      <Text style={{fontSize:14,color:'gray',fontFamily:'Pyidaungsu'}}>
                        {t('no_account')}
                      </Text>
                  </TouchableOpacity>
              </View>
            </View>

          </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width:device.width - 32,
    flexDirection: 'column', 
    flexGrow: 1,     
    justifyContent: 'space-between'
  },
  footer: {
    marginTop: 25,
    marginBottom: 25,
    alignSelf: 'center'
  },
  input: {
    height: 35,
    borderBottomWidth: 1,
    borderRadius:8,
    fontSize:16,
    fontFamily:'Pyidaungsu',
    color: '#000',
    borderColor:'#000'
  },
})

const mapstateToProps = state => {
  return {
      
  }
};

const mapDispatchToProps = dispatch => {
  return {
      setToken: token => {
          dispatch(setToken(token))
      },
      setProfile: profile => {
        dispatch(setProfile(profile))
    },
  };
};

export default connect(mapstateToProps,mapDispatchToProps)((Login));
