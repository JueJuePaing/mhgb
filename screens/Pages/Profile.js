import React, { useContext } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, ScrollView, TouchableOpacity } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import HeaderLabel from '../Components/HeaderLabel';
import { AppContext } from '../../context/AppContextProvider';

const device = Dimensions.get('window');

const Profile = (props) => {

    const {profileData} = useContext(AppContext);

    const { colors } = useTheme();
    const [t] = useTranslation('common');

  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
        <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
                <View style={styles.container}>
                    <HeaderLabel title={t('account_detail')} />
                    <View style={{marginTop:10}}/>
                        <View style={{flexDirection:'row'}}>
                        <View>
                            <View style={{paddingVertical:15}}>
                                {
                                    profileData && profileData.logo ? 
                                    <Image
                                        resizeMode='contain'
                                        source={{ uri: profileData.logo }}
                                        style={{ width: 90, height: 90, borderRadius: 45}} />
                                    :
                                    <Image 
                                        resizeMode='contain'
                                        source={require('../../assets/images/default_user.png')}
                                        style={{ width: 90, height: 90, borderRadius: 45}} />
                                }
                            </View>
                            <View style={{flexDirection:'row',paddingVertical:10}}>
                                <Image 
                                    tintColor={colors.mainTextColor}
                                    style={{width:20,height:20,resizeMode:'contain'}}
                                    source={require('../../assets/images/store.png')} 
                                />
                                <Text style={{fontFamily:'Pyidaungsu',fontSize:15,color:colors.mainTextColor,marginLeft:20}}>
                                    {profileData && profileData.name}
                                </Text>
                            </View>
                                
                            <View style={{flexDirection:'row',paddingVertical:10}}>
                                <Image 
                                    tintColor={colors.mainTextColor}
                                    style={{width:20,height:20,resizeMode:'contain'}}
                                    source={require('../../assets/images/phone.png')} 
                                />
                                <Text style={{fontFamily:'Pyidaungsu',fontSize:15,color:colors.mainTextColor,marginLeft:20}}>
                                    {profileData && profileData.phone}
                                </Text>
                            </View>

                            {profileData && profileData.address && <View style={{flexDirection:'row',paddingVertical:10}}>
                                <Image 
                                    tintColor={colors.mainTextColor}
                                    style={{width:20,height:20,resizeMode:'contain'}}
                                    source={require('../../assets/images/pin.png')} 
                                />
                                <Text style={{fontFamily:'Pyidaungsu',fontSize:15,color:colors.mainTextColor,marginLeft:20}}>
                                    {profileData.address}
                                </Text>
                            </View>}

                            <View style={{ flexDirection: 'row', marginTop: 50, width: device.width-50, justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('EditProfile')}>
                                    <Text style={{ color:colors.mainLinkColor, textDecorationLine: 'underline', textAlign: 'center',fontFamily: 'Pyidaungsu', fontSize: 16 }}>
                                        {t('edit_profile')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
        </ScrollView>
    </View>
  );
}


const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
    },
    profileImage: {
        width:120,
        height:120,
        resizeMode:'cover',
        borderRadius:60,
        borderColor:'#fff',
        borderWidth:2,
    }
})

export default Profile;
