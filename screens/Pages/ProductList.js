import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, ScrollView, Text, TextInput, View, Dimensions,Image, TouchableOpacity } from 'react-native';
import LoadingView from '../Components/LoadingView';
import VerticalProducts from '../Components/VerticalProducts';

import { ApiService } from '../../network/service';
import url from '../../network/url';
import { showAlert } from 'react-native-customisable-alert';
import {useTranslation} from "react-i18next";

const device = Dimensions.get('window');

const ProductList = (props) => {

    const [t] = useTranslation('common');
    const [products, setProduct] = useState([])
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [loading, setLoading] = useState(false);
    const [initialLoading,setInitalLoading] = useState(false);
    const [refreshing,setRefreshing] = useState(false);
    const [search, setSearch] = useState('');
    const [noResult, setNoResult] = useState(false);
    const [currentAlphabet, setCurrentAlphabet] = useState('');

    const ALPHABETS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    const searchProduct = () => {
        setInitalLoading(true);
        setCurrentAlphabet('');
        setPage(1);
        setProduct([]);
        getProducts(1);
      }

    useEffect(() => {
        setInitalLoading(true);
        setPage(1);
        setProduct([]);
        getProducts(1);
    }, [currentAlphabet]);

    useEffect(() => {
        if (page === 1) {
            setInitalLoading(true);
        }
        getProducts(page)
    }, [page]);

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const getProducts = async () => {
        setLoading(true);
        let formdata = new FormData();
        formdata.append('page',page);
        formdata.append('per_page',16);
        if (currentAlphabet && currentAlphabet !== '') {
            formdata.append('search_alphabet',currentAlphabet);
        } else {
            formdata.append('search',search);
        }

        try {
            const response = await ApiService(url.products, formdata , 'POST');

            if (response.code == '200') {

                if(response.products.length > 0){
                    setNoResult(false);
                    if (page === 1) {
                      setProduct(response.products);
                    } else {
                      setProduct(products.concat(response.products));
                    }
                } else if (page === 1){
                    setNoResult(true);
                }

                if(response.next_pages != ''){
                    setLoadMore(true);
                }else{
                    setLoadMore(false);
                }
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
            setInitalLoading(false);
            setRefreshing(false);
        }
    }

    useEffect(() => {
        if(refreshing){
           getProducts();
        }
    }, [refreshing, search])

    const handleRefresh = () => {
        if (refreshing) {
            return;
        }
        setPage(1);
        setProduct([]);
        setRefreshing(true);
    }

    const handleAlphabet = async (alphabet) => {
        if (currentAlphabet === alphabet) {
            setCurrentAlphabet('')
        } else {
            setCurrentAlphabet(alphabet);
            setSearch('')
        }
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            <NavigationHeader title={t('product_listing')} 
                hideBackButton={true}
                cartAction={() => props.navigation.navigate('Cart')}
                action={() => props.navigation.goBack()} />

            <TouchableOpacity style={{ marginBottom : 15 }}>

                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.searchInput}>
                  
                        <TextInput
                            style={styles.searchBox}
                            placeholder={t('search_products')}
                            placeholderTextColor="#A6A6A6"
                            editable={ initialLoading || loading ? false : true }
                            value={search}
                            onChangeText={(value) => {
                                setSearch(value);
                                setNoResult(false);
                            }}
                            onSubmitEditing={searchProduct}
                             />
                                
                    </View>
                    <Image
                        source={require('../../assets/images/search.png')}
                        style={{
                            position: 'absolute',
                            width: 15,
                            height: 15,
                            resizeMode: 'contain',
                            top: 15,
                            marginLeft: 15,
                        }} />
                </View>
            </TouchableOpacity>

            <View style={styles.container}>
           
            { initialLoading &&  <View style={styles.loadingView}>
                <LoadingView />
            </View>}
                    
            <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollContainer}>
                {ALPHABETS.map((alphabet, index) => {
                    return <TouchableOpacity
                        key={alphabet}
                        style={[styles.alphanetBtn, alphabet === currentAlphabet && styles.selected]}
                        activeOpacity={0.7}
                        onPress={()=> handleAlphabet(alphabet)}>
                        <Text style={[styles.alphabet, {
                            color : alphabet === currentAlphabet ? '#fff' : '#575754'
                        }]}>{alphabet}</Text>
                    </TouchableOpacity>
                })}
            </ScrollView>
        
            {
                !noResult ?        
                    <VerticalProducts 
                            items={products} 
                            navigation={props.navigation} 
                            header={null}
                            footer={loading && <LoadingView/>}
                            onEndReached={handleLoadMore}
                            onRefresh={handleRefresh}
                            refreshing={refreshing} />
                    :
                    <Text style={{fontFamily:'Lexend',fontSize:16,alignSelf:'center', marginTop : 100}}>
                        {t('no_result')}
                    </Text>
            }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    lable : {
        fontFamily:'Lexend-Medium',
        fontSize:16,
        marginBottom:20,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:16,
    },
    searchInput: {
        width:device.width - 32, //50
        justifyContent:'center',
        backgroundColor:'#fff',
        height: 45,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 40,
    },
    searchBox: {
        flex: 1,
        marginLeft: 16,
        marginRight: 16,
        fontSize: 14.5,
        fontFamily: 'Lexend',
        color: '#000'
      },
      loadingView : {
        zIndex : 10,position: 'absolute', 
         position: "absolute",
         left: 0,
         right: 0,
         top: 0,
         bottom: 0,
         opacity: 0.7,
         justifyContent: "center",
         alignItems: "center"
      },
    scrollContainer : {
        position : "absolute",
        top: 0,
        right: -10,
        zIndex: 99,
        maxHeight : device.height * 0.73,
        backgroundColor : "#f2f2f0",
        width : device.width * 0.05,
        borderRadius : 7,
        paddingVertical : 5
    },
    alphanetBtn : {
        paddingVertical : 3.5,
        width : device.width * 0.05,
        alignItems: 'center'
    },
    selected : {
        backgroundColor : "#2A318B"
    },
    alphabet : {
        fontSize : 9,
        fontFamily:'Lexend',
        color: '#575754'
    }
})

export default ProductList; 