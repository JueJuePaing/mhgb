import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";

import CButton from '../Components/CButton';
import AddressBox from '../Components/AddressBox';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import LoadingView from '../Components/LoadingView';
import { connect } from 'react-redux';
import { showAlert } from 'react-native-customisable-alert';
import * as Clipboard from 'expo-clipboard';
import { formatTranMoney } from '../../configs/helper';
import Toast from 'react-native-easy-toast';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const CheckOut = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [address, setAddress] = useState([]);
    const [payment,setPayment] = useState([])
    const [totalAmount, setTotalAmount] = useState(0);
    const [viewLoading,setViewLoading] = useState(false);
    const [defaultAddress,setDefaultAdress] = useState(null);
    const [defaultPayment,setDefaultPayment] = useState(null);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(false);
    const [paymentType, setPaymentType] = useState("cash_on_delivery");

    useEffect(() => {
        const unsubscribe = props.navigation.addListener ('focus', async () =>{
            getAddress();
        })
        getPayment();
        return unsubscribe;
    }, [])
    
    useEffect(() => {
        if (address.length !== 0) {
            const defaultAddress = address.find(obj => obj.default === true);
            setDefaultAdress(defaultAddress);
        }
    },[address])

    useEffect(() => {
        if (payment.length !== 0) {
            const defaultPayment = payment.find(obj => obj.selected === true);
            setDefaultPayment(defaultPayment);
        }
    },[payment])

    useEffect(() => {
        if (paymentType === "cash_down") {
            if (defaultAddress && defaultPayment)
                setConfirmBtnDisble(false);
            else 
                setConfirmBtnDisble(true);
        }
        if (paymentType === "cash_on_delivery") {
            if (defaultAddress)
                setConfirmBtnDisble(false);
            else 
                setConfirmBtnDisble(true);
        }
    }, [defaultAddress,defaultPayment, paymentType]); 

    const selectAddress = async (id) => {

        let formdata = new FormData();
        formdata.append('address_id',id);

        try {
            const response = await ApiService(url.change_default_address, formdata, 'POST');

            if (response.code == '200') {
                setAddress(response.addresses);
            }else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {

        }
    }

    const selectedPayment = async(item) => {
        await Clipboard.setStringAsync(item.account_number);
        toast.show(<Text style={{color: '#000',fontFamily:'Lexend',padding:5,borderRadius:5}}>
            {item.account_number} copied to clipboard
        </Text>,2000);

        setPayment(payment.map(x => (x.id === item.id ? { ...x, selected: true } : { ...x, selected: false } )));
    }

    const getPayment = async () => {

        try {
            const response = await ApiService(url.payment_type, [], 'POST');
            setPayment(response.payment_types);
            if (response.code == '200') {
            }
            else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const getAddress = async () => {
        setViewLoading(true);
        try {
            const response = await ApiService(url.address_list, [], 'POST');
           
            if (response.code == '200') {
                setAddress(response.addresses);
                setTotalAmount(response.net_amount);
            }
            else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
        }
    }

  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        <NavigationHeader title={t('Checkout')}  action={() => props.navigation.goBack()} />
        <Toast
            position='bottom'
            style={{
                backgroundColor: '#fff',
                shadowColor: '#000',
                shadowOffset: {
                width: 0,
                height: 2,
                },
                shadowOpacity: 0.2,
                shadowRadius: 4,
                elevation: 2,
                borderWidth: 0.5,
                borderColor: '#ccc',
            }}
            ref={(toast2) => (toast = toast2)}
            />

        <View style={{marginTop:10}}/>
        <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
                <View style={styles.container}>
                    {viewLoading && <LoadingView/> }
                    <View style={{flexDirection:'row'}}>
                        <Text style={styles.addressLable}>{t('shipping_address')}</Text>
                    </View>
                   <AddressBox 
                        datas={address} 
                        onSelectInfo={selectAddress}
                        navigation={props.navigation}
                    />  
                    <TouchableOpacity 
                        onPress={() => props.navigation.navigate('Address',{
                            'lable' : t('add_new_address'),
                            'btnText' : t('add'),
                            'addressCount' : address.length,
                            'isEdit' : false,
                            'editData':{}
                        })}
                        style={{flexDirection:'row',justifyContent:'flex-end',marginTop:20}}>
                        <Text style={{fontFamily:'Lexend',color:colors.inputActiveColor,fontSize:14}}>
                            {` + ${t('add_address')}`}
                        </Text>
                    </TouchableOpacity>

                    <View style={{flexDirection:'row',marginTop:20}}>
                        <Text style={[styles.title,{color: colors.mainTextColor}]}>
                            {t('choose_payment_method')}
                        </Text>
                    </View>

                    <View style={styles.paymentBox}>
                        <TouchableOpacity 
                            onPress={() => setPaymentType("cash_on_delivery")}
                            style={{
                                    flexDirection:'row',
                                    padding:15,
                                    alignItems: "center"
                                }}>
                            <View>
                                <Image 
                                    source={paymentType === "cash_on_delivery" ? require("../../assets/images/selected-checkbox.png") : require("../../assets/images/checkbox.png")} 
                                    style={{width:18,height:18}}
                                    resizeMode={'contain'}
                                />
                            </View>
                            <View style={{marginLeft:30}}>
                                <Text style={{fontFamily:'Lexend',fontSize:12.5}}>
                                    {t("cash_on_delivery")}
                                </Text>
                            </View>
                            
                        </TouchableOpacity>

                        <TouchableOpacity 
                            onPress={() => setPaymentType("cash_down")}
                            style={{
                                    flexDirection:'row',
                                    padding:15,
                                    alignItems: "center",
                                    paddingTop: 7
                                }}>
                            <View>
                                <Image 
                                    source={paymentType === "cash_down" ? require("../../assets/images/selected-checkbox.png") : require("../../assets/images/checkbox.png")} 
                                    style={{width:18,height:18}}
                                    resizeMode={'contain'}
                                />
                            </View>
                            <View style={{marginLeft:30}}>
                                <Text style={{fontFamily:'Lexend',fontSize:12.5}}>
                                    {t("cash_down")}
                                </Text>
                            </View>
                            
                        </TouchableOpacity>
                        {paymentType === "cash_down" && <View>
                            {
                                payment.map((item,index) => {
                                    return (
                                        <TouchableOpacity 
                                            onPress={() => selectedPayment(item)}
                                            key={index} 
                                            style={{
                                                    flexDirection:'row',
                                                    padding:15 
                                                }}>
                                            <View>
                                                <Image source={{uri:item.logo}} 
                                                    style={{width:40,height:40}}
                                                    resizeMode={'contain'}
                                                />
                                            </View>
                                            <View style={{marginLeft:30,marginRight:30}}>
                                                <Text style={{fontFamily:'Lexend',fontSize:12.5, paddingBottom : 1}}>{item.account_name}</Text>
                                                <Text style={{fontFamily:'Lexend-Medium',fontSize:13}}>{item.account_number}</Text>
                                            </View>
                                            {
                                                item.selected &&  
                                                <View style={{alignSelf:'center'}}>
                                                    <Image source={require('../../assets/images/check.png')} 
                                                        style={{width:18,height:18}}
                                                        tintColor={'green'}
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                            }
                                            
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </View>}
                    </View>

                </View>
        </ScrollView>
        <View style={{marginTop:15,marginBottom:isIos ? 15 : 0}}>
            <CButton title={t(`${t('place_order')} @${formatTranMoney(totalAmount)}MMK`)} 
                    color={colors.mainLinkColor}
                    fontFamily={'Lexend-Medium'}
                    width={device.width - 32}
                    height={50}
                    backgroundColor={'#F7EE25'}
                    disabled={confirmBtnDisble}
                    action={() =>  props.navigation.navigate('SlipUpload',{
                        'defaultAddress' : defaultAddress,
                        'defaultPayment' : defaultPayment,
                        'paymentType' : paymentType
                    })}/>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:device.width - 32,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:14
    },
    addressLable: {
        fontFamily:'Pyidaungsu-Bold',
        color:'#090F47',
        fontSize:14
    },
    paymentBox: {
        borderWidth:1,
        borderRadius:6,
        borderColor: 'rgba(9, 15, 71, 0.1)',
        marginTop: 10
    }
})

const mapstateToProps = state => {
    return {
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((CheckOut));

