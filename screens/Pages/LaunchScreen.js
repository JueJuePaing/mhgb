import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import CarouselCards from '../Components/CarouselCards';
import { useTheme } from '@react-navigation/native';

export default function LaunchScreen(props) {  
  const { colors } = useTheme();
  return (
    <SafeAreaView style={[styles.container,{backgroundColor:colors.mainBackgroundColor}]}>
      <CarouselCards navigation={props.navigation}/>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});