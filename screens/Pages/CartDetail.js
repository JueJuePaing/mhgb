import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import GapItem from '../Components/GapItem';
import { showAlert } from "react-native-customisable-alert";
import { ApiService } from '../../network/service';
import url from '../../network/url';
import ErrorMessage from '../Components/ErrorMessage';
import Loading from '../Components/Loading';
import LoadingView from '../Components/LoadingView';
import { connect } from 'react-redux';

const device = Dimensions.get('window');
const secondColumnWidth = device.width - 130;

const Cart = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const [info,setInfo] = useState([])
    const [totalQty,setTotalQty] = useState(0);
    const [totalDiscount,setTotalDiscount] = useState(0);
    const [orderTotal,setOrderTotal] = useState(0);
    const [totalAmount,setTotalAmount] = useState(0);
    const [couponCode, setCouponCode] = useState('');
    const [formErrors, setFormErrors] = useState({});
    const [isCouponValid, setIsCouponValid] = useState(false);
    const [couponDiscount, setCouponDiscount] = useState({});
    const [loading, setLoading] = useState(false);
    const [viewLoading, setViewLoading] = useState(false);
    const [shippingFee, setShippingFee] = useState(0);
    const [shippingStatus, setShippingStatus] = useState();
    const [paymentStatus, setPaymentStatus] = useState();

    useEffect(() => {
        const unsubscribe = props.navigation.addListener ('focus', async () =>{
            getCartList();
            setFormErrors({});
        })
        return unsubscribe;
    }, [])

    const getCartList = async () => {
        setViewLoading(true);
        let formdata = new FormData();
        formdata.append('order_id',props.route.params.orderID);

        try {
            const response = await ApiService(url.order_detail, formdata, 'POST');
            if (response.code == '200') {
                reRenderData(response);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
        }
    }

    const reRenderData = (response) => {
        setInfo(response.cart_detail);
        setTotalAmount(response.net_amount)
        setOrderTotal(response.total_product_amount)
        setTotalDiscount(response.total_product_discount);
        setTotalQty(response.product_count);
        setCouponDiscount(response.coupon_discount);
        setIsCouponValid(response.is_coupon_valid);
        setShippingFee(response.shipping_fee);
        setShippingStatus(response.shipping_status);
        setPaymentStatus(response.payment_status);
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { loading && <Loading/> }
            <NavigationHeader hideBackButton={
                                props.route.params ? props.route.params.hide_back_btn : false
                            } 
                            title={props.route.params.orderID}  
                            action={() => props.navigation.goBack()} />
            <ScrollView 
                keyboardShouldPersistTaps={'handled'}
                showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
                <View style={styles.container}>
                    { viewLoading && <LoadingView/> }

                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontFamily:'Lexend',color:'gray',fontSize:14}}>{totalQty} total items</Text>
                    </View>

                    <View>
                        { 
                            info.length > 0 && 
                            <View>
                                <View style={{marginTop:25}}>
                                {
                                    info.map((item,index) => {
                                    return (
                                        <View key={item.id}>
                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                <View style={{paddingRight:5}}>
                                                    <Image 
                                                    source={{uri: item.feature_image}} 
                                                    style={{width:70,height:70,borderRadius:12}}
                                                    resizeMode={'contain'}
                                                    />
                                                </View>
                                                <View style={{width:secondColumnWidth}}>
                                                    <View style={{
                                                            flexDirection:'row',
                                                            justifyContent:'space-between'
                                                        }}>
                                                        <View style={{width:secondColumnWidth/1.3}}>
                                                            <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                                                                {item.name}
                                                            </Text>
                                                            <Text style={{fontFamily:'Lexend-Light',fontSize:13,color:'gray'}}>
                                                                {item.size} {item.size_unit}
                                                            </Text>
                                                        </View>
                                                    </View>

                                                    <View style={{flexDirection:'row',marginTop:8,justifyContent:'space-between'}}>
                                                        <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                                                            {item.price} MMK
                                                        </Text>
                                                        <Text style={{fontFamily:'Lexend-Light',fontSize:14,color:colors.mainTextColor}}>
                                                            {item.quantity} qty
                                                        </Text>
                                                    </View>
                                                    <View style={{alignSelf:'flex-end',marginTop:3}}>
                                                    {
                                                        formErrors.product_id == item.id && (
                                                            <ErrorMessage message={'Reach product limit'} />
                                                        )
                                                    }
                                                    </View>
                                                </View>
                                            </View>
                                            <GapItem/>
                                        </View>
                                    )
                                    })
                                }
                                </View>

                                <View>
                                    {shippingStatus && <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                        <Text style={[styles.title,{color: colors.mainTextColor}]}>
                                            {t('shipping_status')} 
                                        </Text>
                                        <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                            {shippingStatus === "Pending" ? t('pending') : 
                                            shippingStatus === "Processing" ? t('processing') : 
                                            shippingStatus === "Confirmed" ? t('confirmed') : 
                                            shippingStatus === "Delivering" ? t('delivering') : 
                                            shippingStatus === "Going to Bus Gate" ? t('going_to_bus_gate') : 
                                            shippingStatus === "Completed" ? t('completed') : 
                                            shippingStatus === "Rejected" ? t('rejected') : 
                                            "N/A"}
                                        </Text>
                                    </View>}
                                    {paymentStatus && <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                        <Text style={[styles.title,{color: colors.mainTextColor}]}>
                                            {t('payment_status')} 
                                        </Text>
                                        <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                            {paymentStatus === "Pending" ? t('pending') : 
                                              paymentStatus === "Completed" ? t('payment_completed') : 
                                              paymentStatus === "Rejected" ? t('rejected') : 
                                              "N/A"}
                                        </Text>
                                    </View>}
                                   
                                </View>

                                {(shippingStatus || paymentStatus) && <GapItem/>}

                               

                                <View>
                                    <Text style={[styles.title,{color: colors.mainTextColor}]}>
                                        {t('payment')} 
                                    </Text>
                                    <View>
                                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                            <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                {t('order_total')}
                                            </Text>
                                            <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                {orderTotal} MMK
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                            <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                {t('items_discount')}
                                            </Text>
                                            <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                {totalDiscount} MMK
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                            <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                {t('coupon_discount')}
                                            </Text>
                                            {
                                                isCouponValid ? 
                                                <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {couponDiscount} MMK
                                                </Text>
                                                :
                                                <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                0 MMK
                                                </Text>
                                            }
                                        </View>
                                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                            <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                {t('shipping_fee')}
                                            </Text>
                                            <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                {shippingFee} MMK
                                            </Text>
                                        </View>
                                        <GapItem/>
                                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                            <Text style={{fontSize:16,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                {t('total')}
                                            </Text>
                                            <Text style={{fontSize:16,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                {totalAmount} MMK
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
        flex:1,
    },
    // title: {
    //     fontFamily: 'Pyidaungsu-Bold',
    //     fontSize:16,
    // },
    // modalBackdrop: {
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     backgroundColor: 'rgba(0, 0, 0, 0.5)',
    // },
    // modalContent: {
    //     backgroundColor: 'white',
    //     width: '90%',
    //     borderRadius: 5,
    //     padding: 20,
    // },
    // modalHeader: {
    //     borderBottomWidth: 0.2,
    //     borderColor: '#e0e0e0',
    //     paddingBottom: 15,
    //     marginBottom: 15,
    // },
    // modalHeaderText: {
    //     fontSize: 16,
    //     fontFamily: 'Lexend',
    // },
    // modalBody: {
    //     marginBottom: 10,
    //     marginTop: 10,
    // },
    // input: {
    //     height: 40,
    //     borderColor: '#e0e0e0',
    //     borderWidth: 1,
    //     paddingHorizontal: 10,
    //     borderRadius: 5,
    //     fontFamily:'Lexend'
    // },
    // modalFooter: {
    //     flexDirection: 'row',
    //     justifyContent: 'flex-end',
    //     borderColor: '#e0e0e0',
    //     marginTop: 10
    // },
    // button: {
    //     backgroundColor: '#F7EE25',
    //     padding: 10,
    //     borderRadius: 5,
    //     marginLeft: 10,
    // },
    // buttonText: {
    //     color: '#2A318B',
    //     fontSize: 16,
    //     fontFamily: 'Lexend',
    // },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:15
    },
    modalBackdrop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 20,
    },
    modalHeader: {
        borderBottomWidth: 0.2,
        borderColor: '#e0e0e0',
        paddingBottom: 15,
        marginBottom: 15,
    },
    modalHeaderText: {
        fontSize: 16,
        fontFamily: 'Lexend',
    },
    modalBody: {
        marginBottom: 10,
        marginTop: 10,
    },
    input: {
        height: 40,
        borderColor: '#e0e0e0',
        borderWidth: 1,
        paddingHorizontal: 10,
        borderRadius: 5,
        fontFamily:'Lexend'
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderColor: '#e0e0e0',
        marginTop: 10
    },
    button: {
        backgroundColor: '#F7EE25',
        padding: 10,
        borderRadius: 5,
        marginLeft: 10,
    },
    buttonText: {
        color: '#2A318B',
        fontSize: 16,
        fontFamily: 'Lexend',
    },
})

const mapstateToProps = state => {
    return {

    }
};
  
const mapDispatchToProps = dispatch => {
    return {
       
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((Cart));

