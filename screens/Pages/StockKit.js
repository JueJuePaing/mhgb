import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import CButton from '../Components/CButton';
import BottomSheet from '../Components/BottomSheet';
import HeaderLabel from '../Components/HeaderLabel';

const device = Dimensions.get('window');

const Profile = (props) => {

    const [t] = useTranslation('common');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { colors } = useTheme();

    const handleOpenModal = () => {
      setIsModalVisible(true);
    };
  
    const handleCloseModal = () => {
      setIsModalVisible(false);
    };

    const addStock = () => {
        setIsModalVisible(false);
        props.navigation.navigate('AddStock');
    }
    
    const addStockByQr = () => {
        setIsModalVisible(false);
        props.navigation.navigate('Scanner');
    }

  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
        <BottomSheet visible={isModalVisible} onClose={handleCloseModal}>
            <View style={{marginTop:30}}/>
            <CButton 
                title={t('made_with_qr')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                action={() => addStockByQr() }
            />
            <View style={{marginTop:10}}/>
            <CButton 
                title={t('create_yourself')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#fff'}
                action={() => addStock() }
            />
            <View style={{marginTop:30}}/>
        </BottomSheet>
        <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
                <View style={styles.container}>
                    <HeaderLabel title={t('stock_kit')} />
                    <View style={{marginTop:30}}/>

                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity 
                            onPress={() => handleOpenModal() }
                            style={{
                                width: (device.width - 32) / 2.1,
                                backgroundColor:'#fff',
                                borderWidth:2,
                                borderRadius:9,
                                borderColor:'#F3F4F5',
                                overflow: 'hidden',
                                marginBottom: 15
                            }}
                            >
                            <Image source={require('../../assets/images/add_stock.png')} 
                                style={{width:'100%',height:130}}
                                resizeMode={'contain'}
                                />
                                <View style={{
                                    padding:10,
                                }}>
                                <Text style={{fontFamily:'Lexend-Medium',fontSize:14,alignSelf:'center',color:colors.mainLinkColor}}>
                                    + Add to stock
                                </Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity 
                            onPress={() => props.navigation.navigate('RemainStock')}
                            style={{
                                width: (device.width - 32) / 2.1,
                                backgroundColor:'#fff',
                                borderWidth:2,
                                borderRadius:9,
                                borderColor:'#F3F4F5',
                                overflow: 'hidden',
                                marginBottom: 15
                            }}
                            >
                            <Image source={require('../../assets/images/view_stock.png')} 
                                style={{width:'100%',height:130}}
                                resizeMode={'contain'}
                                />
                                <View style={{
                                    padding:10,
                                }}>
                                <Text style={{fontFamily:'Lexend-Medium',fontSize:14,alignSelf:'center',color:colors.mainLinkColor}}>
                                    View all stock
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity 
                            onPress={() => props.navigation.navigate('RemainStockOut')}
                            style={{
                                width: (device.width - 32) / 2.1,
                                backgroundColor:'#fff',
                                borderWidth:2,
                                borderRadius:9,
                                borderColor:'#F3F4F5',
                                overflow: 'hidden',
                                marginBottom: 15
                            }}
                            >
                            <Image source={require('../../assets/images/stock_out.png')} 
                                style={{width:'100%',height:130}}
                                resizeMode={'contain'}
                                />
                                <View style={{
                                    padding:10,
                                }}>
                                <Text style={{fontFamily:'Lexend-Medium',fontSize:14,alignSelf:'center',color:colors.mainLinkColor}}>
                                    Stockout
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
    }
})

export default Profile;