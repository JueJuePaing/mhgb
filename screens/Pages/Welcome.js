import React,{useState,useEffect} from 'react';
import { StyleSheet, Text, View, Dimensions,Image } from 'react-native';
import CButton from '../Components/CButton';
import LaunchBanner from '../Components/LaunchBanner';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import { isDevice } from 'expo-device';
import { Platform } from 'react-native';
import * as Notifications from 'expo-notifications';

const device = Dimensions.get('window');

const Welcome = (props) => {
  const { colors } = useTheme();
  const [t] = useTranslation('common');

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', async () => {
      await registerForPushNotificationsAsync();
    });
    return unsubscribe;
  }, [])

  async function registerForPushNotificationsAsync() {
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
    if (isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
    } else {
      alert('Must use physical device for Push Notifications');
    }
  }
  
  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',justifyContent:'center'}}>
      <View style={styles.container}>
        
        <LaunchBanner 
          imgUrl={require('../../assets/images/banner4.png')} 
          title={t('app_welcome')}
          body={t('want_some_help')}
        />
          
          <View style={{marginTop: 30}}/>

          <CButton title={t('will_sign_up')}
                  color={'#fff'}
                  fontFamily={'Pyidaungsu-Bold'}
                  width={device.width - 32}
                  height={50}
                  backgroundColor={colors.mainBtnColor}
                  action={() => props.navigation.navigate('Register') }/>
          
          <View style={{marginTop: 15}}/>

          <CButton title={t('will_sign_in')} 
                  color={'rgba(9, 15, 71, 0.75)'}
                  fontFamily={'Pyidaungsu-Bold'}
                  width={device.width - 32}
                  height={50}
                  backgroundColor={'#fff'}
                  action={() => props.navigation.navigate('Login') }/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: device.width - 32,
  },
  image: {
    width: device.width,
    height: 300,
  },
  header: {
    color: "#090F47",
    fontSize: 21,
    paddingTop: 30,
    fontFamily: "Pyidaungsu",
    textAlign:'center'
  },
  body: {
    color: "#222",
    fontSize: 17,
    textAlign:'center',
    fontFamily: "Pyidaungsu",
    opacity : 0.7,
    paddingTop: 20,
  }
});

export default Welcome

