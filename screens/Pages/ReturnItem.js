import React, { useState,useEffect,useRef } from 'react';
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, TextInput, TouchableOpacity, ScrollView, Modal } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import ErrorMessage from '../Components/ErrorMessage';
import { showAlert } from 'react-native-customisable-alert';
import DottedLine from '../Components/DottedLine';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const ReturnItem = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [brandName, setBrandName] = useState('');
    const [brandType, setBrandType] = useState('');
    const [size, setSize] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');

    const [brandNameFocused,setBrandNameFocused] = useState(false);
    const [brandTypeFocused,setBrandTypeFocused] = useState(false);
    const [sizeFocused,setSizeFocused] = useState(false);
    const [priceFocused,setPriceFocused] = useState(false);
    const [descriptionFocused,setDescriptionFocused] = useState(false);

    const [loading, setLoading] = useState(false);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);

    const [medicineModalVisible, setMedicineModalVisible] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState({});
    const [products, setProduct] = useState([]);
    const [searchText, setSearchText] = useState('');

    const [formErrors, setFormErrors] = useState({});

    let brandNameRef = React.createRef();
    let brandTypeRef = React.createRef();
    let sizeRef = React.createRef();
    let priceRef = React.createRef();
    let descriptionRef = React.createRef();

    useEffect(() => {
      getProduct()
    }, []);

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        getProduct();
      }, 150)
      return () => clearTimeout(delayDebounceFn)
    }, [searchText])

    const getProduct = async () => {
      let formdata = new FormData();
      searchText &&  formdata.append('search',searchText)
      formdata.append('per_page',100);
      try {
          const response = await ApiService(url.products, formdata, 'POST');
          if (response.code == '200') {
            setProduct(response.products);
          }else if (response.code == '422') {
            setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false); 
      }
    }

    useEffect(() => {
        setConfirmBtnDisble(!(selectedProduct && size && price && description));
    }, [selectedProduct,size,price,description]); 

    const clearFormFields = () => {
      setSelectedProduct('');
      setBrandName('');
      setBrandType('');
      setSize('');
      setPrice('');
      setDescription('');
    }
    
    const searchProduct = (value) => {
      setSearchText(value);
    }

    const closeModal = () => {
      setMedicineModalVisible(false);
      setSearchText('');
    }

    const requestReturnItem = async () => {
        setLoading(true); 
        setFormErrors({});
        
        let formdata = new FormData();
        formdata.append('medicine_name',selectedProduct.id);
        // formdata.append('brand_name',brandName);
        // formdata.append('type',brandType);
        formdata.append('size',size);
        formdata.append('price',price);
        formdata.append('description',description);

        try {
            const response = await ApiService(url.return_product, formdata, 'POST');
            if (response.code == '200') {
              clearFormFields();
              props.navigation.push('OK',{
                'image' : require('../../assets/images/return_confirm.png'),
                'title' : t('request_receive'),
                'btnTitle' : t('back_to_home'),
                'description' : t('thank_for_return_item')
              })
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false); 
        }
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
            { loading && <Loading/> }
            <NavigationHeader  action={() => props.navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                    keyboardShouldPersistTaps='handled'
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}>
                <View style={styles.container}> 
                <View style={styles.body}>
                    <HeaderLabel title={t('return_item')} />
                    <View style={{marginTop:30}}/>

                    <InputLabel title={t('medicine_name')} />
                    <TouchableOpacity onPress={() => setMedicineModalVisible(true) }>
                      <View style={{paddingTop:15}}>
                          <Text style={styles.modalInput}>
                            {selectedProduct.name}
                          </Text>
                          <View style={{
                            borderBottomColor: 'rgba(9, 15, 71, 0.1)',
                            borderBottomWidth: 1,
                            color:'#000',
                          }} />
                      </View>
                      {formErrors.city && (
                          <ErrorMessage message={formErrors.city[0]} />
                      )}
                    </TouchableOpacity>
                    {formErrors.medicine_name && (
                        <ErrorMessage message={formErrors.medicine_name[0]} />
                    )}

                      <Modal
                          visible={medicineModalVisible}
                          onRequestClose={() => closeModal()}
                          animationType="slide"
                          transparent={true}>
                        <View style={styles.modalBackdrop}>
                        <View style={styles.modalContent}>
                            <View style={styles.modalHeader}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                              <Text style={styles.modalHeaderText}>
                                {t('medicine_name')}
                              </Text>
                              <TouchableOpacity onPress={() => closeModal()}>
                              <Text style={{fontFamily:'Lexend',color:'gray',fontSize:22}}>
                                X
                              </Text>
                            </TouchableOpacity>
                            </View>
                            <TextInput
                                style={styles.searchInput}
                                placeholder="Search"
                                value={searchText}
                                onChangeText={text => searchProduct(text) }
                            />
                            </View>
                            <View style={styles.modalBody}>
                            <ScrollView
                              keyboardShouldPersistTaps={'handled'}>
                              {products && products.map((product) => (
                                <TouchableOpacity
                                  key={product.id}
                                  onPress={() => {
                                    setMedicineModalVisible(false);
                                    setSelectedProduct(product);
                                    setSearchText('');
                                  }}
                                >
                                  <Text style={{paddingVertical : 7, color:'#000',fontFamily:'Lexend',paddingHorizontal:10, fontSize : 13}}>{product.name}</Text>
                                  <DottedLine/>
                                </TouchableOpacity>
                              ))}
                            </ScrollView>
                            </View>
                        </View>
                        </View>
                    </Modal>
                    
                    <View style={{marginTop:25}}/>  
                        <InputLabel title={t('size')} />
                        <TextInput keyboardType="default"
                            ref={sizeRef}
                            maxLength={50}
                            onChangeText={(size) => setSize(size) }
                            onFocus={() => setSizeFocused(true) }
                            onBlur={() => setSizeFocused(false) }
                            value={size}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() => priceRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: sizeFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                        {formErrors.size && (
                            <ErrorMessage message={formErrors.size[0]} />
                        )}
                    <View style={{marginTop:25}}/>  
                        <InputLabel title={t('price')} />
                        <TextInput keyboardType="numeric"
                            ref={priceRef}
                            maxLength={50}
                            onChangeText={(price) => setPrice(price) }
                            onFocus={() => setPriceFocused(true) }
                            onBlur={() => setPriceFocused(false) }
                            value={price}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() => descriptionRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: priceFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                        {formErrors.price && (
                            <ErrorMessage message={formErrors.price[0]} />
                        )}
                    <View style={{marginTop:25}}/>  
                        <InputLabel title={t('description')} />
                        <TextInput keyboardType="default"
                            ref={descriptionRef}
                            maxLength={50}
                            onChangeText={(description) => setDescription(description) }
                            onFocus={() => setDescriptionFocused(true) }
                            onBlur={() => setDescriptionFocused(false) }
                            value={description}
                            selectionColor={colors.inputActiveColor}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: descriptionFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                              }]} />
                        {formErrors.description && (
                            <ErrorMessage message={formErrors.description[0]} />
                        )}
                </View>
            </View>
            </ScrollView>      
            <View style={{flexDirection:'row',marginTop:15,marginBottom:isIos ? 15 : 0}}>
                <CButton title={t('will_give_send')} 
                                color={colors.mainLinkColor}
                                fontFamily={'Pyidaungsu-Bold'}
                                width={device.width - 32}
                                height={50}
                                backgroundColor={'#F7EE25'}
                                disabled={confirmBtnDisble}
                                action={() =>  requestReturnItem() }/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    footer: {
      marginTop: 15,
      marginBottom: 15,
      alignSelf: 'center'
    },
    input: {
      height: 35,
      borderBottomWidth: 1,
      borderRadius:8,
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
    modalInput : {
      borderRadius:8,
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
    modalBackdrop: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 10,
    },
    modalHeader: {
      borderBottomWidth: 1,
      borderColor: '#e0e0e0',
      paddingBottom: 10,
      marginBottom: 10,
    },
    modalHeaderText: {
      fontSize: 15,
      fontFamily: 'Lexend',
      paddingVertical : 5
    },
    modalBody: {
      marginBottom: 10,
      height: 400
    },
    searchInput: {
      height: 40,
      borderWidth: 1,
      borderColor: 'gray',
      borderRadius: 5,
      paddingHorizontal: 10,
      marginTop: 10,
      fontFamily: 'Lexend'
    },
  })

export default ReturnItem;
