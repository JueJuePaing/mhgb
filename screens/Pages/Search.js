import React, { useState,useEffect } from 'react'
import { View, Text, StyleSheet, TextInput, Dimensions, Image, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import VerticalProducts from '../Components/VerticalProducts';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import { showAlert } from 'react-native-customisable-alert';
import {useTranslation} from "react-i18next";

const device = Dimensions.get('window');

const Search = (props) => {
  const [t] = useTranslation('common');
  const [products, setProduct] = useState([])
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [noResult, setNoResult] = useState(false);
  const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);

  useEffect(() => {
    setConfirmBtnDisble(!(search));
  }, [search]); 

  const handleBackPress = () => {
    props.navigation.goBack();
  };

  useEffect(() => {
    if(page > 1){
      findProduct(page)
    }
  }, [page]);

  const searchProduct = () => {
    setLoading(true);
    setPage(1);
    setProduct([]);
    findProduct(1);
  }

  const findProduct = async (noPage) => {

    let formdata = new FormData();
    formdata.append('search',search);
    formdata.append('page', noPage);

    try {
        const response = await ApiService(url.products, formdata , 'POST');

        if (response.code == '200') {
              if(response.products.length > 0){
                setNoResult(false);
                if (noPage === 1) {
                  setProduct(response.products);
                } else {
                  setProduct(products.concat(response.products));
                }
              } else if (noPage === 1){
                setNoResult(true);
              }

              if(response.next_pages != ''){
                setLoadMore(true);
              }else{
                setLoadMore(false);
              }
              
        }else if (response.code == '422') {
            setFormErrors(response.errors);
        }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
        }
    } catch (error) {
        console.error(error);
    } finally {
      setLoading(false); 
    }
  }

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
  }

  return (
    <View style={styles.container}>
      {loading && <Loading/>}
      <View style={styles.header}>
        <TouchableOpacity 
          onPress={() => handleBackPress()}>
          <Image
            source={require('../../assets/images/back.png')}
            tintColor={'#090F47'}
            style={styles.backButton}
          />
        </TouchableOpacity>
        <TextInput
          style={styles.searchBox}
          placeholder={t('search_products')}
          placeholderTextColor="#A6A6A6"
          autoFocus={true}
          value={search}
          onChangeText={(value) => {
              setSearch(value);
              setNoResult(false);
          }}
          onSubmitEditing={search && searchProduct}
        />
        <Ionicons
          name="search"
          size={20}
          color="black"
          disabled={confirmBtnDisble}
          onPress={searchProduct}
        />
      </View>
      <View style={styles.itemcontainer}>
      {
        ! noResult ? 
        <VerticalProducts 
          items={products} 
          navigation={props.navigation} 
          header={null}
          onEndReached={handleLoadMore}        
        />
        :
        <Text style={{fontFamily:'Lexend',fontSize:16,alignSelf:'center'}}>
          {t('no_result')}
        </Text>
      }
     
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems:'center',
    flex: 1,
    backgroundColor: '#F5F5F5',
    marginTop: getStatusBarHeight(),
  },
  backButton: {
    width: 25,
    height: 25,
    marginLeft: 10,
  },
  itemcontainer: {
    flex:1,
    width:device.width - 32,
    marginTop: 30,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 56,
    elevation: 2,
    paddingHorizontal: 16,
  },
  searchBox: {
    flex: 1,
    marginLeft: 16,
    marginRight: 16,
    fontSize: 14.5,
    fontFamily: 'Lexend',
    color: '#000',
  },
});

export default Search;
