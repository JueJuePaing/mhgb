import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";

import GapItem from '../Components/GapItem';
import CButton from '../Components/CButton';
import IndeButton from '../Components/IndeButton';
import { showAlert } from 'react-native-customisable-alert';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import LoadingView from '../Components/LoadingView';
import { Carousel, Pagination } from 'react-native-snap-carousel';
import ImageLoad from 'react-native-image-placeholder';
import numeral from 'numeral';
import ErrorMessage from '../Components/ErrorMessage';
import { connect } from 'react-redux';
import {
  setCartCount,
  addToCart
} from '../../stores/actions';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const ProductDetail = (props) => {
  const { colors } = useTheme();
  const [t] = useTranslation('common');
  const [qty, setQty] = useState(1);
  const [product, setProduct] = useState([]);
  const { productId } = props.route.params;
  const [loading, setLoading] = useState(false);
  const [outOfStock,setOutOfStock] = useState(false);
  const [reachProductLimit,setReachProductLimit] = useState(false);
  const [viewLoading, setViewLoading] = useState(false);
  const [qtyError, setQtyError] = useState(null);
  const [index, setIndex] = useState(0)
  const isCarousel = React.useRef(null)

  useEffect(() => {
    getProduct();
  }, []); 

  useEffect(() => {
    if (product.stock_quantity === 0) {
      setOutOfStock(true);
    } else if(product.available_quantity == 0){
      setReachProductLimit(true);
    }
  }, [product]); 

  const getProduct = async () => {

    setViewLoading(true);

    let formdata = new FormData();
    formdata.append('product_id',productId);

    try {
        const response = await ApiService(url.product_detail, formdata, 'POST');

        if (response.code == '200') {
          setProduct(response.products);
        }else if (response.code == '422') {

        }else if (response.code == '401' || response.code == '400') {
            showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: true,
            });
        }
    } catch (error) {
        console.error(error);
    } finally {
      setViewLoading(false);
    }
  }

  const getCartCount = async() => {
    try {
      const response = await ApiService(url.cart_count, [], 'POST');
      if (response.code == '200') {
        props.setCartCount(response.cart_count);
      } else if (response.code == '422') {
        setFormErrors(response.errors);
      } else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    }
    finally {
      setLoading(false); 
    }
  }

  const increaseQty = (qty) => {
    if(qty >= product.available_quantity && product.available_quantity != null){
      setQtyError(t('reach_product_limit'));
      return;
    }
    setQtyError(null);
    setQty(qty+1)
  }

  const decreaseQty = (qty) => {
    setQtyError(null);
    setQty(qty > 1 ? qty - 1 : 1)
  } 

  const requestAddCart = async() => {

    setLoading(true); 

    // props.addToCart(product);

    // setLoading(false);

    let formdata = new FormData();
    formdata.append('product_id',productId);
    formdata.append('quantity',qty);

    try {
        const response = await ApiService(url.add_to_cart, formdata, 'POST');

        if (response.code == '200') {
          getCartCount();
          showAlert({
            title: t('success'),
            message: t('add_cart_successful'),
            alertType: 'success',
            dismissable: true,
            onPress: () => {
              // props.navigation.goBack();
            }
          });
        }else if (response.code == '422') {
          showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: false,
            });
        }else if (response.code == '401' || response.code == '400') {
            showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: true,
            });
        }
    } catch (error) {
        console.error(error);
    } finally {
      setLoading(false);
    }
  }

  const renderItem = (item) => {
    return (
      <ImageLoad 
          isShowActivity={false}
          source={{uri:item.item}} 
          style={styles.image}
          resizeMode={'contain'}
      />
    )
  }

  const handleQtyChange = (value) => {
    if (isNaN(value)) {
      return value;
    }else{
      const numericValue = value.trim().replace(/[^0-9]/g, '');
      setQty(numericValue === '' ? '' : Number(numericValue));
    }
  }

  const handleQtyBlur = () => {
    if(qty == 0 || qty == ''){
      setQty(1)
    }

    if(qty >= product.available_quantity  && product.available_quantity != null){
      setQtyError(t('reach_product_limit'));
      setQty(product.available_quantity)
    }
    setTimeout(() => {
      setQtyError(null);
    }, 2000);
  }
  
  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loading && <Loading/> }
        <NavigationHeader showCart={true} 
                        cartAction={() => props.navigation.navigate('Cart')}
                        action={() => props.navigation.goBack()} 
                        />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
        <View style={styles.container}>
          { viewLoading && <LoadingView/> }
            <Text style={{fontFamily:'Lexend',fontSize:16,color:colors.mainTextColor}}>
                {product.name}
            </Text>
            <Text style={{fontFamily:'Lexend-Light',fontSize:13.5,color:'gray',marginTop:5}}>
                {product.brand_name}
            </Text>
            <View style={{alignItems:'center',marginTop:20}}>
              <Carousel
                  ref={isCarousel}
                  data={product.gallery}
                  renderItem={renderItem}
                  sliderWidth={device.width}
                  itemWidth={device.width - 32}
                  onSnapToItem={(index) => setIndex(index)}
                  layout="default"
                  loop
                />
              {
                product.gallery && product.gallery.length < 2 &&
                <View style={{ marginBottom:20 }}/>
              }
              {
                product.gallery &&
                <Pagination
                  dotsLength={product.gallery.length}
                  activeDotIndex={index}
                  carouselRef={isCarousel}
                  dotStyle={{
                    width: 6,
                    height: 6,
                    borderRadius: 3,
                    backgroundColor: 'rgba(0, 0, 0, 0.92)',
                  }}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  tappableDots={true}
                />
              }
              
            </View>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <View>
              <Text style={{fontFamily:'Lexend-Medium',fontSize:17,color:colors.mainTextColor}}>
                {product.price ? `${numeral(product.price * qty).format('0,0')} MMK` : 'Loading...'}
              </Text>
              </View>
              { outOfStock ?
              <Text style={{fontFamily:'Lexend',color:'red',fontSize:13}}>
                {t('out_of_stock')}
              </Text>
              : reachProductLimit ? 
                <Text style={{fontFamily:'Lexend',color:'red',fontSize:13}}>
                {t('reach_product_limit')}
                </Text>
              :
                <IndeButton 
                    increment={() => increaseQty(qty) } 
                    decrement={()=> decreaseQty(qty)}
                    onBlur={() => handleQtyBlur()}
                    updateQty={(value) => handleQtyChange(value) }
                    qty={qty}
                />
              }
            </View>
            <View style={{alignSelf:'flex-end',marginTop:3}}>
                {
                  qtyError && (
                    <ErrorMessage message={qtyError} />
                  )
                }
              </View>
            <GapItem/>
            <View style={styles.productDetail}>
              <View style={styles.title}>
                <Text style={{fontFamily:'Pyidaungsu',fontSize:14,paddingBottom:10}}>
                  {t('price_and_size')}
                </Text>
                <View style={styles.productBox}>
                  <Text style={{fontFamily:'Lexend-Bold',color:'#FFA41B',fontSize:15}}>{numeral(product.price).format('0,0')} MMK</Text>
                  <Text style={{fontFamily:'Lexend-Light',color:'#FFA41B',fontSize:14}}>
                    {product.size} {product.size_unit}
                  </Text>
                </View>
              </View>
              <View style={{marginTop:30}}/>
              <View style={styles.description}>
                <Text style={{fontFamily:'Pyidaungsu',fontSize:14,paddingBottom:10}}>
                  {t('product_detail')}
                </Text>
                <Text style={{fontFamily:'Pyidaungsu',color:'gray',fontSize:14,alignSelf:'flex-start'}}>
                  {product.description ?? t('no_detail_description')}
                </Text>
              </View>
            </View>
        </View>
        </ScrollView>
        <View style={{marginTop:15,marginBottom:isIos ? 15 : 0}}>
          <CButton title={
                      outOfStock ? 
                      t('out_of_stock')
                      : reachProductLimit ? 
                      t('reach_product_limit') :
                     t('go_to_cart')
                    } 
                    color={colors.mainLinkColor}
                    fontFamily={'Pyidaungsu'}
                    width={device.width - 32}
                    height={50}
                    disabled={outOfStock || reachProductLimit}
                    backgroundColor={'#F7EE25'}
                    action={() => requestAddCart() }
          />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    title: {
      marginTop: 5
    },
    description: {
      marginTop: 5
    },
    image: {
      width: '100%',
      height: 150,
      borderRadius: 10,
    },
    productBox: {
      borderColor: '#FFA41B',
      borderWidth: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: 'rgba(255, 164, 27, 0.05)',
      height:40,
      borderRadius:6,
      alignItems: 'center',
      paddingLeft: 6,
      paddingRight: 6
    }
  })

const mapstateToProps = state => {
    return {
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
      setCartCount: cart_count => {
        dispatch(setCartCount(cart_count))
      },
      addToCart: product => {
        dispatch(addToCart(product))
      }
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((ProductDetail));
