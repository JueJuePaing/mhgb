import React,{ useContext, useState,useEffect } from 'react';
import { connect } from 'react-redux';
import { View, Image, Text, StyleSheet, Dimensions,TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Spacer from '../Components/Spacer';
import ErrorMessage from '../Components/ErrorMessage';
import InputLabel from '../Components/InputLabel';
import CButton from '../Components/CButton';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import { useTheme } from '@react-navigation/native';
import NavigationHeader from '../Components/NavigationHeader';
import HeaderLabel from '../Components/HeaderLabel';
import {useTranslation} from "react-i18next";
import { setProfile } from '../../stores/actions';
import * as ImagePicker from 'expo-image-picker';
import mime from "mime";
import { showAlert } from 'react-native-customisable-alert';
import { AppContext } from '../../context/AppContextProvider';
const device = Dimensions.get('window');
import { ImageManipulator } from 'expo-image-manipulator';


const EditProfile = (props) => {

    const {changeProfileData, profileData} = useContext(AppContext);

    const [loading, setLoading] = useState(false);
    const [shopName, setShopName] = useState('');
    const [address, setAddress] = useState('');
    const [logo, setLogo] = useState(null);
    const [image, setImage] = useState(null);

    const [formErrors, setFormErrors] = useState({});
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(false);
    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const [addressFocused,setAddressFocused] = useState(false);
    const [shopNameFocused,setShopNameFocused] = useState(false);
    let addressRef = React.createRef();

    useEffect(() => {
        if (profileData) {
            setShopName(profileData.name);
            setAddress(profileData.address);
            setLogo(profileData.logo);
        }
    }, [profileData])

    useEffect(() => {
        setConfirmBtnDisble(!(shopName && address));
    }, [shopName,address]); 

    const handleImagePicker = async () => {

        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
            console.log('Permission denied');
            setLoading(false);
          return;
        }
    
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: false,
          quality: 0.5
        });
    
        if (!result.canceled) {
            const { uri } = result.assets[0];
                setImage(uri);
                setLogo(uri);
        }
    };

    const generateRandomString = () => {
        return Math.random().toString(36).substring(2, 16);
    }

    const requestEdit = async() => {
        setLoading(true);
        let retry = true; 

        let formdata = new FormData();
        formdata.append('name',shopName);
        formdata.append('address',address);

        image && formdata.append('logo', {
            type: mime.getType(image), 
            uri: image, 
            name: `${generateRandomString()}.${mime.getType(image).split("/")[1]}`
        });
        try {   
            const response = await ApiService(url.edit_profile, formdata, 'POST');
            if (response.code == '200') {
                console.log(response.detail)
                props.setProfile(response.detail);
                changeProfileData(response.detail);
                props.navigation.goBack();
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            if (retry) {
                retry = false; 
                await requestEdit();
            }else{
                showAlert({
                    title: `Oops!`,
                    message: 'Netwok request failed.Please try again!',
                    alertType: 'error',
                    dismissable: true,
                  });
            }
        } finally {
            setLoading(false);
        }
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
            <NavigationHeader action={() => props.navigation.goBack()} />
            { loading && <Loading/> }
            <ScrollView keyboardShouldPersistTaps='handled'>
                <HeaderLabel title={t('account_detail')} />
                <View style={styles.formView}>
                    {formErrors.logo && (
                        <ErrorMessage message={formErrors.logo[0]} />
                    )}
                    <TouchableOpacity 
                        onPress={() => handleImagePicker() }
                        style={styles.imageContainer}>
                        <View style={styles.iconBackground}>
                            {
                                logo ? 
                                <Image 
                                    resizeMode='contain'
                                    source={{ uri: logo }}
                                    style={{ width: 90, height: 90, borderRadius: 45}} />
                                :
                                <Image 
                                    resizeMode='contain'
                                    source={require('../../assets/images/default_user.png')}
                                    style={{ width: 90, height: 90, borderRadius: 45}} />
                            }
                        </View>
                    </TouchableOpacity>
                        <InputLabel title={t('shop_name')} /> 
                        <TextInput keyboardType="default"
                            maxLength={50}
                            onChangeText={(value) => setShopName(value) }
                            onFocus={() => setShopNameFocused(true)}
                            onBlur={() => setShopNameFocused(false)}
                            value={shopName}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() =>  addressRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: shopNameFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                         {formErrors.name && (
                            <ErrorMessage message={formErrors.name[0]} />
                        )}
                        
                        <View style={{marginTop:25}}/>

                        <InputLabel title={t('address')} />
                        <TextInput keyboardType="default"
                                ref={addressRef}
                                maxLength={255}
                                onChangeText={(value) => setAddress(value) }
                                onFocus={() => setAddressFocused(true)}
                                onBlur={() => setAddressFocused(false)}
                                value={address}
                                selectionColor={colors.inputActiveColor}
                                multiline={true}
                                numberOfLines={4}
                                style={[styles.input,{
                                    height:80,
                                    color:'#000',
                                    borderColor:'#000',
                                    borderBottomColor: addressFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                                }]} />
                            {formErrors.shipping_address && (
                                <ErrorMessage message={formErrors.shipping_address[0]} />
                            )}
                </View>
            <Spacer marginTop={35} />
            <CButton
                title="ပြင်ဆင်မယ်"
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}   
                color={colors.mainLinkColor}
                fontFamily="Pyidaungsu"
                disabled={confirmBtnDisble}
                action={() => requestEdit() }
            />
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    imageContainer: {
        width: 110,
        height: 110,
        borderRadius: 55,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 3,
        borderColor: '#F9DD5A',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.7,
        shadowRadius: 7,
        elevation: 10,
        alignSelf:'center',
        marginBottom: 30
    },
    iconBackground: {
        width: 75,
        height: 75,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backButton: {
        fontFamily: 'Lexend',
        fontSize: 16,
        color: '#090F47',
        borderWidth: 1,
        borderColor: '#090F47',
        borderRadius: 12,
        padding: 10,
        textAlign: 'center',
    },
    nameText: {
        fontFamily:'Lexend',
        fontSize:18,
        textAlign:'center'
    },
    idText: {
        fontFamily:'Lexend',
        fontSize:18,
        marginTop:20,
        borderWidth: 1,
        borderColor: '#F9DD5A',
        borderRadius: 12,
        padding: 10,
        textAlign: 'center',
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:16,
        fontFamily:'Pyidaungsu',
        color: '#000',
        borderColor:'#000',
        borderBottomColor: 'rgba(9, 15, 71, 0.1)'
    },
    formView:{
        width:device.width - 32,
        justifyContent: 'center',
        marginTop: 30
    },
});


const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setProfile: profile => {
          dispatch(setProfile(profile))
      },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((EditProfile));
