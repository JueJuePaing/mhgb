import React, { useState,useEffect,Fragment, useMemo } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, TextInput, TouchableOpacity, ScrollView, Modal, Keyboard } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import IndeButton from '../Components/IndeButton';
import GapItem from '../Components/GapItem';
import CButton from '../Components/CButton';
import { showAlert,closeAlert } from "react-native-customisable-alert";
import { ApiService } from '../../network/service';
import url from '../../network/url';
import ErrorMessage from '../Components/ErrorMessage';
import Loading from '../Components/Loading';
import LoadingView from '../Components/LoadingView';
import { connect } from 'react-redux';
import { setCartCount } from '../../stores/actions';
import { formatTranMoney } from '../../configs/helper';

const device = Dimensions.get('window');
const secondColumnWidth = device.width - 130;
const isIos = Platform.OS == 'ios';

const Cart = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const [info,setInfo] = useState([])
    const [totalQty,setTotalQty] = useState(0);
    const [totalDiscount,setTotalDiscount] = useState(0);
    const [orderTotal,setOrderTotal] = useState(0);
    const [totalAmount,setTotalAmount] = useState(0);
    const [modalVisible, setModalVisible] = useState(false);
    const [couponCode, setCouponCode] = useState('');
    const [formErrors, setFormErrors] = useState({});
    const [isCouponValid, setIsCouponValid] = useState(false);
    const [couponDiscount, setCouponDiscount] = useState({});
    const [loading, setLoading] = useState(false);
    const [viewLoading, setViewLoading] = useState(false);
    const [shippingFee, setShippingFee] = useState(0);
    const [selectedQuantity, setSelectedQuantity] = useState(0);

    const disableBtn = useMemo(() => {
        if (info?.length > 0) {
            const disabledItem = info.some(item => item.stock_quantity === 0);
            return disabledItem;
        } else {
            return false;
        }
      }, [info]);
    
    useEffect(() => {
        const unsubscribe = props.navigation.addListener ('focus', async () =>{
            getCartList();
            setFormErrors({});
            getCartCount();
        })
        return unsubscribe;
    }, [])

    const getCartCount = async() => {
        try {
          const response = await ApiService(url.cart_count, [], 'POST');
          if (response.code == '200') {
            props.setCartCount(response.cart_count);
          } else if (response.code == '422') {
            setFormErrors(response.errors);
          } else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        }
        finally {
          setLoading(false); 
        }
    }

    const getCartList = async () => {
        setViewLoading(true);
        try {
            const response = await ApiService(url.cart_list, [], 'POST');
          
            if (response.code == '200') {
                reRenderData(response);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
        }
    }

    const reRenderData = (response) => {
        setInfo(response.cart_detail);
        setTotalAmount(response.net_amount);
        setOrderTotal(response.total_product_amount);
        setTotalDiscount(response.total_product_discount);
        setTotalQty(response.product_count);
        setCouponDiscount(response.coupon_discount);
        setIsCouponValid(response.is_coupon_valid);
        setShippingFee(response.shipping_fee);
    }

    const increaseQty = async(productId) => {
        setFormErrors({});
        let formdata = new FormData();
        formdata.append('product_id',productId);

        try {
            const response = await ApiService(url.cart_increase, formdata, 'POST');
            if (response.code == '200') {
                reRenderData(response);
            }
            else if (response.code == '422') {
                setFormErrors(response.errors);
                setTimeout(() => {
                    setFormErrors({});
                }, 1500);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }

    }

    const decreaseQty = async(productId,quantity) => {
        if(quantity == 1){
            return;
        }
        setFormErrors({});
        let formdata = new FormData();
        formdata.append('product_id',productId);

        try {
            const response = await ApiService(url.cart_decrease, formdata, 'POST');
            if (response.code == '200') {
                reRenderData(response);
            }
            else if (response.code == '422') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
                setTimeout(() => {
                    setFormErrors({});
                }, 1500);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const requestItemRemove = (productId) => {
        showAlert({
            title: t('remove_product_title'),
            message: t('remove_product_desc'),
            alertType: 'warning',
            onPress: () => confirmItemRemove(productId)
      });
    }

    const confirmItemRemove = async (productId) => {
        closeAlert();
        setFormErrors({});
        setLoading(true);
        let formdata = new FormData();
        formdata.append('product_id',productId);

        try {
            const response = await ApiService(url.cart_remove, formdata, 'POST');
            if (response.code == '200') {
                getCartCount()
                reRenderData(response);
            }
            else if (response.code == '422') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {

        }
    }

    const requestCouponCheck = async () => {

        setFormErrors({});
        try {

            let formdata = new FormData();
            formdata.append('coupon_number',couponCode);

            const response = await ApiService(url.coupon_check, formdata, 'POST');
            if (response.code == '200') {
                reRenderData(response);
                setModalVisible(false);
            }
            else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {

        }
    }   

    const closeModal = () => {
        setFormErrors({});
        setCouponCode('');
        setModalVisible(false);
    }

    const confirmRemoveCoupon = async() => {
        setFormErrors({});
        try {
            const response = await ApiService(url.remove_coupon, [], 'POST');

            if (response.code == '200') {
                reRenderData(response);
                closeAlert()
            }else if (response.code == '422') {

            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {

        }
    }

    const removeCoupon = () => {
        showAlert({
            title: t('remove_coupon_title'),
            message: t('remove_product_desc'),
            alertType: 'warning',
            onPress: () => confirmRemoveCoupon()
      });
    }

    const openCouponBox = () => {
        setFormErrors({})
        setCouponCode('')
        setModalVisible(true)
    }

    const goToCheckout = () => {
        setFormErrors({})
        props.navigation.navigate('Checkout');
    }

    const handleBlurQuantity = async(productId,limit) => { 
        if(!selectedQuantity){
            return;
        }
        setFormErrors({});
        setLoading(true);
        let formdata = new FormData();
        formdata.append('product_id',productId);
        formdata.append('quantity', selectedQuantity);

        try {
            const response = await ApiService(url.cart_quantity_update, formdata, 'POST');
            if (response.code == '200') {
                reRenderData(response);
            }
            else if (response.code == '422') {
                setFormErrors(response.errors);
                reRenderData(response.errors);
                setTimeout(() => {
                    setFormErrors({});
                }, 1500);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
            setSelectedQuantity(0);
        }
    }

    const goHome = () => {
        props.navigation.reset({
            index: 0,
            routes: [{ name: 'TabNav' }],
        });
    }

    const handleQtyChange = (itemId, value, limit) => {
        if (/^[-0]/.test(value)) {
          return value;
        } else {
          const numericValue = value.trim().replace(/[^0-9]/g, '');
      
          const updatedInfo = info.map(obj => {
            if (obj.id === itemId) {
              return { ...obj, quantity: numericValue };
            }
            return obj;
          });
          setInfo(updatedInfo);
          setSelectedQuantity(numericValue);
        }
      }
      

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { loading && <Loading/> }
            <NavigationHeader hideBackButton={
                                props.route.params ? props.route.params.hide_back_btn : false
                            } 
                            title={t('your_cart')}  
                            action={() => props.navigation.goBack()} />
            <ScrollView 
                showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
                <View style={styles.container}>
                    { viewLoading ?
                     <LoadingView/> 
                     :
                    <Fragment>
                        {
                            info.length == 0 ?
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../assets/images/empty_cart.png')} 
                                    resizeMode={'contain'} 
                                    style={{width:125,height:125}}
                                />
                                {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16,marginTop:50}}>
                                    Whoops
                                </Text> */}
                                <Text style={{
                                    marginTop: 30,
                                    fontFamily: 'Pyidaungsu',
                                    fontSize : 14.5
                                }}>
                                    {t('no_items_in_cart')}
                                </Text>
                            </View>
                            :
                            <View>
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <Text style={{fontFamily:'Lexend',color:'gray',fontSize:14}}>{totalQty} items in your cart</Text>
                                    <TouchableOpacity onPress={() => goHome()}>
                                    <Text style={{fontFamily:'Lexend',color:'#4157FF',fontSize:14}}>
                                        {t('continue_shopping')}
                                    </Text>
                                    </TouchableOpacity>
                                </View>
                                <Fragment>
                                    <View style={{marginTop:25}}>
                                    {
                                        info.map((item,index) => {
                                        return (
                                            <View key={item.id}>
                                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                    <View style={{paddingRight:5}}>
                                                        <Image 
                                                        source={{uri: item.feature_image}} 
                                                        style={{width:70,height:70,borderRadius:12}}
                                                        resizeMode={'contain'}
                                                        />
                                                    </View>
                                                    <View style={{width:secondColumnWidth}}>
                                                        <View style={{
                                                                flexDirection:'row',
                                                                justifyContent:'space-between'
                                                            }}>
                                                            <View style={{width:secondColumnWidth/1.3}}>
                                                                <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                                                                    {item.name}
                                                                </Text>
                                                                <Text style={{fontFamily:'Lexend-Light',fontSize:13,color:'gray'}}>
                                                                    {item.size} {item.size_unit}
                                                                </Text>
                                                            </View>
                                                            <TouchableOpacity onPress={() => requestItemRemove(item.id) }>
                                                                <Image 
                                                                    source={require('../../assets/images/close.png')} 
                                                                    style={{width:22,height:22,resizeMode:'contain',borderRadius:11}}/>
                                                            </TouchableOpacity>
                                                        </View>

                                                        <View style={{flexDirection:'row',marginTop:8,justifyContent:'space-between'}}>
                                                            <Text style={{fontFamily:'Lexend',fontSize:14,color:colors.mainTextColor}}>
                                                                {formatTranMoney(item.price)} MMK
                                                            </Text>
                                                            <IndeButton increment={() => increaseQty(item.id) } 
                                                                        decrement={()=> decreaseQty(item.id,item.quantity)}
                                                                        qty={item.quantity}
                                                                        onBlur={() => handleBlurQuantity(item.id,item.limit)}
                                                                        updateQty={(value) => handleQtyChange(item.id,value,item.limit) }
                                                                        />
                                                        </View>
                                                        <View style={{alignSelf:'flex-end',marginTop:3}}>
                                                        {
                                                            item.stock_quantity === 0 ? 
                                                                <ErrorMessage message={t('out_of_stock')} /> : 
                                                            <>
                                                                {
                                                                    formErrors.product_id === item.id ? (
                                                                        <ErrorMessage message={`max to ${item.limit}`} />
                                                                    )
                                                                    :
                                                                    (
                                                                        item.limit != null &&
                                                                        (
                                                                            <Text style={{fontFamily:'Lexend-Light',fontSize:11,color:'gray'}}>
                                                                                max to {item.limit}
                                                                            </Text>
                                                                        )
                                                                    )
                                                                }
                                                            </>
                                                        }
                                                        
                                                        </View>
                                                    </View>
                                                </View>
                                                <GapItem/>
                                            </View>
                                        )
                                        })
                                    }
                                    </View>
                                    <View>
                                        <Text style={[styles.title,{color: colors.mainTextColor}]}>
                                            {t('payment')} 
                                        </Text>
                                        <View>
                                            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                                <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                    {t('order_total')}
                                                </Text>
                                                <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {formatTranMoney(orderTotal)} MMK
                                                </Text>
                                            </View>
                                            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                                <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                    {t('items_discount')}
                                                </Text>
                                                <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {formatTranMoney(totalDiscount)} MMK
                                                </Text>
                                            </View>
                                            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                                <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                    {t('coupon_discount')}
                                                </Text>
                                                {
                                                    isCouponValid ? 
                                                    <TouchableOpacity onPress={() => removeCoupon() } >
                                                        <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend',textDecorationLine:'underline'}}>
                                                            {formatTranMoney(couponDiscount)} MMK
                                                        </Text>
                                                    </TouchableOpacity>
                                                    :
                                                    <TouchableOpacity 
                                                        onPress={() => openCouponBox() }>
                                                        <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend',textDecorationLine:'underline'}}>
                                                            {t('add_coupon')}
                                                        </Text>
                                                    </TouchableOpacity>
                                                }
                                            
                                                <Modal
                                                    visible={modalVisible}
                                                    onRequestClose={() => setModalVisible(false)}
                                                    animationType="slide"
                                                    transparent={true}
                                                >
                                                    <View style={styles.modalBackdrop}>
                                                    <View style={styles.modalContent}>
                                                        <View style={styles.modalHeader}>
                                                        <Text style={styles.modalHeaderText}>{t('coupon_code')}</Text>
                                                        </View>
                                                        <View style={styles.modalBody}>
                                                        <TextInput
                                                            placeholder={t('enter_coupon_code')}
                                                            value={couponCode}
                                                            onChangeText={setCouponCode}
                                                            style={styles.input}
                                                        />
                                                        {formErrors.coupon_number && (
                                                            <ErrorMessage message={formErrors.coupon_number[0]} />
                                                        )}
                                                        </View>
                                                        <View style={styles.modalFooter}>
                                                        <TouchableOpacity 
                                                            onPress={() => requestCouponCheck() } 
                                                            style={styles.button}
                                                        >
                                                            <Text style={styles.buttonText}>{t('add')}</Text>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => closeModal() } style={{
                                                            padding: 10,
                                                            borderRadius: 5,
                                                            marginLeft: 10,
                                                        }}>
                                                            <Text style={styles.buttonText}>{t('close')}</Text>
                                                        </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                    </View>
                                                </Modal>
                                            </View>
                                            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                                                <Text style={{fontSize:14,color:'gray',fontFamily:'Lexend-Light'}}>
                                                    {t('shipping_fee')}
                                                </Text>
                                                <Text style={{fontSize:14,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {formatTranMoney(shippingFee)} MMK
                                                </Text>
                                            </View>
                                            <GapItem/>
                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                <Text style={{fontSize:16,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {t('total')}
                                                </Text>
                                                <Text style={{fontSize:16,color:colors.mainTextColor,fontFamily:'Lexend'}}>
                                                    {formatTranMoney(totalAmount)} MMK
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </Fragment>
                            </View>
                        }
                    </Fragment>
                    }
                </View>
            </ScrollView>
            <View style={{flexDirection:'row',marginTop:15,marginBottom:isIos ? 15 : 0}}>
                <CButton title={info.length > 0 ? t('Order') : t('back_to_home')} 
                    color={colors.mainLinkColor}
                    fontFamily={info.length > 0 ? 'Lexend-Medium':'Pyidaungsu'}
                    width={(device.width - 32)}
                    height={50}
                    disabled={disableBtn}
                    backgroundColor={'#F7EE25'}
                    action={() => (info.length > 0 ? goToCheckout() : props.navigation.goBack())}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
        flex:1,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:15
    },
    modalBackdrop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 20,
    },
    modalHeader: {
        borderBottomWidth: 0.2,
        borderColor: '#e0e0e0',
        paddingBottom: 15,
        marginBottom: 15,
    },
    modalHeaderText: {
        fontSize: 16,
        fontFamily: 'Lexend',
    },
    modalBody: {
        marginBottom: 10,
        marginTop: 10,
    },
    input: {
        height: 40,
        borderColor: '#e0e0e0',
        borderWidth: 1,
        paddingHorizontal: 10,
        borderRadius: 5,
        fontFamily:'Lexend'
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderColor: '#e0e0e0',
        marginTop: 10
    },
    button: {
        backgroundColor: '#F7EE25',
        padding: 10,
        borderRadius: 5,
        marginLeft: 10,
    },
    buttonText: {
        color: '#2A318B',
        fontSize: 16,
        fontFamily: 'Lexend',
    },
})

const mapstateToProps = state => {
    return {
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
      setCartCount: cart_count => {
        dispatch(setCartCount(cart_count))
      },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((Cart));

