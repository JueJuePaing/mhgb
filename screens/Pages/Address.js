import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView, Modal } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import CButton from '../Components/CButton';
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import ErrorMessage from '../Components/ErrorMessage';
import { showAlert } from 'react-native-customisable-alert';
import DottedLine from '../Components/DottedLine';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const Address = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [shopName,setShopName] = useState('');
    const [phoneNumber,setPhoneNumber] = useState('');
    const [address,setAddress] = useState('');

    const [shopNameFocused,setShopNameFocused] = useState(false);
    const [phoneNumberFocused,setPhoneNumberFocused] = useState(false);
    const [addressFocused,setAddressFocused] = useState(false);
    
    const [loading, setLoading] = useState(false);
    const [formErrors, setFormErrors] = useState({});

    const [city, setCity] = useState([]);
    const [township, setTownship] = useState([]);

    const [selectedCity, setSelectedCity] = useState({});
    const [selectedTownship, setSelectedTownship] = useState({});
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(false);

    const [cityModalVisible, setCityModalVisible] = useState(false);
    const [townshipModalVisible, setTownshipModalVisible] = useState(false);

    let phoneNumberRef = React.createRef();
    let addressRef = React.createRef();
    
    const { lable,btnText,addressCount,editData,isEdit } = props.route.params;

    useEffect(() => {
      getCities();
      if(isEdit == true){
          setShopName(editData.name);
          setSelectedCity(editData.city);
          setSelectedTownship(editData.township);
          setPhoneNumber(editData.phoneNumber);
          setAddress(editData.address);
      }
    }, [])

    useEffect(() => {
        if (Object.keys(selectedCity).length !== 0) {
          getTownships();
        }
    }, [selectedCity]);      

    useEffect(() => {
        setConfirmBtnDisble(!(shopName && phoneNumber));
      }, [shopName,phoneNumber,selectedTownship,selectedCity]); 

    const getCities = async () => {
        try {
            const response = await ApiService(url.cities, [], 'POST');

            if (response.code == '200') {

                setCity(response.cities);

            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false); 
        }
    }

    const getTownships = async() => {
        let formdata = new FormData();
        formdata.append('city', selectedCity.id);

        try {
            const response = await ApiService(url.townships, formdata, 'POST');

            if (response.code == '200') {
                setTownship(response.townships);
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false); 
        }
    }

    const addAddress = async() => {
        setLoading(true); 
        setFormErrors({});
                
        let formdata = new FormData();

        if(isEdit == true){
          formdata.append('name', shopName);
          formdata.append('phone_number',phoneNumber);
          formdata.append('city',selectedCity.id);
          formdata.append('township',selectedTownship.id);
          formdata.append('address_id',editData.id);
          address && formdata.append('shipping_address',address);
        }else{
          formdata.append('name', shopName);
          formdata.append('phone_number',phoneNumber);
          formdata.append('city',selectedCity.id);
          formdata.append('township',selectedTownship.id);
          address && formdata.append('shipping_address',address);
          formdata.append('default',1);
        }
       
        try {
            let apiUrl = isEdit ? url.update_address : url.add_address;
            const response = await ApiService(apiUrl, formdata, 'POST');
            if (response.code == '200') {
                showAlert({
                    title: t('success'),
                    message: isEdit ? t('edit_address_success') : t('add_address_success'),
                    alertType: 'success',
                    dismissable: false,
                    onPress: () => {
                        props.navigation.goBack()
                    }
                  });
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false); 
        }
    }

    return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loading && <Loading/> }
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
        <View style={{marginTop:10}}/>
        <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                    flexGrow: 1,
                }}>
               <View style={styles.container}>
                    <View style={styles.body}>
                    <HeaderLabel size={15} title={lable} />
                    <View style={{marginTop:30}}/>

                    <InputLabel title={t('shop_name')} /> 
                        <TextInput keyboardType="default"
                            maxLength={50}
                            onChangeText={(value) => setShopName(value) }
                            onFocus={() => setShopNameFocused(true)}
                            onBlur={() => setShopNameFocused(false)}
                            value={shopName}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() =>  phoneNumberRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: shopNameFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                         {formErrors.name && (
                            <ErrorMessage message={formErrors.name[0]} />
                        )}
                        <View style={{marginTop:25}}/> 
                    
                    <InputLabel title={t('phone_number')} /> 
                    <TextInput keyboardType="numeric"
                            ref={phoneNumberRef}
                            maxLength={50}
                            onChangeText={(value) => setPhoneNumber(value) }
                            onFocus={() => setPhoneNumberFocused(true)}
                            onBlur={() => setPhoneNumberFocused(false)}
                            value={phoneNumber}
                            selectionColor={colors.inputActiveColor}
                            onSubmitEditing={() =>  addressRef.current.focus()}
                            style={[styles.input,{
                                color:'#000',
                                borderColor:'#000',
                                borderBottomColor: phoneNumberFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                            }]} />
                            {formErrors.phone_number && (
                                <ErrorMessage message={formErrors.phone_number[0]} />
                            )}
                    <View style={{marginTop:25}}/>  

                    <InputLabel title={t('city')} /> 
                    <TouchableOpacity onPress={() => setCityModalVisible(true) }>
                      <View style={{paddingTop:15}}>
                          <Text style={styles.modalInput}>
                            {selectedCity.name}
                          </Text>
                          <View style={{
                            borderBottomColor: 'rgba(9, 15, 71, 0.1)',
                            borderBottomWidth: 1,
                            color:'#000',
                            borderColor:'#000',
                          }} />
                      </View>
                      {formErrors.city && (
                          <ErrorMessage message={formErrors.city[0]} />
                      )}
                    </TouchableOpacity>
                    <Modal
                          visible={cityModalVisible}
                          onRequestClose={() => setCityModalVisible(false)}
                          animationType="slide"
                          transparent={true}>
                        <View style={styles.modalBackdrop}>
                        <View style={styles.modalContent}>
                            <View style={styles.modalHeader}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                              <Text style={styles.modalHeaderText}>
                                {t('city')}
                              </Text>
                              <TouchableOpacity onPress={() => setCityModalVisible(false)}>
                              <Text style={{fontFamily:'Lexend',color:'gray',fontSize:22}}>
                                X
                              </Text>
                            </TouchableOpacity>
                            </View>
                            </View>
                            <View style={styles.modalBody}>
                            <ScrollView>
                              {city && city.map((cit) => (
                                <TouchableOpacity
                                  key={cit.id}
                                  onPress={() => {
                                    setSelectedCity(cit);
                                    setCityModalVisible(false);
                                    setSelectedTownship({});
                                  }}
                                >
                                 <Text style={{paddingVertical : 7, color:'#000',fontFamily:'Lexend',paddingHorizontal:10, fontSize : 13}}>{cit.name}</Text>
                                  <DottedLine/>
                                </TouchableOpacity>
                              ))}
                            </ScrollView>
                            </View>
                        </View>
                        </View>
                    </Modal>
                    <View style={{marginTop:25}}/>  
                    <InputLabel title={t('township')} /> 
                    <TouchableOpacity onPress={() => setTownshipModalVisible(true) }>
                    <View style={{paddingTop:15}}>
                          <Text style={styles.modalInput}>
                            {selectedTownship.name}
                          </Text>
                          <View style={{
                            borderBottomColor: 'rgba(9, 15, 71, 0.1)',
                            borderBottomWidth: 1,
                            color:'#000',
                            borderColor:'#000',
                          }} />
                      </View>
                      {formErrors.township && (
                          <ErrorMessage message={formErrors.township[0]} />
                      )}
                    </TouchableOpacity>
                    <Modal
                          visible={townshipModalVisible}
                          onRequestClose={() => setTownshipModalVisible(false)}
                          animationType="slide"
                          transparent={true}>
                        <View style={styles.modalBackdrop}>
                        <View style={styles.modalContent}>
                            <View style={styles.modalHeader}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                              <Text style={styles.modalHeaderText}>{t('township')}</Text>
                              <TouchableOpacity onPress={() => setTownshipModalVisible(false)}>
                              <Text style={{fontFamily:'Lexend',color:'gray',fontSize:22}}>
                                X
                              </Text>
                              </TouchableOpacity>
                            </View>
                            </View>
                            <View style={styles.modalBody}>

                              {
                                township.length > 0 ?
                                <ScrollView>
                                {township && township.map((town) => (
                                  <TouchableOpacity
                                    key={town.id}
                                    onPress={() => {
                                      setSelectedTownship(town);
                                      setTownshipModalVisible(false);
                                    }}
                                  >
<Text style={{paddingVertical : 7, color:'#000',fontFamily:'Lexend',paddingHorizontal:10, fontSize : 13}}>{town.name}</Text>
                                    <DottedLine/>
                                  </TouchableOpacity>
                                ))}
                                </ScrollView>
                                :
                                <Text style={{fontFamily:'Pyidaungsu',fontSize:16,alignSelf:'center',marginTop:50}}>
                                  {t('choose_city_first')}
                                </Text>
                              }
                             
                            
                            </View>
                        </View>
                        </View>
                    </Modal>

                    <View style={{marginTop:25}}/>  
                    <InputLabel title={t('address_detail')} />
                        <TextInput keyboardType="default"
                                ref={addressRef}
                                maxLength={255}
                                onChangeText={(value) => setAddress(value) }
                                onFocus={() => setAddressFocused(true)}
                                onBlur={() => setAddressFocused(false)}
                                value={address}
                                selectionColor={colors.inputActiveColor}
                                multiline={true}
                                numberOfLines={4}
                                style={[styles.input,{
                                    height:80,
                                    color:'#000',
                                    borderColor:'#000',
                                    borderBottomColor: addressFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                                }]} />
                            {formErrors.shipping_address && (
                                <ErrorMessage message={formErrors.shipping_address[0]} />
                            )}
                    </View>
                </View>
        </ScrollView>
        <View style={{marginTop:15,marginBottom:isIos ? 15 : 0}}>
        <CButton title={btnText} 
            color={colors.mainLinkColor}
            fontFamily={'Pyidaungsu-Bold'}
            width={device.width - 32}
            height={50}
            backgroundColor={'#F7EE25'}
            disabled={confirmBtnDisble}
            action={() =>  addAddress()
        }/>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:16,
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:15,
        fontFamily:'Pyidaungsu',
        color: '#000',
    },
    modalInput : {
      fontSize:15,
      fontFamily:'Pyidaungsu',
      color: '#000',
    },
    modalBackdrop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '80%',
        borderRadius: 5,
        padding: 10,
    },
    modalHeader: {
        borderBottomWidth: 1,
        borderColor: '#e0e0e0',
        paddingBottom: 10,
        marginBottom: 10,
    },
    modalHeaderText: {
      fontSize: 15,
      fontFamily: 'Lexend',
      paddingVertical : 5
    },
    modalBody: {
        marginBottom: 10,
        height: 400
    },
})

export default Address;