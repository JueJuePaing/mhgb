import React, { useState,useEffect,useRef } from 'react'
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity,KeyboardAvoidingView, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import HeaderLabel from '../Components/HeaderLabel';
import CButton from '../Components/CButton';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { ApiService } from '../../network/service';
import url from '../../network/url';
import ErrorMessage from '../Components/ErrorMessage';
import Loading from '../Components/Loading';
import VerificationTimer from '../Components/VerificationTimer';
import { showAlert } from 'react-native-customisable-alert';

const device = Dimensions.get('window');

const ForgotPasswordVerifyOtp = (props) => {

  const { token, phone_number } = props.route.params;
  const { colors } = useTheme();
  const [t] = useTranslation('common');
  const otpRef = useRef(null);
  const [code, setCode] = useState('');
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(60);
  const [formErrors, setFormErrors] = useState({});
  const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
  const [clearInput, setclearInput] = useState(false)

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      if (otpRef?.current) {
        otpRef.current.focusField(0);
      }
    }, 250);
  }, [otpRef]);

  useEffect(() => {

    if(code.length >= 6){
      setConfirmBtnDisble(false);
    }else{
      setConfirmBtnDisble(true);
    }
  }, [code])

  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
  
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(interval);
        } else {
          setSeconds(59);
          setMinutes(minutes - 1);
        }
      }
    }, 1000);
  
    return () => {
      clearInterval(interval);
    };
  }, [seconds]);

  const resendCode = async () => {
    setLoading(true); 
    setFormErrors({});

    let formdata = new FormData();
    formdata.append('phone_number', phone_number);
    try {
      const response = await ApiService(url.forgot_password, formdata, 'POST');

      if (response.code == '200') {
        setSeconds(60);
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false); 
    }
  }

  const verifyOTP = async () => {
    setLoading(true); 
    setFormErrors({});

    let formdata = new FormData();
    formdata.append('otp',code);
    formdata.append('token',token);
    formdata.append('phone_number',phone_number);

    try {
      const response = await ApiService(url.forgot_password_otp_verify, formdata, 'POST');

      if (response.code == '200') {
        props.navigation.replace('UpdatePassword',{
          phone_number: phone_number,
          token : response.token
        });
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false); 
    }
  }

  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
        { loading && <Loading/> }
        <NavigationHeader  action={() => props.navigation.goBack()} />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
          <View style={styles.container}>
            <HeaderLabel title={t('forgot_password')} />

            <View style={{marginTop:30}}/>

              <Text style={styles.noticeText}>
                {t('verify_otp_text1')} {phone_number} {t('verify_otp_text2')}
              </Text>

              <Text style={styles.noticeText}>
                {t('verify_otp_text3')} 09777793522 {t('verify_otp_text4')}
              </Text>

            <View style={{marginTop:35}}/>

            <OTPInputView
                style={{width: '100%', height:20}}
                ref={otpRef}
                pinCount={6}
                keyboardType='phone-pad'
                autoFocusOnLoad={false}
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeChanged = {(code => {
                  setCode(code);
                })}
                clearInputs={clearInput}
            />
            {formErrors.shop_name && (
                <ErrorMessage message={formErrors.otp[0]} />
            )}

            <View style={{marginTop:30}}/>
            
            <CButton 
                title={t('will_send')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                disabled={confirmBtnDisble}
                action={() =>  verifyOTP() }
            />

            <VerificationTimer minutes={minutes} seconds={seconds} />
            
            <TouchableOpacity onPress={ () => resendCode() }>
              <Text style={{
                    fontFamily:'Pyidaungsu',
                    fontSize:15,
                    color:colors.mainLinkColor,
                    paddingTop:15,
                    alignSelf:'center'
                  }}>
                    {t('resend_code')}
                </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width:device.width - 32,
  },
  noticeText: {
    fontFamily:'Pyidaungsu',
    fontSize:16,
    color:'gray'
  },
  borderStyleBase: {
    width: 30,
    height: 45
  },
  borderStyleHighLighted: {
    borderColor: "grey",
  },
  underlineStyleBase: {
    width: 40,
    height: 35,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'grey',
    fontFamily: 'Lexend-Light',
    fontSize: 21
  },
  underlineStyleHighLighted: {
    borderColor: "#4157FF",
  },
})

const mapstateToProps = state => {
  return {
      
  }
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(mapstateToProps,mapDispatchToProps)((ForgotPasswordVerifyOtp));
