import { Dimensions, StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    checkBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft : 10,
        paddingVertical : 10
    },
    languageTxt: {
        paddingLeft : 20,
        fontFamily: 'Lexend',
        color: '#000',
        fontSize : 14
    },

    modalBackdrop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 15,
    },
    modalHeader: {
        borderBottomWidth: 0.2,
        borderColor: '#e0e0e0',
        paddingBottom: 15,
        marginBottom: 15,
    },
    modalHeaderText: {
        fontSize: 16,
        fontFamily: 'Lexend',
    },
    modalBody: {
        marginBottom: 10,
        marginTop: 10,
    },
});

export default styles;