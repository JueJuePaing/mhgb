import React from 'react';
import { Text, Modal, View, TouchableOpacity} from 'react-native';
import {useTranslation} from "react-i18next";
import {MaterialIcons} from '@expo/vector-icons';

import styles from './styles';
import i18next from 'i18next';

const Language = ({
    closeHandler
}) => {
    const [t] = useTranslation('common');
    const currentLanguage = i18next.language;

    return <Modal
    visible={true}
    onRequestClose={closeHandler}
    animationType="slide"
    transparent={true}
>
    <TouchableOpacity activeOpacity={1} onPress={closeHandler} style={styles.modalBackdrop}>
        <View style={styles.modalContent}>
            <View style={styles.mainContent}>
                <TouchableOpacity
                    style={styles.checkBtn}
                    onPress={ () => {
                        i18next.changeLanguage('en')
                        closeHandler();
                    }}>
                    <MaterialIcons 
                        name={currentLanguage === 'en' ? 'radio-button-checked'  : 'radio-button-off' }
                        size={22}
                        color={'#000'} />
                    <Text style={[styles.languageTxt, {
                        
                    }]}>
                        {t('english')}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.checkBtn, {
                        borderBottomWidth : 0
                    }]}
                    onPress={ () => {
                        i18next.changeLanguage('my');
                        closeHandler();
                    }}>
                    <MaterialIcons 
                        color={'#000'} 
                        name={currentLanguage === 'my' ? 'radio-button-checked'  : 'radio-button-off' }
                        size={20} />
                    <Text style={styles.languageTxt}>
                        {t('myanmar')}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    </TouchableOpacity>
</Modal>;
}

export default Language;
