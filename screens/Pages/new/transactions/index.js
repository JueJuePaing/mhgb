import React, {useEffect, useState} from 'react';
import { Dimensions, FlatList, Text, DeviceEventEmitter, View, StatusBar} from 'react-native';
import {useTranslation} from "react-i18next";
import { showAlert } from 'react-native-customisable-alert';
import { connect } from 'react-redux';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { clearReceipts, updateReceipts } from '../../../../stores/actions';

import styles from './styles';
import NavigationHeader from '../../../Components/NavigationHeader';
import TransactionItem from '../../../Components/TransactionItem';
import LoadingView from '../../../Components/Loading';
import CButton from '../../../Components/CButton';

const device = Dimensions.get('window');

const LatestTransactions = (props) => {
    const [t] = useTranslation('common');
    const { saved } = props.route.params;

    const [
        initialLoading,
        setInitialLoading
    ] = useState(true);
    const [
        transactions,
        setTransactions
    ] = useState();
    const [refreshing,setRefreshing] = useState(false);
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (saved) {
            setInitialLoading(false);
            setTransactions(props.receipts);

        } else {
            getTransactions();
        }
    }, [saved, props.receipts]);


    useEffect(() => {
        if (!saved) {
            getTransactions()
        }
    }, [page, saved]);

    const getTransactions = async () => {
        try {
            setLoading(true)

            let formdata = new FormData();
            formdata.append('page',page);
            formdata.append('per_page', 16);

            const response = await ApiService(url.shop_invoices, formdata, 'POST');

            if (response.code == '200') {
                const newTransactions = page > 1 ? transactions?.concat(response.invoices) : response.invoices;
                setTransactions(newTransactions);
                if(response.next_pages != ''){
                    setLoadMore(true);
                }else{
                    setLoadMore(false);
                }
            }else  {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setInitialLoading(false);
            setLoading(false)
            setRefreshing(false);
        }
    }

    const filterHandler = () => {}
    const searchHandler = () => {}

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const handleRefresh = () => {
        if (refreshing) {
            return;
        }
        if (!saved) {
            setRefreshing(true);
            setPage(1);
            getTransactions();
        }
    }

    const submitAllHandler = async () => {
        setInitialLoading(true);
        try {

            const chunkSize = 10;
            const result = [];

            for (let i = 0; i < transactions.length; i += chunkSize) {
                const chunk = transactions.slice(i, i + chunkSize);
                result.push(chunk);
            }

            const requests = [];
            const numberOfRequests = result.length;
          
            for (let i = 0; i < numberOfRequests; i++) {

              let formdata = new FormData();

              result[i]?.map((transaction, index) => {
                    formdata.append(`orders[${index}][sub_total]`, parseInt(transaction.sub_total));
                    formdata.append(`orders[${index}][tax]`, parseInt(transaction.tax));
                    formdata.append(`orders[${index}][total]`, parseInt(transaction.total));
    
                    transaction.products?.map((product, pIndex) => {
                        formdata.append(`orders[${index}][products][${pIndex}][product_id]`, product.id);
                        formdata.append(`orders[${index}][products][${pIndex}][quantity]`, product.count);
                        formdata.append(`orders[${index}][products][${pIndex}][sale_price]`, product.sale_price);
                    })
              })
              const request = ApiService(url.shop_order, formdata , 'POST');
              requests.push(request);
            }
          
            try {
                const responses = await Promise.all(requests);
                // All requests are complete here
                
                const allCodesAre200 = responses?.every(item => item.code === 200 || item.code === '200');
                const atLeastOneCodeIs200 = responses?.some(item => item.code === 200);

                if (allCodesAre200 || atLeastOneCodeIs200) {

                    DeviceEventEmitter.emit('dashboard_products_change', 'true');
                    props.navigation.navigate('OK',{
                        'image' : require('../../../../assets/images/return_confirm.png'),
                        'title' : t('ok'),
                        'btnTitle' : t('back_to_home'),
                        'description' : allCodesAre200 ? t('submit_all_transaction_successful') : t('submit_some_transaction_successful'),
                        'isBack' : true
                        })
                    if (allCodesAre200) {
                        props.clearReceipts();
                    } else {
                        const indicesWithNon200Code = [];

                        responses.forEach((item, index) => {
                          if (item.code !== 200 && item.code !== '200') {
                            indicesWithNon200Code.push(index);
                          }
                        });
                        const filteredData = result.filter((_, index) => indicesWithNon200Code.includes(index));
                        const combinedArray = filteredData.flat();
                        props.updateReceipts(combinedArray)
                    }
                } else {
                    showAlert({
                        title: `Oops!`,
                        message: response.message,
                        alertType: 'error',
                        dismissable: true,
                    });
                }
            } catch (error) {
                console.error(error);
            } finally {
                setInitialLoading(false);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setInitialLoading(false);
        }
    };

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#F7FBFF'} />
        <NavigationHeader 
            title={saved ? t('save_order') :  t('transactions')}
            action={() => props.navigation.goBack()} 
            filterHandler={filterHandler}
            searchHandler={searchHandler}
            />
        { initialLoading && <LoadingView/> }

        {transactions?.length > 0 ? <FlatList
                showsVerticalScrollIndicator={false}
                data={transactions}
                style={{alignSelf: 'center', marginTop : -15}}
                keyExtractor={(item, index) => index.toString()}
                ListFooterComponent={loading && <LoadingView/>}
                renderItem={({ item, index }) => (
                    <TransactionItem
                        latest={index === transactions.length - 1}
                        itemHandler={()=> 
                            {
                                if (saved) {
                                    props.navigation.navigate('Receipt', {transaction : item, products: null})
                                } else {
                                    props.navigation.navigate('TransactionDetail', {item})
                                }
                            }}
                        color='#fff' 
                        key={item.id} 
                        item={item}
                        index={index} />
                )}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={1}
                onRefresh={handleRefresh}
                refreshing={refreshing}
                />
         :  !initialLoading ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16}}>
                Whoops!
            </Text> */}
            <Text style={{
                marginTop: 10,
                fontFamily: 'Pyidaungsu',
                fontSize : 14.5
            }}>
                {saved ? t('no_saved_invoices') : t('no_invoices')}
            </Text>
        </View> : null}

        <View style={{
            alignSelf: 'center',
            marginBottom : 15
        }}>
            {saved && transactions && transactions.length > 0 &&   <CButton 
                title={t('submit_all')} 
                color={'#fff'}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#2b3186'}
                action={() => submitAllHandler() }
            />}
        </View>

    </View>
}


const mapstateToProps = state => {
    return {
        receipts : state.receipts
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        clearReceipts: () => {
            dispatch(clearReceipts())
          },
        updateReceipts: receipts => {
            dispatch(updateReceipts(receipts))
        }
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((LatestTransactions));