import React, { useState,useEffect } from 'react'
import { StyleSheet, Text, Image,View, Dimensions, TouchableOpacity,TextInput,FlatList } from 'react-native';
import {useTranslation} from "react-i18next";
import { connect } from 'react-redux';
import { showAlert } from 'react-native-customisable-alert';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { ApiService } from '../../../network/service';
import url from '../../../network/url';

import { setCartCount } from '../../../stores/actions';

import NavigationHeader from '../../Components/NavigationHeader'
import LoadingView from '../../Components/LoadingView';
import Loading from '../../Components/Loading';

const device = Dimensions.get('window');

const RemainStockOut = (props) => {
    const [t] = useTranslation('common');

    const [page, setPage] = useState(1);
    const [loading,setLoading] = useState(true);
    const [refreshing,setRefreshing] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const [initialLoading, setInitialLoading] = useState(false);

    const [
      searchName,
      setSearchName
    ] = useState('');
    const [
        lowStockProducts,
        setLowStockProducts
    ] = useState();

  useEffect(() => {
      getLowStockProducts();
  }, []);

  useEffect(() => {
    getLowStockProducts()
}, [page]);


  useEffect(() => {
    if (!searchName) {
      searchHandler();
    }
}, [searchName]);

  const getLowStockProducts = async () => {
      try {
          setLoading(true);
          let formdata = new FormData();
          if (searchName) formdata.append('name', searchName);
          formdata.append('page',page);
          formdata.append('per_page', 16);

          const response = await ApiService(url.shop_low_stock, formdata, 'POST');
;
          if (response.code == '200') {
              setLowStockProducts(response.products);

              const newProducts = page > 1 ? lowStockProducts?.concat(response.products) : response.products;
              setLowStockProducts(newProducts);

              if(response.next_pages != ''){
                  setLoadMore(true);
              }else{
                  setLoadMore(false);
              }
          }else if (response.code == '422') {
              // setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
              showAlert({
                  title: `Oops!`,
                  message: response.message,
                  alertType: 'error',
                  dismissable: true,
              });
          }
      } catch (error) {
          console.error(error);
          setInitialLoading(false);
          setLoading(false);
      } finally {
          setInitialLoading(false);
          setLoading(false);
      }
  }

  //   const deleteStock = (id) => {
  //     showAlert({
  //           title:"Remove Stock",
  //           message: "Are you sure want to remove?",
  //           alertType: 'warning',
  //           onPress: () => handleDeleteStock(id)
  //     });
  // }

    // const handleDeleteStock = async(id) => {
    //   closeAlert();
    //   setInitialLoading(true);
    //   let formdata = new FormData();
    //   formdata.append('product_id',id);
      
    //   try {
    //       const response = await ApiService(url.stock_delete, formdata, 'POST');
    //       if (response.code == '200') {
    //         const updatedStocks = stocks.filter(stock => stock.id !== id);
    //         setStocks(updatedStocks);

    //       }else if (response.code == '422')
    //        {
    //       }else if (response.code == '401' || response.code == '400') {
    //           showAlert({
    //             title: `Oops!`,
    //             message: response.message,
    //             alertType: 'error',
    //             dismissable: true,
    //           });
    //       }
    //   } catch (error) {
    //       console.error(error);
    //   } finally {
    //       setInitialLoading(false);
    //   }
    // }
    // const handleCancelEdit = () => {
    //   setEditableStock(null);
    //   setIsEditing(false);
    // }

  const addedToCart = (item) => {
    const updatedData = lowStockProducts?.map((product, index) => {
      if (product.id === item.id) {
        if (product['checked'])
          product['checked'] = false;
        else 
          product['checked'] = true;
        return product;
      } else {
        return product;
      }
    })
    setLowStockProducts(updatedData);
  };

  const checkAllHandler = async () => {
    setInitialLoading(true);

    for (const item of lowStockProducts) {
      let formdata = new FormData();
      formdata.append('product_id', item.product_code);
      formdata.append('quantity', 1);
  
      try {
          const response = await ApiService(url.add_to_cart, formdata, 'POST');
      } catch (error) {
          console.error(error);
      }
    }

    getCartCount();
    const updatedData = lowStockProducts?.map((product) => {
      product['checked'] = true;
      return product;
    })
    setLowStockProducts(updatedData);
  }

  const checkHandler = async(item) => {

    if (item.checked) {
      const updatedData = lowStockProducts?.map((product) => {
        if (product.id === item.id) {
          product['checked'] = false;
          return product;
        } else {
          return product;
        }
      })
      setLowStockProducts(updatedData);
      return;
    }

    setInitialLoading(true); 

    let formdata = new FormData();
    formdata.append('product_id', item.product_code);
    formdata.append('quantity', 1);

    try {
        const response = await ApiService(url.add_to_cart, formdata, 'POST');

        if (response.code == '200') {
          getCartCount(item);
        }else if (response.code == '422') {
          showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: false,
            });
        }else if (response.code == '401' || response.code == '400') {
            showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: true,
            });
        }
    } catch (error) {
        console.error(error);
    } finally {
      setInitialLoading(false);
    }
  }

  const getCartCount = async(item) => {
    try {
      const response = await ApiService(url.cart_count, [], 'POST');
      if (response.code == '200') {
        props.setCartCount(response.cart_count);
        if (item) {
          addedToCart(item);
        }
      } else if (response.code == '422') {
        // setFormErrors(response.errors);
      } else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      console.error(error);
    }
    finally {
      setInitialLoading(false); 
    }
  }

  const searchHandler = () => {
    setPage(1);
    setLowStockProducts([]);
    setInitialLoading(true);
    getLowStockProducts();
  };

  const handleRefresh = () => {
    if (refreshing) {
        return;
    }
    setPage(1);
    setLowStockProducts([]);
    setRefreshing(true);
  }

  const handleLoadMore = () => {
    if(loadMore){
        setPage(page + 1)
    }
  }

  const allChecked = lowStockProducts?.every(product => product.checked === true);

  return (
   <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { initialLoading && <Loading/> }
        <View style={{marginTop:10}}/>
        <NavigationHeader 
          title={t('remain_stockout')}  
          action={() => props.navigation.goBack()}
          showCart={true} 
          cartAction={() => props.navigation.navigate('Cart')} />
          <View style={styles.container}> 

          {/* <SearchBar searchHandler={() => props.navigation.navigate('Search')} /> */}


              <View style={styles.searchInput}>
                <TextInput
                    style={styles.inputBox}
                    placeholder={t('search_products')}
                    placeholderTextColor='#999'
                    value={searchName}
                    onChangeText={ setSearchName }
                />
                <TouchableOpacity onPress={searchHandler}>
                  <Image
                    source={require('../../../assets/images/search.png')}
                    style={styles.searchIcon}
                    />
                </TouchableOpacity>
            </View>
      
            {lowStockProducts?.length > 0 && <View style={styles.selectAll}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={()=> checkAllHandler()}>
                <MaterialCommunityIcons
                  name={allChecked ? 'checkbox-outline' : 'checkbox-blank-outline'}
                  size={22}
                  color='gray' />
              </TouchableOpacity>
              <Text style={styles.selectAllTxt}>
                {t('select_all')}
              </Text>
            </View>}
            

              <View style={{marginTop:15}}/>
              
              {lowStockProducts?.length > 0 ? <FlatList
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps={'handled'}
                data={lowStockProducts}
                keyExtractor={(item) => item.id.toString()}
                ListFooterComponent={loading && <LoadingView/>}

                onEndReached={handleLoadMore}
                onEndReachedThreshold={1}
                onRefresh={handleRefresh}
                refreshing={refreshing}

                // onEndReached={handleLoadMore}
                renderItem={({ item }) => (
                    <View key={item.id} style={styles.orderCard}>
                      <View style={styles.orderContent}>

                        <View style={{flexDirection: 'row',
                      alignItems: 'center'}}>

                          <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={()=> checkHandler(item)}
                            style={styles.checkbox}>
                            <MaterialCommunityIcons
                              name={item.checked ? 'checkbox-outline' : 'checkbox-blank-outline'}
                              size={22}
                              color='gray' />
                          </TouchableOpacity>

                          <View style={styles.middleContent}>
                            <View style={styles.nameRow}>
                              <Text style={styles.nameLabel}>
                                {t('name')}
                              </Text>
                              <Text style={styles.name}>
                                &nbsp;-&nbsp;{item.name}
                              </Text>
                            </View>
                            <View style={[styles.nameRow, {marginTop: 7}]}>
                              <Text style={styles.stockLabel}>
                                {t('remain_stock_count')}
                              </Text>
                              <Text style={styles.stock}>
                                &nbsp;-&nbsp;{item.remain_quantity ? item.remain_quantity : 0}
                              </Text>
                            </View>
                            
                          </View>
                        </View>

                        <View style={styles.rightContent}>
                          {item.id && <View style={styles.nameRow}>
                            <Text style={styles.nameLabel}>
                              PID
                            </Text>
                            <Text style={styles.name}>
                              {` - ${item.id}`}
                            </Text>
                          </View>}
{/* 
                          <View style={styles.btnRow}>
                            <TouchableOpacity 
                              onPress={() => deleteStock(item.id)}
                              style={[styles.button, { backgroundColor: '#f0a741', marginRight: 8 }]}>
                              <Ionicons name="eye" color="#fff" size={15} />
                            </TouchableOpacity>
                            <TouchableOpacity 
                              onPress={() => deleteStock(item.id)}
                              style={[styles.button, { backgroundColor: '#EB5757' }]}>
                              <Icon name="trash" color="#fff" size={15} />
                            </TouchableOpacity>
                          </View> */}
                        </View>

                        {/* <View>
                         

                          <View style={styles.buttonContainer}>
                            <View style={{ marginHorizontal: 5 }} />
                              <TouchableOpacity 
                                onPress={() => deleteStock(item.id)}
                                style={[styles.button, { backgroundColor: '#EB5757' }]}>
                                <Icon name="trash" color="#fff" size={15} />
                              </TouchableOpacity>
                            </View>

                        </View> */}
                    </View>
                  </View>
                )}
                /> : !initialLoading ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16, marginTop : -50}}>
                    Whoops!
                </Text> */}
                <Text style={{
                    marginTop: 10,
                    fontFamily: 'Pyidaungsu',
                    fontSize : 14.5
                }}>
                   {t('no_stock_out_items')}
                </Text>
            </View> : null}
          </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    buttonText: {
      fontFamily: 'Lexend-Medium',
      fontSize: 14,
      color: '#1987FB',
      alignSelf:'center',
      marginLeft: 15
    },
    orderCard: {
      marginLeft: 1,
      marginRight: 1,
      padding: 12,
      marginBottom: 15,
      backgroundColor: '#F6F7F9',
      borderRadius: 9,
      shadowColor: '#000',
      shadowOffset: {
      width: 0,
      height: 1
      },
      shadowOpacity: 0.23,
      shadowRadius: 1.62,
      elevation: 1,
    },
    orderContent: {
      flexDirection:'row',
      justifyContent:'space-between', 
      marginTop: 0,
      alignItems: 'center'
    },
    checkbox: {
      marginRight: 10
    },
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    button: {
      width: 23,
      height: 23,
      borderRadius: 5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    disabledButton : {
      opacity: 0.5
    },
    input: {
      width: 100,
      height: 40,
      borderWidth: 1,
      borderColor: '#ccc',
      borderRadius: 5,
      paddingHorizontal: 10,
      fontFamily: 'Lexend-Light',
      fontSize: 14,
      color: '#000',
    },
  nameRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  nameLabel: {
    fontFamily: 'Lexend-Medium', 
    fontSize: 13,
    color: '#000'
  },
  name: {
    fontFamily: 'Lexend-Light', 
    fontSize: 14, 
    color: '#000'
  },
  selectAllTxt: {
    fontFamily: 'Lexend', 
    fontSize: 13, 
    color: '#000',
    paddingLeft: 10
  },
  stockLabel: {
    fontFamily: 'Lexend-Medium', 
    fontSize: 13,
    color: '#d45550'
  },
  stock: {
    fontFamily: 'Lexend-Light', 
    fontSize: 14, 
    color: '#d45550'
  },
  middleContent: {
    // width: device.width * 0.6,
    paddingRight: 3
  },
  rightContent: {
    // width: device.width * 0.3
  },
  btnRow: {
    flexDirection: 'row',
    marginTop: 7
  },
  selectAll: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginLeft: 12
  },
  searchInput: {
    alignItems:'center',
    backgroundColor:'#fff',
    height: 50,
    borderWidth: 1,
    borderColor:'#fff',
    borderRadius:56,
    fontSize:14,
    width : device.width - 32,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.84,
    elevation: 2,
    paddingLeft: 15,

    flexDirection: 'row', marginTop: 3
},
searchIcon: {
  width: 18,
  height: 18,
  resizeMode: 'contain',
  marginLeft: 10
},
inputBox: {
  width: device.width - 110,
  fontFamily: 'Lexend',
  //backgroundColor:'red',
  paddingLeft : 10
}
  })

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setCartCount: cart_count => {
          dispatch(setCartCount(cart_count))
        },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((RemainStockOut));
