import React, {
    useState,
    useEffect
} from 'react';
import { Dimensions, Text, View, StatusBar, TextInput} from 'react-native';
import {useTranslation} from "react-i18next";
import { showAlert } from 'react-native-customisable-alert';
import { useTheme } from '@react-navigation/native';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';

import styles from './styles';
import LoadingView from '../../../Components/Loading';
import NavigationHeader from '../../../Components/NavigationHeader';
import HeaderLabel from '../../../Components/HeaderLabel';
import CButton from '../../../Components/CButton';

const ProfitMargin = (props) => {
    const device = Dimensions.get('window');
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const [
        profitMargin,
        setProfitMargin
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(true);
    const [
        focused,
        setFocused
    ] = useState(false);

    useEffect(() => {
        getProfitMargin();
    }, []);

    const btnDisable = !profitMargin;

    const getProfitMargin = async () => {
        try {
            const response = await ApiService(url.profit_margin, [] , 'POST');

            if (response.code == '200') {
                if (response.profit_margin) {
                    setProfitMargin(response.profit_margin + '');
                }
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const submitProfitMargin = async () => {
        setLoading(true);

        try {
            let formdata = new FormData();
            formdata.append('rate', profitMargin);

            const response = await ApiService(url.edit_profit_margin, formdata, 'POST');

            if (response.code == '200') {
                setProfitMargin(response.profit_margin + '');

                showAlert({
                    title: t('success'),
                    message: t('profit_margin_edit_successful'),
                    alertType: 'success',
                    dismissable: false,
                    onPress: () => {
                        props.navigation.goBack();
                    }
                  });
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.errors?.rate?.length > 0 ? response.errors.rate[0] : response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const pattern = /^[0-9\s]+$/;

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#F7FBFF'} />
        { loading && <LoadingView/> }

        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
        
        <View style={styles.content}>
            <HeaderLabel title={t('profit_margin')} />

            <View style={styles.row}>
                <TextInput 
                    keyboardType="number-pad"
                    maxLength={3}
                    onChangeText={(value) => {
                        if (!value || value === "") {
                            setProfitMargin(value)
                        } else if (pattern.test(value)) {
                            setProfitMargin(value)
                        }
                    } }
                    value={profitMargin}
                    selectionColor={colors.inputActiveColor}
                    onFocus={() => setFocused(true)}
                    onBlur={() => setFocused(false)}
                    style={[styles.input,{
                        color:'#000',
                        borderColor:'#000',
                        borderBottomColor: focused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                    }]} />
                <Text style={styles.percent}>%</Text>
            </View>

            <CButton title={t('submit')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                disabled={btnDisable}
                action={() =>  submitProfitMargin()}/>
        </View>
        
    </View>
}

export default ProfitMargin;