import { Dimensions, StyleSheet } from "react-native";

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#fff',
        // paddingHorizontal : 16
        // paddingTop: getStatusBarHeight() + 20
    },
    content : {
        paddingHorizontal : 16
    },
    row : {
        flexDirection : 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width : width * 0.9,
        height: 35,
        marginTop : 10,
        marginBottom : 30,
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:15,
        fontFamily:'Pyidaungsu',
        color: '#000',
        width : width * 0.9
    },
    percent : {
        fontSize:15,
        fontFamily:'Pyidaungsu',
        color: '#000',
        left: -20
    }
});

export default styles;