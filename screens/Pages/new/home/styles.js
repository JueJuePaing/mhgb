import { Dimensions, StyleSheet } from "react-native";
import { getStatusBarHeight } from 'react-native-status-bar-height';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#fff',
        paddingTop: getStatusBarHeight() + 20
    },
    content: {
        paddingHorizontal : width * 0.04 // '4%',
    },
    headerRow : {
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-between'
    },
    leftContent : {

    },
    day : {
        fontFamily:'Pyidaungsu',
        fontSize:14,
        color:'gray'
    },
    date : {
        fontSize: 14,
        fontFamily: 'Lexend-Medium',
        color : '#000',
        marginTop : 5
    },
    rightContent : {
        flexDirection : 'row',
        alignItems : 'center',
    },
    profile : {
        width: height * 0.09,
        height: height * 0.09,
        resizeMode:'cover',
        borderRadius:37,
        borderColor:'#000'
    },
    searchBtn : {
        width: height * 0.07,
        height: height * 0.07,
        borderRadius:35,
        borderColor:'lightgray',
        borderWidth:1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 9
    },
    name : {
        fontSize: 18,
        fontFamily: 'Lexend-Medium',
        color : '#000',
        paddingTop : height * 0.02,
        paddingBottom: 5,
        fontWeight: 'bold'
    },
    need : {
        fontSize: 15,
        fontFamily: 'Lexend',
        color : '#000',
        paddingTop : height * 0.02,
        paddingBottom: 5,
        fontWeight: 'bold'
    },
    avenueContainer: {
        width: width * 0.92, //  '94%',
        marginVertical: 10,
        borderRadius: 10,
        paddingHorizontal: 10,
        paddingVertical: 15
    },
    amount: {
        fontSize: 14,
        fontFamily: 'Lexend',
        color : '#fff'
    },
    avenueLabel: {
        fontSize: 9,
        fontFamily: 'Lexend',
        color : '#e8e8df',
        paddingTop: 3
    },
    avenueTime: {
        fontSize: 9,
        fontFamily: 'Lexend',
        color : '#e8e8df',
        paddingTop: 3,
        alignSelf: 'flex-end'
    },
    menuContainer: {
        marginTop: 12
    },
    menu: {
        width: '47.5%',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    menuTitle: {
        fontSize: 14,
        fontFamily: 'Lexend',
        color : '#fff',
        paddingTop: 8
    },
    dashboardMenu: {
        backgroundColor: 'green',
        height: height * 0.2 //150
    },
    transactionMenu: {
        backgroundColor: 'orange',
        height: height * 0.16, //120
        position: 'absolute',
        right: 0
    },
    lowStockMenu: {
        backgroundColor: 'orange',
        height: height * 0.16, // 120
        position: 'absolute',
        top: height * 0.2 + 15 //165
    },
    allproductMenu: {
        backgroundColor: 'skyblue',
        height: height * 0.2, //150
        position: 'absolute',
        top: height * 0.2 - 15, //135
        right: 0 
    },
    shadowContainer: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'transparent'
    },
    footer: {
        height : 50,
        backgroundColor: '#fff',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,

        shadowColor: '#000',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 6,
        elevation: 8
    },
    addBtnOuter: {
        alignSelf: 'center',
        marginTop: -30,
        width: 62,
        height: 62,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F7FBFF',
        borderRadius: 40,
    },
    addBtn: {
        backgroundColor: '#323790',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,

        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        elevation: 4, 

    },
    backButton: {
        // width: height * 0.035,
        // height: height * 0.035,
        width : 22,
        height : 22
        // marginLeft: 20,
        // marginRight: 10,
      },
      backBtn: {
        alignSelf: 'center', 
        width: width - 60, 
        height: 50,
        backgroundColor: '#F7EE25', 
        borderRadius: 30, 
        justifyContent: 'center', 
        alignItems: 'center',

        position: 'absolute',
        bottom: 90
      },
      backBtnTxt: {
        fontSize: 15,
        textAlign: 'center', textAlignVertical: 'center',fontFamily:'Lexend-Medium'
      }
});

export default styles;