import React, {
    useState,
    useEffect,
    useContext
} from 'react';
import { Text, View, StatusBar,Image, TouchableOpacity} from 'react-native';
import {useTranslation} from "react-i18next";
import { LinearGradient } from 'expo-linear-gradient';
import moment from 'moment';
import { showAlert } from 'react-native-customisable-alert';
import { useTheme } from '@react-navigation/native';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { formatTranMoney } from '../../../../configs/helper';
import { AppContext } from '../../../../context/AppContextProvider';

import styles from './styles';
import Dashboard from '../../../../assets/icons/Dashboard';
import Transaction from '../../../../assets/icons/Transaction';
import LowStock from '../../../../assets/icons/LowStock';
import AllProduct from '../../../../assets/icons/AllProduct';
import Plus from '../../../../assets/icons/Plus';
import LoadingView from '../../../Components/Loading';

const Home = (props) => {
    const [t] = useTranslation('common');
    const currentDate = moment();
    const { colors } = useTheme();
    const {profileData} = useContext(AppContext);

    const [
        revenue,
        setRevenue
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(true);

    useEffect(() => {
        getRevenue();
    }, []);

    const getRevenue = async () => {
        try {
            const response = await ApiService(url.shop_revenue, [] , 'POST');

            if (response.code == '200') {
                setRevenue(response);
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#F7FBFF'} />
        { loading && <LoadingView/> }
        <View style={styles.content}>
            <View style={styles.headerRow}>
                <View style={styles.leftContent}>
                    <TouchableOpacity 
                        onPress={() => props.navigation.goBack()}>
                        <Image
                            source={require('../../../../assets/images/back.png')}
                            tintColor={'#090F47'}
                            style={styles.backButton}
                            />
                    </TouchableOpacity>
                    <Text style={styles.date}>{`${currentDate.format('dddd')} ${currentDate.format('D MMM')}`}</Text>
                </View>
                <View style={styles.rightContent}>
                    <TouchableOpacity
                     activeOpacity={1}
                     style={styles.profileBg}>
                        <Image
                            source={profileData?.logo ? {uri : profileData.logo} : require('../../../../assets/images/default_user.png')}
                            style={styles.profile}/>
                    </TouchableOpacity>
                </View>
            </View>

            <Text style={styles.name}>
                {`Hi ${profileData?.name ? profileData.name : ''}`}
            </Text>
            <Text style={styles.day}>{t('welcome_from_dashboard')}</Text>

            <LinearGradient
                colors={['rgba(131,117,241,1)','rgba(91,86,192,1)','rgba(35,38,99,1)']} 
                style={styles.avenueContainer}
                >
                <Text style={styles.amount}>
                    {`${revenue?.today_revenue ? formatTranMoney(revenue.today_revenue) : 0} MMK`}
                </Text>
                <Text style={styles.avenueLabel}>{t('today_revenue')}</Text>
            </LinearGradient>

            <Text style={styles.need}>{t('what_you_need')}</Text>

            <View style={styles.menuContainer}>
                <TouchableOpacity
                    onPress={()=> props.navigation.navigate('NewDashboard')}
                    activeOpacity={0.8}>
                    <LinearGradient
                        style={[styles.menu, styles.dashboardMenu]}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 1 }}
                        colors={[
                            'rgba(180,249,229, 1)',
                            'rgba(165,239,218,1)',
                            'rgba(146,228,204,1)',
                            'rgba(125,215,188,1)',
                            'rgba(106,201,171,1)',
                            'rgba(99,196,165,1)',
                            'rgba(96,193,161,1)',
                            'rgba(91,189,155,1)',
                            'rgba(88,186,152,1)',
                            'rgba(84,181,145,1)'
                        ]}>
                        <Dashboard />
                        <Text style={styles.menuTitle}>{t('dashboard')}</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <LinearGradient
                    style={[styles.menu, styles.transactionMenu]}
                    colors={['rgba(244,197,137,1)','rgba(242,166,78,1)']}>
                    <TouchableOpacity style={styles.center}
                        onPress={()=> props.navigation.navigate('LatestTransactions', {saved :false})}>
                        <Transaction />
                        <Text style={styles.menuTitle}>{t('transactions')}</Text>
                    </TouchableOpacity>
                </LinearGradient>
                <LinearGradient
                    style={[styles.menu, styles.lowStockMenu]}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 1 }} 
                    colors={[
                        'rgba(242,157,182, 1)',
                        'rgba(235,63,103,1)'
                    ]}>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={()=> props.navigation.navigate('NewRemainStockOut')}
                        style={styles.center}>
                        <LowStock />
                        <Text style={styles.menuTitle}>{t('remain_stockout')}</Text>
                    </TouchableOpacity>
                </LinearGradient>
                <LinearGradient
                    style={[styles.menu, styles.allproductMenu]}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 1 }} 
                    colors={[
                        'rgba(185,233,252, 1)',
                        'rgba(96,188,225,1)'
                    ]}>
                    <TouchableOpacity 
                        onPress={()=> props.navigation.navigate('AllProducts')}
                        style={styles.center}>
                        <AllProduct />
                        <Text style={styles.menuTitle}>{t('remain_stock')}</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </View>

        </View>


        <TouchableOpacity 
            onPress={() => props.navigation.reset({
                index: 0,
                routes: [{ name: 'TabNav' }],
            })}
            style={styles.backBtn}>
            <Text style={[styles.backBtnTxt, {color : colors.mainLinkColor}]}>{t('MHGB_STORE')}</Text>
        </TouchableOpacity>
        
        <View style={styles.shadowContainer}>
            <View style={styles.footer}>
                <TouchableOpacity 
                    activeOpacity={1}
                    style={styles.addBtnOuter}
                    >
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => props.navigation.navigate('AddTransaction')}
                        style={styles.addBtn}>
                        <Plus />
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        </View>
        
        {/* {showDrawer && <Drawer 
            onClose={()=> setShowDrawer(false)}
            menuHandler={menuHandler} />} */}

    </View>
}

export default Home;