import React, {useState, useEffect} from 'react';
import { ScrollView, FlatList, ToastAndroid, TextInput, Modal, Text, TouchableWithoutFeedback, View, StatusBar, Dimensions,Image, TouchableOpacity} from 'react-native';
import {useTranslation} from "react-i18next";
import {FontAwesome5, Ionicons} from '@expo/vector-icons';
import { useTheme, useFocusEffect } from '@react-navigation/native';
import { showAlert } from 'react-native-customisable-alert';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { formatTranMoney } from '../../../../configs/helper';

import styles from './styles';
import ProductItem from '../../../Components/ProductItem';
import CartItem from '../../../Components/CartItem';
import CButton from '../../../Components/CButton';
import { getStatusBarHeight } from "react-native-status-bar-height";
import LoadingView from '../../../Components/Loading';

const {width} = Dimensions.get('window');

const AddTransaction = (props) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const [
        name,
        setName
    ] = useState('');
    const [
        showCart,
        setShowCart
    ] = useState(false);
    const [
        productList,
        setProductList
    ] = useState();
    const [
        categories,
        setCategories
    ] = useState();
    const [
        categoryId,
        setCategoryId
    ] = useState('');
    const [
        cartProducts,
        setCartProducts
    ] = useState([]);

    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [loading, setLoading] = useState(false);
    const [initialLoading, setInitialLoading] = useState(false);

    const [isConnection, SetIsConnection] = useState(true);

    useEffect(() => {
      const unsubscribe = NetInfo.addEventListener(state => {
          handleConnectivityChange(state.isInternetReachable)
      });
      return unsubscribe;
    }, [])
  
    const handleConnectivityChange = isConnection => {
      SetIsConnection(isConnection);
    };


    useFocusEffect(
        React.useCallback(() => {
          // Fetch data or perform any other side effect
            setName('');
            setShowCart(false)
            setProductList();
            setCartProducts([]);
            setCategoryId('');
            setPage(1);
            setCategories();

            if (!isConnection) {
                setProductList({products : props.products})
            } 
            else {
                setInitialLoading(true);
                getCategories();
                getProductList();
            }
        
    
        }, [isConnection]) // The empty dependency array ensures this effect only runs on mount/unmount
      );

    useEffect(() => {
        if (isConnection) {
            getCategories();
        }
    }, [isConnection]);

    useEffect(() => {
        if (isConnection) {
            setInitialLoading(true);
            getProductList();
        }
    }, [categoryId, isConnection]);

    useEffect(() => {
        if (!name && isConnection) {
          searchHandler();
        }
    }, [name, isConnection]);

    useEffect(() => {
        if (isConnection) {
            getProductList()
        }
    }, [page, isConnection]);

    const getCategories = async () => {
        try {
            const response = await ApiService(url.shop_categories, [] , 'POST');

            if (response.code == '200') {
         
                if (response.categories) {
                    const uniqueTableIds = [];
                    const uniqueData = [];
                    
                    response.categories.forEach(item => {
                      if (!uniqueTableIds.includes(item.table_id)) {
                        uniqueTableIds.push(item.table_id);
                        uniqueData.push(item);
                      }
                    });

                    setCategories(uniqueData);
                }
                    
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const getProductList = async () => {
        try {
            setLoading(true);

            let formdata = new FormData();
            formdata.append('category_id', categoryId);
            formdata.append('name', name);

            formdata.append('page',page);
            formdata.append('per_page', 16);

            const response = await ApiService(url.shop_products, formdata, 'POST');

            if (response.code == '200') {

                const newProducts = page > 1 ? productList?.products?.concat(response.products) : response.products;
                setProductList({...response, products: newProducts});

                if(response.next_pages != ''){
                    setLoadMore(true);
                }else{
                    setLoadMore(false);
                }
            }else if (response.code == '422') {
                // setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
            setInitialLoading(false);
            setLoading(false);
        } finally {
            setInitialLoading(false);
            setLoading(false);
        }
    }

    const searchHandler = () => {
        if (isConnection) {
            setPage(1);
            setProductList([]);
            setInitialLoading(true);
            getProductList();
        } else {
            offlineSearchHandler();
        }
    }

    const offlineSearchHandler = () => {
        if (name) {
            const filteredData = props.products?.filter((item) => item.name.includes(name));
            setProductList({products: filteredData});
        } else {
            setProductList({products: props.products});
        }
    }

    const requestAddCart = (product) => {
        setCartProducts([...cartProducts, {...product, count : 1}]);
    }

    const increment = (product) => {
        const updatedData = cartProducts?.map(item => {
            if (item.id === product.id) {
                if (item.count + 1 <= product.remain_quantity) {
                    return { ...item, count: item.count + 1 };
                }
                else {
                    ToastAndroid.show(t('cannot_add_error'), ToastAndroid.SHORT);
                    return item;
                }
            }
            return item;
          });
        setCartProducts(updatedData);
    }

    const decrement = (product) => {
        const updatedData = cartProducts?.map(item => {
            if (item.id === product.id && item.count > 1) {
              return { ...item, count: item.count - 1 };
            }
            return item;
          });
        setCartProducts(updatedData);
    }

    const removeCartHandler = (product) => {
        const filteredData = cartProducts?.filter(item => item.id !== product.id);
        setCartProducts(filteredData);
    }

    const updateQty = (product, val) => {
        if (val > product.remain_quantity) {
            ToastAndroid.show(t('cannot_add_error'), ToastAndroid.SHORT);
        } else {
            const updatedData = cartProducts?.map(item => {
                if (item.id === product.id) {
                  return { ...item, count: val };
                }
                return item;
              });
            setCartProducts(updatedData);
        }
    }

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const totalCost = cartProducts?.reduce((accumulator, item) => {
        return accumulator + item.count * item.sale_price;
      }, 0);

    // if (initialLoading) return null

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#f8f7eb'} />

        <View style={styles.headerContent}>
            
            <View style={styles.titleRow}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => props.navigation.goBack()}>
                    <Ionicons   
                        name='close'
                        size={22}
                        color='#000' />
                </TouchableOpacity>
                <Text style={styles.title}>
                    {t('add_transaction')}
                </Text>
                {/* <Ionicons
                    name='ios-barcode-outline'
                    size={22}
                    color='#000' /> */}
            </View>
            <View style={styles.searchInput}>
                <TextInput
                    style={styles.inputBox}
                    placeholder={t('search_products')}
                    placeholderTextColor='#999'
                    value={name}
                    onChangeText={ setName }
                />
                <TouchableOpacity onPress={searchHandler}>
                  <Image
                    source={require('../../../../assets/images/search.png')}
                    style={styles.searchIcon}
                    />
                </TouchableOpacity>
            </View>
            {/* <View style={[styles.titleRow, {marginTop : 15}]}>
                <TextInput
                    style={styles.searchInput}
                    placeholder="Search"
                    value={name}
                    onChangeText={ changeName }
                />
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ marginRight : 5 }}
                    onPress={searchHandler}>
                    <Ionicons   
                        name='search'
                        size={25}
                        color='#40403d' />
                </TouchableOpacity>
            </View> */}
                {/* <Feather   
                    name='edit'
                    size={20}
                    color='#000' />
                <Feather   
                    name='menu'
                    size={23}
                    color='#000' /> */}
     
        </View>

        { initialLoading && <LoadingView/> }

        <View style={styles.categoryList}>
            <ScrollView   horizontal={true} showsHorizontalScrollIndicator={false}>
        
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={()=> setCategoryId('')}
                        style={[styles.categoryBtn, !categoryId && styles.selectedCategory]}>
                        <Text style={[styles.categoryTxt, !categoryId && styles.selectedCategoryTxt]}>All</Text>
                    </TouchableOpacity>
                    {
                        categories?.map((category, index) =>{
                            return <TouchableOpacity
                                key={index}
                                activeOpacity={0.8}
                                style={[styles.categoryBtn, categoryId === category.table_id && styles.selectedCategory]}
                                onPress={()=> setCategoryId(category.table_id)}>
                                <Text style={[styles.categoryTxt, categoryId === category.table_id && styles.selectedCategoryTxt]}>{category.name}</Text>
                            </TouchableOpacity>
                        })
                    }
            </ScrollView>
        </View>

        {productList?.products?.length > 0 ? <FlatList
                showsVerticalScrollIndicator={false}
                data={productList?.products}
                keyExtractor={(item, index) => index.toString()}
                style={styles.list}
                ListFooterComponent={loading && <LoadingView/>}
                renderItem={({ item }) => {
                    const exists = cartProducts?.some(product => product.id === item.id);
                    return <ProductItem 
                        key={item.id}
                        item={item}
                        selected={exists}
                        requestAddCart={() => requestAddCart(item)} />
                }}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={1}
                /> : !initialLoading ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16, marginTop : -100}}>
                    Whoops!
                </Text> */}
                <Text style={{
                    marginTop: -100,
                    fontFamily: 'Pyidaungsu',
                    fontSize : 16
                }}>
                   {t('no_product')}
                </Text>
            </View> : null}
           

        <View style={styles.footer}>
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={()=> setShowCart(true)}
                style={styles.more}>
                <FontAwesome5
                    name='grip-lines'
                    size={20}
                    color='darkgray' />
            </TouchableOpacity>

            <View style={styles.cartRow}>
                <Image
                    source={require('../../../../assets/images/cart.png')}
                    style={{
                    width: 22,
                    height: 22,
                    alignSelf: 'center',
                    resizeMode: 'contain',
                    }}
                    tintColor={'#9093AC'}
                />
                <Text style={styles.cart}>{t('cart')}</Text>
            </View>
            <CButton 
                title={`${totalCost ? formatTranMoney(totalCost) : 0} MMK`} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                action={() =>  setShowCart(true)}/>
        </View>

        <Modal
            visible={showCart}
            onRequestClose={()=> setShowCart(false)}
            animationType="slide"
            transparent={true}>
            <TouchableWithoutFeedback onPress={()=> setShowCart(false)}>
                <View style={styles.overlay} />
            </TouchableWithoutFeedback>
            <View style={styles.modal} onPress={()=> setShowCart(false)}>
                <View style={[styles.headerContent2, {paddingTop: getStatusBarHeight()}]}>
                    <View style={[styles.titleRow]}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => props.navigation.goBack()}>
                            <Ionicons   
                                name='close'
                                size={22}
                                color='#000' />
                        </TouchableOpacity>
                        <Text style={styles.title}>
                            {t('add_transaction')}
                        </Text>
                        {/* <Ionicons
                            name='ios-barcode-outline'
                            size={22}
                            color='#000' /> */}
                    </View>  
                </View>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={()=> setShowCart(false)}
                    style={[styles.more, {paddingTop: 10}]}>
                    <FontAwesome5
                        name='grip-lines'
                        size={18}
                        color='darkgray' />
                </TouchableOpacity>

                <View style={[styles.cartRow, {paddingLeft: width * 0.04 }]}>
                    <Image
                        source={require('../../../../assets/images/cart.png')}
                        style={{
                        width: 22,
                        height: 22,
                        alignSelf: 'center',
                        resizeMode: 'contain',
                        }}
                        tintColor={'#000'}
                    />
                    <Text style={[styles.cart, {color:"#000"}]}>
                        {t('cart')}
                    </Text>
                </View>

                <View style={{flexDirection:'row',justifyContent:'space-between', width: width * 0.92, marginLeft: width * 0.04, paddingTop: 10,}}>
                    <Text style={{fontFamily:'Lexend',color:'gray',fontSize:14}}>
                        {`${cartProducts?.length} ${cartProducts?.length > 1 ? 'items' : 'item'} in your cart`}
                    </Text>
                    <TouchableOpacity 
                        style={{flexDirection: 'row', alignItems: 'center'}}
                        onPress={() => setShowCart(false)}>
                        <Ionicons
                            name='add'
                            size={18}
                            color='#4157FF' />
                        <Text style={{fontFamily:'Lexend',color:'#4157FF',fontSize:14, paddingHorizontal: 6}}>{t('add_more')}</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView showsVerticalScrollIndicator={false} style={{marginTop: 15, marginHorizontal: width * 0.04, paddingTop: 15}}>
                    {cartProducts?.map((product, index) => {
                        return <CartItem 
                            key={index}
                            item={product}
                            increment={increment}
                            decrement={decrement}
                            updateQty={updateQty} 
                            removeCartHandler={removeCartHandler} />
                    })}
                </ScrollView>

                <View style={{ alignSelf: 'center',marginBottom: 10 }}>
                    <CButton 
                        title={`${totalCost ? formatTranMoney(totalCost) : 0} MMK`} 
                        color={colors.mainLinkColor}
                        fontFamily={'Pyidaungsu-Bold'}
                        width={width - 32}
                        height={50}
                        backgroundColor={'#F7EE25'}
                        action={() =>  {
                            cartProducts?.length > 0 && props.navigation.navigate('Receipt', {products : cartProducts, transaction : null })
                        }} />
                </View>

            </View>
        </Modal>

    </View>
}

const mapstateToProps = state => {
    return {
        products : state.products
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((AddTransaction));