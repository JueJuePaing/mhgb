import { Dimensions, StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f8f8f8'
    },
    headerContent: {
        backgroundColor: '#f8f7eb',
        paddingTop: getStatusBarHeight() + 20,
        height : height * 0.2
        // paddingBottom: 13
    },
    headerContent2: {
        backgroundColor: '#f8f7eb',
        paddingTop: getStatusBarHeight() + 20,
        paddingBottom: 20
    },
    titleRow: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        paddingHorizontal: width * 0.04
    },
    title: {
        fontFamily: 'Lexend-Medium',
        fontSize: 15,
        paddingLeft : 5
        // width : width * 0.7
    },
    categoryList: {
        backgroundColor: '#f2f4fe',
        paddingHorizontal : width * 0.04,
        height: height * 0.1
        // flexDirection: 'row',
        // maxHeight: 70,
        // height: 70,
        // paddingTop: 15
    },
    categoryBtn: {
        paddingHorizontal : 14,
        borderRadius: 11,
        backgroundColor: '#e0e3fd',
        marginRight: 12,
        // height: 40,
        justifyContent: 'center',
       
        height : height * 0.05,
        marginTop : height * 0.022
    },
    selectedCategory: {
        backgroundColor: '#2b3186'
    },
    list : {
       // backgroundColor:'red',
        height : height * 0.5,
        marginTop : 10,
        marginBottom : 150
    },
    categoryTxt: {
        color: '#7582f7',
        fontSize: 12.5,
        fontFamily: 'Lexend'
    },
    selectedCategoryTxt: {
        color: '#fff'
    },
    footer: {
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        backgroundColor: '#fff',
        // paddingBottom: 15,
        width: width,
        // paddingTop: 8,
        paddingLeft: 25,
        height : height * 0.2
    },
    more: {
        alignSelf: 'center',
        width: width * 0.5,
        alignItems:'center',
        justifyContent: 'center',
        height: 30
    },
    cartRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,

    },
    cart:{
         fontFamily: 'Lexend-Medium',
         fontSize: 15,
         color: 'gray',
         paddingLeft: 9
    },
    overlay: {
        flex: 1,
        //backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modal: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'white',
        height: height
    },
    // searchInput: {
    //     height: 50,
    //     // borderWidth: 0.2,
    //     borderColor: 'lightgray',
    //     borderRadius: 15,
    //     paddingHorizontal : 15,
    //     // marginTop: 10,
    //     fontFamily: 'Lexend',
    //     width : width * 0.8,
    //     backgroundColor: '#fff'
    //   },
      searchInput: {
        alignItems:'center',
        backgroundColor:'#fff',
        alignSelf: 'center',
        height: 50,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        width : width * 0.92,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 15,
    
        flexDirection: 'row', 
        marginTop: 10

    },
    searchIcon: {
      width: 18,
      height: 18,
      resizeMode: 'contain',
      marginLeft: 10
    },
    inputBox: {
      width: width * 0.74,
      fontFamily: 'Lexend',
      // backgroundColor:'red',
      paddingLeft : 10
    }
});

export default styles;