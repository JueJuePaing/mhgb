import React, {useState, useEffect} from 'react';
import { DeviceEventEmitter, Text, Image, TextInput, View, StatusBar, Dimensions, TouchableOpacity, FlatList} from 'react-native';
import {useTranslation} from "react-i18next";
import { LinearGradient } from 'expo-linear-gradient';
import {Entypo, MaterialCommunityIcons} from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import { showAlert } from 'react-native-customisable-alert';
import * as FileSystem from 'expo-file-system';
import * as XLSX from 'xlsx';
import * as Sharing from 'expo-sharing';
import moment from 'moment';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { formatTranMoney } from '../../../../configs/helper';

import styles from './styles';
import NavigationHeader from '../../../Components/NavigationHeader';
import Plus from '../../../../assets/icons/Plus';
import BottomSheet from '../../../Components/BottomSheet';
import CButton from '../../../Components/CButton';
import LoadingView from '../../../Components/Loading';
import BalanceItem from '../../../Components/BalanceItem';

const device = Dimensions.get('window');

const AllProducts = ({navigation, route}) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const [
        balanceList,
        setBalanceList
    ] = useState();

    const [
        fileName,
        setFileName
    ] = useState();
    const [
        inputFocused,
        setInputFocused
    ] = useState(false)
    const [
        scanModalVisible,
        setScanModalVisible
    ] = useState(false);
    const [
        exportModalVisible,
        setExportModalVisible
    ] = useState(false);
    const [
        searchName,
        setSearchName
    ] = useState('');

    const [refreshing,setRefreshing] = useState(false);
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [loading, setLoading] = useState(false);
    const [initialLoading, setInitialLoading] = useState(false);

    useEffect(()=> {
        const eventEmitter = DeviceEventEmitter.addListener(
            'all_products_change',
            val => {
                setPage(1);
                setBalanceList();
                setInitialLoading(true);
                setSearchName('');
                getBalanceList();
            },
          );
          return () => eventEmitter;
    }, [])

    useEffect(() => {
        if (!searchName) {
          searchHandler();
        }
    }, [searchName]);

    const searchHandler = () => {
        setPage(1);
        setBalanceList();
        setInitialLoading(true);
        getBalanceList();
    }
    
    useEffect(() => {
        setInitialLoading(true);
        getBalanceList();
    }, []);

    useEffect(() => {
            getBalanceList()
    }, [page]);

    const getBalanceList = async () => {
        try {
            setLoading(true);

            let formdata = new FormData();
            formdata.append('page',page);
            formdata.append('per_page', 16);
            formdata.append('name', searchName);

            const response = await ApiService(url.balance_list, formdata, 'POST');

            if (response.code == '200') {
                const newBalances = page > 1 ? balanceList?.balances?.concat(response.balances) : response.balances;
                setBalanceList({...response, balances: newBalances});
                if(response.next_pages != ''){
                    setLoadMore(true);
                }else{
                    setLoadMore(false);
                }
            } else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
            // setInitialLoading(false);
            // setRefreshing(false);
            // setLoading(false)
        } finally {

            setInitialLoading(false);
            setRefreshing(false);
            setLoading(false)
        }
    }


    const handleRefresh = () => {
        if (refreshing) {
            return;
        }
        setRefreshing(true);
        setPage(1);
        setBalanceList();
        getBalanceList();
    }

    const filterHandler =() => {}

    const handleCloseModal = () => {
        setExportModalVisible(false);
        setScanModalVisible(false);
      };


    const addStock = () => {
        setScanModalVisible(false);
        navigation.navigate('AddStock', {scanProducts : null});
    }
    
    const exportToExcel = async () => {
        setExportModalVisible(false)
        setInitialLoading(true);
        let data = [];

        for (const balance of balanceList?.balances) {
            const item = {
                "Transaction ID" : balance.transaction_id,
                "Name" : balance.name,
                "PID" : balance.id,
                "Product Code" : balance.product_code,
                "Total Quantity" : balance.total_quantity,
                "Used Quantity" : balance.used_quantity,
                "Remaining Quantity" : balance.remain_quantity,
                "Purchase Price" : balance.purchase_price,
                "Sale Price" : balance.sale_price,
                "Created At" :  moment(balance.created_at).format('DD/MM/YY')
            }
            data.push(item);
        }

        var ws = XLSX.utils.json_to_sheet(data);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Cities");
        const wbout = XLSX.write(wb, {
          type: 'base64',
          bookType: "xlsx"
        });
        const uri = FileSystem.documentDirectory + fileName + '.xlsx';
       
        try {
            await FileSystem.writeAsStringAsync(uri, wbout, {
                encoding: FileSystem.EncodingType.Base64
              });

            setInitialLoading(false)
            setFileName();
            await Sharing.shareAsync(uri);
        } catch (error) {
            console.error("write error >> ", error);
            setInitialLoading(false)
            setFileName();
            showAlert({
                title: `Oops!`,
                message: "Exporting to Excel fails!",
                alertType: 'error',
                dismissable: true,
            });
            
        }
      };

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const itemHandler = (product) => {
        navigation.navigate('AddStock', {scanProducts : [product]});
    }

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#F7FBFF'} />
        <NavigationHeader 
            title={t('remain_stock')}
            action={() => navigation.goBack()} 
            filterHandler={filterHandler}
            // searchHandler={searchHandler}
            />

        { initialLoading && <LoadingView/> }

        <BottomSheet 
            visible={exportModalVisible} 
            onClose={handleCloseModal}>
            <View style={{marginTop:20}}/>
            <Text style={styles.modalTitle}>
                {t('export_as_excel')}
            </Text>
            <TextInput 
                maxLength={50}
                onChangeText={(name) => setFileName(name) }
                onFocus={() => setInputFocused(true) }
                onBlur={() => setInputFocused(false) }
                value={fileName}
                placeholder={t('enter_file_name')}
                selectionColor={colors.inputActiveColor}
                style={[styles.input,{borderBottomColor: inputFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                    }]} />

            <CButton 
                txtSize={13.5}
                title={t('save')} 
                disabled={!fileName}
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                action={() => exportToExcel() }
            />
        </BottomSheet>

        {/* <BottomSheet visible={scanModalVisible} onClose={handleCloseModal}>
            <View style={{marginTop:30}}/>
            <CButton 
                title={t('made_with_qr')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#F7EE25'}
                action={() => addStockByQr() }
            />
            <View style={{marginTop:10}}/>
            <CButton 
                title={t('create_yourself')} 
                color={colors.mainLinkColor}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#fff'}
                action={() => addStock() }
            />
            <View style={{marginTop:30}}/>
        </BottomSheet> */}

        <View style={{
            backgroundColor: '#f7f5fe'
        }}>
            <View style={styles.searchInput}>
                <TextInput
                    style={styles.inputBox}
                    placeholder={t('search_products')}
                    placeholderTextColor='#999'
                    value={searchName}
                    onChangeText={ setSearchName }
                />
                <TouchableOpacity onPress={searchHandler}>
                    <Image
                    source={require('../../../../assets/images/search.png')}
                    style={styles.searchIcon}
                    />
                </TouchableOpacity>
            </View>

            <LinearGradient
                colors={['rgba(131,117,241,1)','rgba(91,86,192,1)','rgba(35,38,99,1)']} 
                style={styles.avenueContainer}
                >
                <View style={styles.avenueBlock}>
                    <Text style={styles.stock}>
                        {balanceList?.total_quantity ? formatTranMoney(balanceList.total_quantity) : 0}
                    </Text>
                    <Text style={styles.stockLabel}>
                        {t('total_stock_in_hand')}
                    </Text>
                </View>
                <View style={styles.avenueBlock}>
                    <Text style={styles.price}>
                        {balanceList?.total_purchase_price ? formatTranMoney(balanceList.total_purchase_price) : 0}
                    </Text>
                    <Text style={styles.stockLabel}>
                        {t('total_purchase_price')}
                    </Text>
                </View>
            </LinearGradient>
        </View>
        

        <View style={styles.listContainer}>
            {balanceList?.balances?.length > 0 ? <FlatList
                showsVerticalScrollIndicator={false}
                data={balanceList?.balances}
                keyExtractor={(item, index) => index.toString()}
                ListFooterComponent={loading && <LoadingView/>}
                renderItem={({ item }) => (
                    <BalanceItem 
                        key={item.id} 
                        product={item}
                        itemHandler={ itemHandler } />
                )}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={1}
                onRefresh={handleRefresh}
                refreshing={refreshing}
                /> : !initialLoading ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16, marginTop : 120}}>
                    Whoops!
                </Text> */}
                <Text style={{
                    marginTop: 10,
                    fontFamily: 'Pyidaungsu',
                    fontSize : 14.5
                }}>
                   {t('no_product')}
                </Text>
            </View> : null}
        </View>

        <View style={styles.shadowContainer}>
            <View style={styles.footer}>
                <View style={styles.footerRow}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={()=> setExportModalVisible(true)}
                        style={styles.footerBtn}>
                        <Entypo
                            name='calendar'
                            size={13}
                            color='#000' />
                        <Text style={styles.btnTxt}>
                            {t('export')}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={()=> navigation.navigate('Scanner')}
                        style={[styles.footerBtn, {backgroundColor: '#fdf4d6'}]}>
                        <MaterialCommunityIcons
                            name='barcode'
                            size={20}
                            color='#000' />
                        <Text style={styles.btnTxt}>
                            {t('scan')}
                        </Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity 
                    activeOpacity={1}
                    style={styles.addBtnOuter}>
                    <TouchableOpacity
                        onPress={() => addStock()}
                        activeOpacity={0.8}
                        style={styles.addBtn}>
                        <Plus />
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        </View>

    </View>
}


export default AllProducts;