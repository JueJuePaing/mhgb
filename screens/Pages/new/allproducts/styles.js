import { Dimensions, StyleSheet } from "react-native";

const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    shadowContainer: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'transparent'
    },
    footer: {
        height : 50,
        backgroundColor: '#fff',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,

        shadowColor: '#000',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 6,
        elevation: 8,
        justifyContent: 'center'
    },
    addBtnOuter: {
        alignSelf: 'center',
        top: -30,
        width: 62,
        height: 62,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F7FBFF',
        borderRadius: 40,
        position: 'absolute'
    },
    addBtn: {
        backgroundColor: '#323790',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,

        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        elevation: 4, 

    },


    container : {
        flex : 1,
        backgroundColor : '#fff', //
    },
    content: {
        paddingHorizontal : '4%',
    },
    avenueContainer: {
        width: '94%',
        marginTop: 5,
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingBottom: 20,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 20,
        marginBottom : 10
    },
    avenueBlock: {

    },
    stock: {
        fontSize: 16,
        fontFamily: 'Lexend-Medium',
        color : '#fff'
    },
    stockLabel: {
        fontSize: 9,
        fontFamily: 'Lexend-Light',
        color : '#e8e8df',
        paddingTop: 6
    },
    price: {
        fontSize: 16,
        fontFamily: 'Lexend-Bold',
        color : '#fff'
    },

    listContainer: {
        backgroundColor: '#fff',
        flexGrow: 1,
        marginTop: 15,
        marginBottom : 310
    },
    footerRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: width * 0.05
    },
    footerBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#e2dffb',
        borderRadius: 5,
        width: width * 0.27,
        justifyContent: 'center',
        paddingVertical: 5
    },
    btnTxt: {
        paddingLeft: 4,
        color: '#000',
        fontSize: 13
    },
    modalTitle: {
        color: '#2b30c2',
        fontFamily: 'Lexend-Bold',
        fontSize: 13,
        paddingBottom: 8,
        fontWeight: '600'
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:14,
        fontFamily:'Pyidaungsu',
        color: '#000',
        color:'#000',
        borderColor:'#000',
        marginBottom: 25,
        marginTop: 15
      },

      searchInput: {
        alignItems:'center',
        backgroundColor:'#fff',
        alignSelf: 'center',
        height: 50,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        width : width * 0.92,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.84,
        elevation: 2,
        paddingLeft: 15,
    
        flexDirection: 'row', 
        marginVertical: 10,
    

    },
    searchIcon: {
      width: 18,
      height: 18,
      resizeMode: 'contain',
      marginLeft: 10
    },
    inputBox: {
      width: width * 0.74,
      fontFamily: 'Lexend',
      // backgroundColor:'red',
      paddingLeft : 10
    }

    
});

export default styles;