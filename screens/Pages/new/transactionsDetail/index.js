import React, {useEffect, useRef, useState, useContext} from 'react';
import { ScrollView, Text, View, StatusBar, Dimensions,Image, TouchableOpacity, Animated} from 'react-native';
import {useTranslation} from "react-i18next";
import { useTheme } from '@react-navigation/native';
import { showAlert } from 'react-native-customisable-alert';
import { connect } from 'react-redux';
import moment from 'moment';
import {
    NetPrinter,
    USBPrinter
} from 'react-native-thermal-receipt-printer-image-qr';
import { captureRef } from 'react-native-view-shot';
import * as FileSystem from 'expo-file-system';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { AppContext } from '../../../../context/AppContextProvider';

import styles from './styles';
import CButton from '../../../Components/CButton';
import LoadingView from '../../../Components/LoadingView';
import { formatTranMoney } from '../../../../configs/helper';

const device = Dimensions.get('window');

const TransactionDetail = (props) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const {
        netPrinterConnected,
        usbPrinterConnected,
        profileData
    } = useContext(AppContext);

    const {item} = props.route.params;

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        detailData,
        setDetailData
    ] = useState();

    const viewRef = useRef();

    useEffect(() => {
        if (item) {
            getTransactionDetail(item.invoice_no);
        }
    }, [item]);


    const getTransactionDetail = async (no) => {
        try {
            setLoading(true);
            let formdata = new FormData();
            formdata.append('invoice_no', no);

            const response = await ApiService(url.shop_invoices_detail, formdata, 'POST');

            if (response.code == '200') {
                setDetailData(response.invoice);
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const printReceipt = async () => {

        if (viewRef.current) {
          try {
            setLoading(true)
            const result = await captureRef(viewRef, {
              format: 'jpg', // Set the format to 'jpg'
              quality: 1, // Set the image quality (0.0 - 1.0)
            });
    
            if (result) {

                try {
                    const convertedImg = await convertImageToBase64(result);
            
                    let qrProcessed = convertedImg.replace(/(\r\n|\n|\r)/gm, '');

                    const height = detailData?.products?.length <= 3 ? 600 : 600 + ((detailData?.products?.length-3) * 60);

                    // await NetPrinter.printImageBase64(
                    //     qrProcessed,
                    //     {
                    //         imageWidth: 500,
                    //         imageHeight: height,
                    //     },
                    //     );
                        
                    //     await NetPrinter.printBill('', {beep: false});

                    if (usbPrinterConnected) {
                        await USBPrinter.printImageBase64(
                            qrProcessed,
                            {
                                imageWidth: 500,
                                imageHeight: height,
                            },
                            );
                            
                        await USBPrinter.printBill('', {beep: false});
                    } else {
                        await NetPrinter.printImageBase64(
                            qrProcessed,
                            {
                                imageWidth: 500,
                                imageHeight: height,
                            },
                            );
                            
                        await NetPrinter.printBill('', {beep: false});
                    }
          
                } catch (e) {
                    console.error('printer e >> ', e);
                    setLoading(false)
                } finally {
                    setLoading(false)
                }
            } else {
                setLoading(false)
            }
    
            // You can also do other things with the screenshot, like sending it to a server
          } catch (error) {
            console.error('Error capturing screenshot:', error);
          } finally {
            setLoading(false)
          }
        }
      };


    const convertImageToBase64 = async (imageUri) => {
        try {
          // Read the image file
          const fileInfo = await FileSystem.getInfoAsync(imageUri);
      
          if (fileInfo.exists) {
            // Read the image file as a base64 string
            const base64Image = await FileSystem.readAsStringAsync(imageUri, {
              encoding: FileSystem.EncodingType.Base64,
            });
      
            return base64Image;
          } else {
            console.error('Image file does not exist.');
            return null;
          }
        } catch (error) {
          console.error('Error converting image to Base64:', error);
          return null;
        }
      };

    const currentDate = moment();

    const formattedDate = detailData?.created_at ? moment(detailData?.created_at).format('DD/MM/YY') : currentDate.format('DD/MM/YY');

    return <ScrollView style={{backgroundColor: '#dedfe3',}} showsVerticalScrollIndicator={false}>
    <View style={styles.container}>
        <StatusBar backgroundColor={'#dedfe3'} />
        <TouchableOpacity
            style={styles.backButton}
            onPress={() => props.navigation.goBack()}>
            <Image
                source={require('../../../../assets/images/back.png')}
                tintColor={'#090F47'}
                style={styles.backButtonImg}
                />
        </TouchableOpacity>
        <View style={styles.mainContent}  ref={viewRef}>

            
            
            { loading && <LoadingView/> }
            <View style={styles.header}>
                {/* <Image
                    source={profileData?.logo ? {uri : profileData?.logo} : require('../../../../assets/images/default_user.png')}
                    style={styles.logo} />
                <View> */}
                    <Text style={styles.name}>{profileData?.name}</Text>
                    <Text style={styles.phone}>{profileData?.phone}</Text>
                    <Text style={styles.phone}>{profileData?.address}</Text>

            </View>

            {/* <View style={styles.receiptRow}>
                <Text style={styles.receipt}>{t('receipt')}</Text>
                <View>
                    <Text style={[styles.receiptInfo, {marginBottom: 3}]}>
                        {`#${detailData?.invoice_id}`}
                    </Text>
                    <Text style={styles.receiptInfo}>
                        {`${t('receipt_date')}: ${formattedDate}`}
                    </Text>
                </View>
            </View> */}

                <Text style={styles.receiptInfo2}>
                    {`${t('receipt_date')} : ${formattedDate}`}
                </Text>

            {/* <View style={styles.dashRow}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View> */}

            <View style={[styles.receiptRow, {paddingTop: 2}]}>
               <View style={styles.column1}>
                    <Text style={styles.colHeader}>{t('receipt_item')}</Text>
                    {detailData?.products?.map((product, index) => {
                        return <Text key={index} style={styles.itemName}>
                            {`${index+1}. ${product.name}`}
                        </Text>
                    })}
               </View>
               <View style={styles.column2}>
                    <Text style={styles.colHeader}>{t('receipt_quantity')}</Text>
                    {detailData?.products?.map((product, index) => {
                        return <Text key={index} style={styles.qty}>{product.quantity}</Text>
                    })}
                </View>
                <View style={styles.column3}>
                    <Text style={styles.colHeader}>{t('price')}</Text>
                    {detailData?.products?.map((product, index) => {
                        return <Text key={index} style={styles.price}>
                            {product.sale_price ? formatTranMoney(product.sale_price * product.quantity) : 0}
                        </Text>
                    })}
                </View>
            </View>

            <View style={{marginVertical: 5}}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={styles.totalLabel}>
                    {`${t('receipt_sub_total')}`}
                </Text>
                <Text style={styles.totalVal}>{detailData?.sub_total ? formatTranMoney(detailData.sub_total) : 0}</Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={styles.totalLabel}>
                    {`${t('receipt_tax')}`}
                </Text>
                <Text style={styles.totalVal}>
                    {detailData?.tax ? formatTranMoney(detailData.tax) : 0}
                </Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={[styles.totalLabel, {fontFamily: 'Lexend-Bold'}]}>
                    {`${t('receipt_total')}`}
                </Text>
                <Text style={[styles.totalVal, {fontFamily: 'Lexend-Bold', marginTop: 4}]}>
                    {detailData?.total ? formatTranMoney(detailData.total) : 0}
                </Text>
            </View>

            <View style={{marginVertical: 5}}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View>
{/* 
            <Text style={styles.note}>
                {`${t('receipt_note')} :`}
            </Text> */}
            <Text style={styles.description}>
               {t('receipt_desc')}
            </Text>

            <Text style={styles.thank_you}>
                {t('thank_you')}
            </Text>

        </View>

            <View style={{marginTop: 20, marginBottom : 15}}>
                <CButton 
                    title={t('back_btn')} 
                    color={'#fff'}
                    fontFamily={'Pyidaungsu-Bold'}
                    width={device.width - 32}
                    height={50}
                    backgroundColor={'#2b3186'}
                    action={() => props.navigation.goBack() }
                />
                <View style={{marginTop : 10}} />
                <CButton 
                    title={t('print')} 
                    color={colors.mainLinkColor}
                    fontFamily={'Pyidaungsu-Bold'}
                    width={device.width - 32}
                    height={50}
                    backgroundColor={'#F7EE25'}
                    action={() => printReceipt() }
                />
            </View>
       
      
    </View>
    </ScrollView>
}



const mapstateToProps = state => {
    return {

    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((TransactionDetail));