import React, {useState, useEffect, useRef, useContext} from 'react';
import { ScrollView, Text, DeviceEventEmitter, View, StatusBar, Dimensions,Image, TouchableOpacity, Animated} from 'react-native';
import {useTranslation} from "react-i18next";
import moment from 'moment';
import { showAlert, closeAlert } from 'react-native-customisable-alert';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { NetPrinter, USBPrinter } from 'react-native-thermal-receipt-printer-image-qr';
import { captureRef } from 'react-native-view-shot';
import * as FileSystem from 'expo-file-system';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { addOrderReceipt, removeOrderReceipt, decreaseProductRemainQty } from '../../../../stores/actions';
import { AppContext } from '../../../../context/AppContextProvider';

import CButton from '../../../Components/CButton';
import styles from './styles';
import LoadingView from '../../../Components/Loading';
import { formatTranMoney } from '../../../../configs/helper';

const device = Dimensions.get('window');

const Receipt = (props) => {
    const [t] = useTranslation('common');
    const {products, transaction} = props.route.params;
    const {
        netPrinterConnected,
        usbPrinterConnected,
        profileData,
        changeUsbConnected
    } = useContext(AppContext);

    const viewRef = useRef();

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        taxPercentage,
        setTaxPercentage
    ] = useState(0.001);
    const [
        receiptProducts,
        setReceiptProducts
    ] = useState();
    const [
        tax,
        setTax
    ] = useState(0);
    const [
        printData,
        setPrintData
    ] = useState();

    const currentDate = moment();

    // Format the current date as "DD/MM/YY"
    const formattedDate = currentDate.format('DD/MM/YY');

    useEffect(() => {
        USBPrinter.init().then(()=> {

            //list printers
            USBPrinter.getDeviceList().then((printers) => {
                if (printers && printers.length > 0)
                    connectUSBPrinter(printers[0])
            });
          })
    }, []);

    const connectUSBPrinter = async (printer) => {
        const status = await USBPrinter.connectPrinter(
            printer.vendor_id, 
            printer.product_id
          );
        console.log("Connected to usb printer .. ", status, status.product_id);
        if (status?.product_id) {
            changeUsbConnected(true);
        }
    }


    useEffect(() => {
        if (transaction) {
            setReceiptProducts(transaction.products)
        } else {
            setReceiptProducts(products)
        }
    }, [products, transaction]);

    useEffect(() => {
        const formattedData = receiptProducts?.map((item, index) => [`${index+1}. ${item.name}`, item.count+'', formatTranMoney(item.sale_price * item.count)+'']);
        if (formattedData) {
            setPrintData(formattedData);
        }
    }, [receiptProducts]);

    const [isConnection, SetIsConnection] = useState(true);

    useEffect(() => {
      const unsubscribe = NetInfo.addEventListener(state => {
          handleConnectivityChange(state.isInternetReachable)
      });
      return unsubscribe;
    }, [])

    const handleConnectivityChange = isConnection => {
        SetIsConnection(isConnection);
    };

    useEffect(() => {
        if (!transaction) {
            if (isConnection) {
                getTax();
            } else {
                setTaxPercentage(parseInt(props.taxPercent));
            }
        }
    }, [isConnection]);

    const getTax = async () => {
        const response = await ApiService(url.tax, [] , 'POST');
            if (response?.code == '200' && response?.tax_percentage) {
                setTaxPercentage(response.tax_percentage);
            }
    }

    const totalCost = transaction ? transaction.sub_total : products?.reduce((accumulator, item) => {
        return accumulator + item.count * item.sale_price;
      }, 0);

    const total = transaction ? transaction.total : totalCost + tax;

    useEffect(() => {
        let data = 0;
        if (transaction)
            data = transaction.tax;
        else {
            if (totalCost) {
                if (taxPercentage) {  
                    data = totalCost * taxPercentage;
                } else {
                    data = totalCost * 0.001;
                }
            }
        } 
        setTax(data);
    }, [transaction, totalCost, taxPercentage]);

    const receiptHandler = () => {
        if (isConnection) {
            if (transaction) {
                finshTransaction(1)
            }
            else {
                // finshTransaction(2)
                showAlert({
                    title: t('want_to_print'),
                    message: '',
                    alertType: 'warning',
                    customIcon: 'none',
                    onPress: () => finshTransaction(2),
                    onDismiss: () => finshTransaction(1)
                });
            }
        } else {
            if (transaction) {
                showAlert({
                    title: `Oops!`,
                    message: 'ကွန်ရက်ချိတ်ဆက်မှု မရှိပါ။',
                    alertType: 'error',
                    dismissable: true,
                });
            } else {
                const receipt = {
                    sub_total : totalCost,
                    tax,
                    total,
                    products
                }
                props.addOrderReceipt(receipt);

                captureScreenshot();

                // props.decreaseProductRemainQty(receiptProducts);
                props.navigation.replace('OK',{
                    'image' : require('../../../../assets/images/return_confirm.png'),
                    'title' : t('ok'),
                    'btnTitle' : t('back_to_home'),
                    'description' : t('temp_new_transaction_successful'),
                    // 'isTransaction' : true,
                    'isBack' : true
                  })
            }
        }
        
    }

    const convertImageToBase64 = async (imageUri) => {
        try {
          // Read the image file
          const fileInfo = await FileSystem.getInfoAsync(imageUri);
      
          if (fileInfo.exists) {
            // Read the image file as a base64 string
            const base64Image = await FileSystem.readAsStringAsync(imageUri, {
              encoding: FileSystem.EncodingType.Base64,
            });
      
            return base64Image;
          } else {
            console.error('Image file does not exist.');
            return null;
          }
        } catch (error) {
          console.error('Error converting image to Base64:', error);
          return null;
        }
      };
    
    const printReceipt = async (uri) => {
        try {
            const convertedImg = await convertImageToBase64(uri);

            let qrProcessed = convertedImg.replace(/(\r\n|\n|\r)/gm, '');

            const height = receiptProducts?.length <= 3 ? 600 : 600 + ((receiptProducts?.length-3) * 70);

         if (usbPrinterConnected) {
                await USBPrinter.printImageBase64(
                    qrProcessed,
                    {
                        imageWidth: 500,
                        imageHeight: height
                    },
                );
                    
                await USBPrinter.printBill('', {beep: false});

   
            } else {
                await NetPrinter.printImageBase64(
                    qrProcessed,
                    {
                        imageWidth: 500,
                        imageHeight: height
                    },
                );
                    
                await NetPrinter.printBill('', {beep: false});
            }

            navigateToOk();
        } catch (e) {
            console.error('printer e >> ', e);
        }
      };

    const finshTransaction = async (status) => { 
        try {
            closeAlert();
            setLoading(true);
            let formdata = new FormData();
            formdata.append('orders[0][sub_total]', totalCost);
            formdata.append('orders[0][tax]', parseInt(tax));
            formdata.append('orders[0][total]', parseInt(total));

            receiptProducts?.map((product, index) => {
                formdata.append(`orders[0][products][${index}][product_id]`, product.id);
                formdata.append(`orders[0][products][${index}][quantity]`, product.count);
                formdata.append(`orders[0][products][${index}][sale_price]`, product.sale_price);
            })


            const response = await ApiService(url.shop_order, formdata , 'POST');

            if (response.code == '200') {
                if (status === 1) {
                    navigateToOk();
                } else {
                    // testing
                    if (netPrinterConnected || usbPrinterConnected) {
                        captureScreenshot();
                    } else {
                        showAlert({
                            title: `Oops!`,
                            message: t('cannot_connect_printer'),
                            alertType: 'error',
                            onPress : () => navigateToOk()
                          });
                    }
                }
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const navigateToOk = () => {
        DeviceEventEmitter.emit('dashboard_products_change', 'true');
        props.navigation.replace('OK',{
            'image' : require('../../../../assets/images/return_confirm.png'),
            'title' : t('ok'),
            'btnTitle' : t('back_to_home'),
            'description' : t('new_transaction'),
            'isBack' : true
            })
        if (transaction && transaction.id) {
            props.removeOrderReceipt(transaction.id);
        } 
    }
    

  

    const captureScreenshot = async () => {
        if (viewRef.current) {
          try {
            const result = await captureRef(viewRef, {
              format: 'jpg', // Set the format to 'jpg'
              quality: 1, // Set the image quality (0.0 - 1.0)
            });
    
            // Save the screenshot to the device's media library (optional)
            if (result) {
                printReceipt(result);
            }
    
            // You can also do other things with the screenshot, like sending it to a server
          } catch (error) {
            console.error('Error capturing screenshot:', error);
          }
        }
      };


    return <ScrollView style={{backgroundColor: '#dedfe3',}} showsVerticalScrollIndicator={false}>
    <View style={styles.container}>
        <StatusBar backgroundColor={'#dedfe3'} />

        <TouchableOpacity
            style={styles.backButton}
            onPress={() => props.navigation.goBack()}>
            <Image
                source={require('../../../../assets/images/back.png')}
                tintColor={'#090F47'}
                style={styles.backButtonImg}
                />
        </TouchableOpacity>

        <View style={styles.mainContent}   ref={viewRef}>
            { loading && <LoadingView/> }
            <View style={styles.header}>
                {/* <Image
                    source={profileData.logo ? {uri : profileData.logo} : require('../../../../assets/images/default_user.png')}
                    style={styles.logo} /> */}
                {/* <View> */}
                    <Text style={styles.name}>{profileData?.name}</Text>
                    <Text style={styles.phone}>{profileData?.phone}</Text>
                    <Text style={styles.phone}>{profileData?.address}</Text>
                {/* </View> */}
            </View>

            {/* <View style={styles.receiptRow}>
                <Text style={styles.receipt}>{t('receipt')}</Text>
                <View>
                    <Text style={styles.receiptInfo}>
                        {`${t('receipt_date')}: ${formattedDate}`}
                    </Text>
                </View>
            </View> */}

                <Text style={styles.receiptInfo2}>
                    {`${t('receipt_date')} : ${formattedDate}`}
                </Text>

            {/* <View style={styles.dashRow}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View> */}

            <View style={[styles.receiptRow, {paddingTop: 2}]}>
               <View style={styles.column1}>
                    <Text style={styles.colHeader}>{t('receipt_item')}</Text>
                    {receiptProducts?.map((product, index) => {
                        return <Text key={index} style={styles.itemName}>{`${index+1}. ${product.name}`}</Text>
                    })}
               </View>
               <View style={styles.column2}>
                    <Text style={styles.colHeader}>{t('receipt_quantity')}</Text>
                    {receiptProducts?.map((product, index) => {
                        return <Text key={index} style={styles.qty}>{product.count}</Text>
                    })}
                </View>
                <View style={styles.column3}>
                    <Text style={styles.colHeader}>{t('price')}</Text>
                    {receiptProducts?.map((product, index) => {
                        return <Text key={index} style={styles.price}>
                            {product.sale_price ? formatTranMoney(product.sale_price * product.count) : 0}
                        </Text>
                    })}
                </View>
            </View>

            <View style={{marginVertical: 5}}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={styles.totalLabel}>
                {`${t('receipt_sub_total')}`}
                </Text>
                <Text style={styles.totalVal}>{totalCost ? formatTranMoney(totalCost) : 0}</Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={styles.totalLabel}>
                {`${t('receipt_tax')}`}
                </Text>
                <Text style={styles.totalVal}>
                    {formatTranMoney(tax)}
                </Text>
            </View>

            <View style={styles.totalRow}>
                <Text style={[styles.totalLabel, {fontFamily: 'Lexend-Bold'}]}>
                    {`${t('receipt_total')}`}
                </Text>
                <Text style={[styles.totalVal, {fontFamily: 'Lexend-Bold', marginTop: 4}]}>
                    {formatTranMoney(total)}
                </Text>
            </View>

            <View style={{marginVertical: 5}}>
                <Text numberOfLines={1} ellipsizeMode='clip' style={styles.dash}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View>

            {/* <Text style={styles.note}>
                {`${t('receipt_note')} :`}
            </Text> */}
            <Text style={styles.description}>
                {t('receipt_desc')}
            </Text>

            <Text style={styles.thank_you}>
                {t('thank_you')}
            </Text>

        </View>

           {transaction ? <View style={{marginTop: 20, marginBottom : 15}}>
            <CButton 
                title={t('finish')} 
                color={'#fff'}
                fontFamily={'Pyidaungsu-Bold'}
                width={device.width - 32}
                height={50}
                backgroundColor={'#2b3186'}
                action={() => receiptHandler() }
            />
            </View>
           :  <View style={{marginTop: 20, marginBottom : 15}}>
           <CButton 
               title={isConnection ? t('finish') : t('temp_finish')} 
               color={'#fff'}
               fontFamily={'Pyidaungsu-Bold'}
               width={device.width - 32}
               height={50}
               backgroundColor={'#2b3186'}
               action={() => receiptHandler() }
           />
       </View>}
    </View>
    </ScrollView>
}

const mapstateToProps = state => {
    return {
        taxPercent : state.taxPercent,
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        addOrderReceipt: receipt => {
            dispatch(addOrderReceipt(receipt))
          },
        decreaseProductRemainQty: products => {
            dispatch(decreaseProductRemainQty(products))
          },
        removeOrderReceipt: id => {
            dispatch(removeOrderReceipt(id))
          },
    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((Receipt));