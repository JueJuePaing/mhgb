import { Dimensions, StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dedfe3',
        alignItems: 'center',
        height: '100%',
        alignItems: 'center'
    },
    mainContent: {
        backgroundColor: '#fff',
        width: width * 0.9,
        borderRadius: 3,
        padding: 10,
        marginTop: getStatusBarHeight() + 70,
        // marginBottom : 30
        // height: height * 0.7
    },
    header: {
        // flexDirection:'row',
        alignItems: 'center',
        // paddingLeft: width * 0.01,
        paddingVertical: 10,
        justifyContent: 'center'
    },
    logo: {
        width: width * 0.11,
        height: width * 0.11,
        resizeMode : 'contain',
        marginRight: 10
    },
    name: {
          fontFamily : 'Lexend-Bold',
        color: '#a6302e',
        fontSize: 13.5,
        paddingBottom: 6
    },
    phone: {
          fontFamily : 'Lexend-Light',
      //  color: 'gray',
        fontSize: 13,
        color: '#2e2e2c'
    },
    receiptRow: {
        flexDirection:'row',
        alignItems: 'center',
        paddingHorizontal: width * 0.01,
        paddingTop: 15,
        justifyContent: 'space-between',
        // backgroundColor:'red'
    },
    receipt: {
          fontFamily : 'Lexend-Medium',
        color: '#000',
        fontSize: 25,
        width: width * 0.56
    },
    receiptInfo: {
        //   fontFamily : 'Lexend',
        color: '#5c5858',
        fontSize: 12,
        width: width * 0.27,
        paddingHorizontal: width * 0.01,
        paddingTop: 15,
    },
    receiptInfo2: {
        color: '#3b3939',
        fontSize: 12,
        width: width * 0.27,
        marginBottom : 15
    },
    dashRow: {
        marginVertical: 10
    },
    dash: {
        color: '#000', //1c1b1b
        fontSize: 17
    },
    column1: {
        width: width * 0.36,
        // backgroundColor: 'green'
    },
    column2: {
        width: width * 0.2,
        // backgroundColor: 'pink',
        alignItems: 'center'
    },
    column3: {
        width: width * 0.27,
        // backgroundColor: 'blue',
        alignItems: 'flex-end'
    },
    colHeader: {
          fontFamily : 'Lexend-Bold',
        fontSize: 13,
        color: '#000',
        marginBottom: 25
    },
    itemName: {
        fontFamily : 'Lexend-Medium',
        color: '#000',
        fontSize: 13,
        height:35
    },
    qty: {
          fontFamily : 'Lexend',
        color: '#000',
        fontSize: 14,
        height:35
    },
    price: {
        fontFamily : 'Lexend',
        color: '#000',
        // fontWeight: 'bold',
        fontSize: 14,
        height:35
    },
    totalRow: {
        flexDirection:'row',
        alignItems: 'center',
        paddingHorizontal: width * 0.01,
        paddingTop: 10,
        justifyContent: 'space-between'
        // alignSelf: 'flex-end',
    },
    totalLabel: {
        // paddingRight: 15,
        color: '#000',
        fontFamily : 'Lexend',
        fontSize: 13,
        paddingLeft : 20
    },
    totalVal: {
        width: width * 0.27,
        // fontFamily : 'Lexend',
        color: '#000',
        fontSize: 13,
        textAlign: 'right'
    },
    note: {
          fontFamily : 'Lexend-Bold',
        color: '#000',
        fontSize: 13,
        marginTop: 15
    },
    description: {
          fontFamily : 'Lexend-Light',
        color: '#2e2e2c',
        fontSize: 12,
        marginTop: 10,
        textAlign: 'center'
    },
    thank_you: {
        color: '#000',
        fontSize: 14,
        marginTop: 10,
        textAlign: 'center',
        marginBottom : 7
    },
    backButton: {
        width: 25,
        height: 25,
        marginTop : getStatusBarHeight() + 20,
        position: 'absolute',
        left: width * 0.05,
    },
    backButtonImg: {
        width: 25,
        height: 25,
    },
    shotImageContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#3197F7',
      },
      imgTest: {
        width: width * 0.9,
        height: height * 0.9,
        resizeMode : 'contain',
        alignSelf: 'center'
      }
});

export default styles;