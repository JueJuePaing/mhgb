import React, {
    useState,
    useEffect
} from 'react';
import { Text, View, StatusBar, Dimensions, TouchableOpacity} from 'react-native';
import {useTranslation} from "react-i18next";
import { useTheme } from '@react-navigation/native';
import { showAlert } from 'react-native-customisable-alert';

import { ApiService } from '../../../../network/service';
import url from '../../../../network/url';
import { formatTranMoney } from '../../../../configs/helper';

import NavigationHeader from '../../../Components/NavigationHeader';
import styles from './styles';
import Plus from '../../../../assets/icons/Plus';
import Medicine from '../../../../assets/icons/Medicine';
import Coin from '../../../../assets/icons/Coin';
import Dollar from '../../../../assets/icons/Dollar';
import TransactionItem from '../../../Components/TransactionItem';
import LoadingView from '../../../Components/Loading';

const {width} = Dimensions.get('window');

const Dashboard = ({navigation}) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();

    const [
        revenue,
        setRevenue
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(true);
    const [
        transactions,
        setTransactions
    ] = useState();

    useEffect(() => {
        getRevenue();
        getTransactions();
    }, []);

    const getRevenue = async () => {
        try {
            const response = await ApiService(url.shop_revenue, [] , 'POST');

            if (response.code == '200') {
                setRevenue(response);
            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    const getTransactions = async () => {
        try {
            const response = await ApiService(url.shop_invoices, [] , 'POST');

            if (response.code == '200') {
                const allInvoices = response.invoices;
                if (allInvoices?.length > 2) {
                    setTransactions(allInvoices.slice(0, 2));
                } else {
                    setTransactions(allInvoices);
                }

            }else {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    return <View style={styles.container}>
        <StatusBar backgroundColor={'#F7FBFF'} />
        { loading && <LoadingView/> }

        <NavigationHeader 
            title={t('dashboard')}
            action={() => navigation.goBack()} 
            />
        <View style={styles.content}>

            <View style={styles.productsContainer}>
                <View style={styles.leftContent}>
                    <Text style={styles.label}>{t('products')}</Text>
                    <View style={styles.amtRow}>
                        <Text style={styles.amount}>
                            {revenue?.product_balance ? 
                                    revenue?.product_balance < 1000000 ?
                                        formatTranMoney(revenue.product_balance) : 
                                            `${(revenue.product_balance/1000000).toFixed(3)} M` : 0}
                        </Text>
                    </View>
                </View>
                <View style={styles.rightContent}>
                    <Medicine size={width * 0.28} />
                </View>
            </View>

            <View style={styles.avenueRow}>
                <View style={[styles.avenueBlock, {backgroundColor: '#ccfdef'}]}>
                    <View style={styles.row}>
                        <Coin />
                    </View>
                    <Text style={styles.avenueLabel}>{t('today_revenue')}</Text>
                    <Text style={styles.value}>
                        {revenue?.today_revenue ? 
                                    revenue?.today_revenue < 1000000 ?
                                        formatTranMoney(revenue.today_revenue) : 
                                            `${(revenue.today_revenue/1000000).toFixed(3)} M` : 0}
                    </Text>
                </View>
                <View style={[styles.avenueBlock, {backgroundColor: '#f7eedc'}]}>
                    <View style={styles.row}>
                            <Dollar />
                        </View>
                        <Text style={styles.avenueLabel}>{t('total_revenue')}</Text>
                        <View style={styles.amtRow}>
                            <Text style={styles.value}>
                                {revenue?.total_revenue ? 
                                    revenue?.total_revenue < 1000000 ?
                                        formatTranMoney(revenue.total_revenue) : 
                                            `${(revenue.total_revenue/1000000).toFixed(3)} M` : 0}
                            </Text>
                            {/* <Text style={styles.avenueUnit}>Lakhs</Text> */}
                        </View>
                </View>
            </View>

            {transactions?.length > 0 && <Text style={[styles.transactionLabel, {color:colors.mainTextColor}]}>
                {t('latest_transactions')}</Text>}

            {transactions?.map((transaction) => {
                return <TransactionItem 
                    color='#fff'
                    key={transaction.id} 
                    item={transaction}
                    itemHandler={()=> navigation.navigate('TransactionDetail', {item: transaction})} />
            })}

        </View>

        <View style={styles.shadowContainer}>
            <View style={styles.footer}>
                <TouchableOpacity 
                    activeOpacity={1}
                    style={styles.addBtnOuter}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => navigation.navigate('AddTransaction')}
                        style={styles.addBtn}>
                        <Plus />
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        </View>
    </View>
}


export default Dashboard;