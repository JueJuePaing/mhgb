import { Dimensions, StyleSheet } from "react-native";
import { getStatusBarHeight } from 'react-native-status-bar-height';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#fff',
        // paddingTop: getStatusBarHeight() + 10,
       // marginBottom : 50
    },
    content: {
      paddingHorizontal : width * 0.04
    },
    headerRow : {
      flexDirection : 'row',
      alignItems : 'center',
      justifyContent : 'space-between'
  },
  title: {
    fontSize: 19,
    fontFamily: 'Lexend',
    color : '#000',
    paddingTop : height * 0.032,
    paddingBottom: 5,
    fontWeight: 'bold'
  },



    shadowContainer: {
      width: '100%',
      position: 'absolute',
      bottom: 0,
      backgroundColor: 'transparent'
  },
  footer: {
      height : 50,
      backgroundColor: '#fff',
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,

      shadowColor: '#000',
      shadowOffset: {
        width: 5,
        height: 5,
      },
      shadowOpacity: 1,
      shadowRadius: 6,
      elevation: 8
  },
  addBtnOuter: {
      alignSelf: 'center',
      marginTop: -30,
      width: 62,
      height: 62,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#F7FBFF',
      borderRadius: 40,
  },
  addBtn: {
      backgroundColor: '#323790',
      width: 56,
      height: 56,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 40,

      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.5,
      shadowRadius: 4,
      elevation: 4
  },
  productsContainer: {
    width: '100%',
    backgroundColor: '#e1ddfc',
    marginVertical: 10,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 25,
    flexDirection: 'row'
  },  
  leftContent: {
    width: '60%',
  },
  rightContent: {
    width: '40%'
  },
  label: {
    fontSize: 15,
    color : '#000',
    marginBottom: 10
},
  amtRow: {
    flexDirection: 'row',
    alignItems: 'baseline'
  },
  amount: {
    fontSize: 30,
    fontFamily: 'Lexend',
    color : '#000',
    padding: 0
  },
  unit: {
    fontSize: 20,
    color : '#000',
    paddingLeft: 10
  },
  avenueRow: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  avenueBlock: {
    backgroundColor: '#e1ddfc',
    borderRadius: 10,
    paddingHorizontal: 17,
    paddingVertical: 20,
    width: '48%'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 14,
  },
  avenueLabel: {
    fontSize: 13,
    color : '#000',
    paddingBottom: 4
},
  value: {
    fontSize: 21,
    fontFamily: 'Lexend',
    color : '#000',
  },
  avenueUnit: {
    fontSize: 15,
    color : '#000',
    paddingLeft: 5,
    fontFamily: 'Lexend',
  },
  transactionLabel: {
    fontFamily:'Lexend-Medium',
    fontSize:14.5,
    marginTop:25,
    marginBottom: 7
  }
});

export default styles;