import React, { useState,useEffect } from 'react';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity, Modal, TextInput } from 'react-native';
import { useTheme } from '@react-navigation/native';
import GapItem from '../Components/GapItem';
import { showAlert } from 'react-native-customisable-alert';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import LoadingView from '../Components/LoadingView';
import { connect } from 'react-redux';
import { setNotiCount } from '../../stores/actions';
import {useTranslation} from "react-i18next";

const device = Dimensions.get('window');

const Notification = (props) => {
    const [t] = useTranslation('common');
    const { colors } = useTheme();
    const [notification,setNotification] = useState([]);
    const [formErrors, setFormErrors] = useState('');
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [initialLoading,setInitialLoading] = useState(false);
    const [viewLoading,setViewLoading] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [reason, setReason] = useState('');
    
    const [selectedNotiID,setSelectedNotiId] = useState('');

    useEffect(() => {
        const unsubscribe = props.navigation.addListener ('focus', async () =>{
            setInitialLoading(true);
            setPage(1);
            setNotification([]);

            getNotification();
            readAllNotification();
        })
        return unsubscribe;
    }, [])

    const readAllNotification = async () => {
        try {
            const response = await ApiService(url.read_all_notification, [], 'POST');
            if (response.code == '200') {
                props.setNotiCount(0);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    useEffect(() => {
        if(page > 1){
            getNotification()
        }
    }, [page]);

    const handleLoadMore = () => {
        if(loadMore){
            setPage(page + 1)
        }
    }

    const getNotification = async () => {
        setViewLoading(true);
        let formdata = new FormData();
        formdata.append('page',page);
        formdata.append('per_page',16);

        try {
            const response = await ApiService(url.notification, formdata, 'POST');
            if (response.code == '200') {
                if(response.next_pages != ''){
                    setNotification(notification.concat(response.notifications));
                    setLoadMore(true);
                }else{
                    setNotification(notification.concat(response.notifications));
                    setLoadMore(false);
                }
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
            setInitialLoading(false);
        }
    }

    const confirmRequestSuvery = async(notificationId) => {
        setModalVisible(true);
        setSelectedNotiId(notificationId);
    }

    const requestSuvery = async (notificationId,answer) => {
        setInitialLoading(true);
        let formdata = new FormData();
        formdata.append('notification_id',notificationId);
        formdata.append('answer',answer);
        formdata.append('reason',reason);

        try {
            const response = await ApiService(url.noti_survey, formdata, 'POST');
            if (response.code == '200') {
                const updateNoti = notification.map(noti => {
                    if (noti.id === notificationId) {
                      return { ...noti, type: 'Notification' };
                    }
                    return noti;
                });
                setNotification(updateNoti);
                setSelectedNotiId('');
                setReason('');
                setModalVisible(false);
            }
            else if (response.code == '422') {
                showAlert({
                    title: `Oops!`,
                    message: response.message,
                    alertType: 'error',
                    dismissable: true,
                });
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setInitialLoading(false);
        }
    }

    const renderItem = ({ item }) => (
        <View>
          <Text style={{ fontFamily: 'Pyidaungsu', fontSize: 13.5, color: colors.secondaryTextColor }}>
            {item.description}
          </Text>
          <Text style={{ fontFamily: 'Lexend-Light', color: 'gray', fontSize: 12 }}>{item.date_time}</Text>

            {
                item.type == 'Survey' && (
                    <View style={{width:device.width/2.5,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>         
                        <TouchableOpacity onPress={() => requestSuvery(item.id,'Good') } style={[styles.button,{backgroundColor:'#2A318B'}]}>
                            <Text style={[styles.buttonText,{color:'#fff'}]}>Yes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => confirmRequestSuvery(item.id) } style={[styles.button,{backgroundColor:'#F5F5F5'}]}>
                            <Text style={[styles.buttonText,{color:'#2A318B'}]}>No</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
          
          <GapItem />
          
        </View> 
    );

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { initialLoading && <LoadingView/> }   
            <NavigationHeader hideBackButton={true} title={t('notification')} action={() => props.navigation.goBack()} />
            <View style={styles.container}> 
                
                {
                    notification && notification.length > 0 ? <FlatList
                    data={notification}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                    contentContainerStyle={{ paddingBottom: 20 }}
                    ListFooterComponent={viewLoading && <LoadingView/>}
                    onEndReached={handleLoadMore}
                    onEndReachedThreshold={1}
                /> : !initialLoading ? <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                {/* <Text style={{fontFamily:'Lexend-Bold',fontSize:16}}>
                    Whoops!
                </Text> */}
                <Text style={{
                    marginTop: 10,
                    fontFamily: 'Pyidaungsu',
                    fontSize : 14.5
                }}>
                    {t('no_notification')}
                </Text>
            </View> : null}
            

                    <Modal
                        visible={modalVisible}
                        onRequestClose={() => setModalVisible(false)}
                        animationType="slide"
                        transparent={true}
                    >
                    <View style={styles.modalBackdrop}>
                    <View style={styles.modalContent}>
                        <View style={styles.modalHeader}>
                            <Text style={styles.modalHeaderText}>
                                {t('feedback_requests')}
                            </Text>
                        </View>
                        <View style={styles.modalBody}>
                        <TextInput
                            keyboardType='default'
                            maxLength={255}
                            placeholder=""
                            value={reason}
                            onChangeText={setReason}
                            multiline={true}
                            numberOfLines={4}
                            style={styles.input}
                        />
                        </View>
                        <View style={styles.modalFooter}>
                        <TouchableOpacity 
                            onPress={() => requestSuvery(selectedNotiID,'Bad') } 
                            style={styles.button}
                        >
                            <Text style={styles.buttonText}>{t('submit')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setModalVisible(false) } style={{
                            padding: 10,
                            borderRadius: 5,
                            marginLeft: 10,
                        }}>
                            <Text style={styles.buttonText}>{t('close')}</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                    </View>
                </Modal>
            </View>
        </View>
    );
}

const mapstateToProps = state => {
    return {

    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setNotiCount: noti_count => {
          dispatch(setNotiCount(noti_count))
      },
    };
  };

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    button: {
        backgroundColor: '#F7EE25',
        padding: 10,
        borderRadius: 5,
        marginLeft: 10,
    },
    buttonText: {
        fontSize: 16,
        fontFamily: 'Lexend',
        paddingHorizontal: 10
    },
    modalBackdrop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 20,
    },
    modalHeader: {
        borderBottomWidth: 0.2,
        borderColor: '#e0e0e0',
        paddingBottom: 15,
        marginBottom: 15,
    },
    modalHeaderText: {
        fontSize: 16,
        fontFamily: 'Lexend',
    },
    modalBody: {
        marginBottom: 10,
        marginTop: 10,
    },
    input: {
        height: 80,
        borderColor: '#e0e0e0',
        borderWidth: 1,
        paddingHorizontal: 10,
        borderRadius: 5,
        fontFamily:'Pyidaungsu'
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderColor: '#e0e0e0',
        marginTop: 10
    },
})

export default connect(mapstateToProps,mapDispatchToProps)((Notification));
