import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity,TextInput,ScrollView, Modal } from 'react-native';
import {useTranslation} from "react-i18next";
import { setToken } from '../../stores/actions';
import { connect } from 'react-redux';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import LoadingView from '../Components/LoadingView';
import Loading from '../Components/Loading';
import { closeAlert, showAlert } from 'react-native-customisable-alert';
import ErrorMessage from '../Components/ErrorMessage';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';

const device = Dimensions.get('window');
const isIos = Platform.OS == 'ios';

const StockList = (props) => {
    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [loading,setLoading] = useState(false);
    const [loadingView,setLoadingView] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const { orderId } = props.route.params;
    const [ products, setProduct] = useState([]);
    const [ modalVisible, setModalVisible] = useState(false);
    const [ seletedProductId, setSeletedProductId] = useState(null);
    const [ selectedQuantity, setSelectedQuantity] = useState(0);
    const [ selectedPercentage, setSelectedPercentage] = useState(0);

    const [ percentModalVisible, setPercentModalVisible] = useState(false);

    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
    const [confirmPercentBtnDisble,setConfirmPercentBtnDisble] = useState(true);

    useEffect(() => {
        stockList();
    }, [])

    const closeModal = () => {
      setFormErrors({});
      setModalVisible(false);
    }

    const closePercentModal = () => {
      setFormErrors({});
      setPercentModalVisible(false);
    }

    useEffect(() => {
      setConfirmBtnDisble(!(selectedQuantity));
    }, [selectedQuantity]); 

    useEffect(() => {
      setConfirmPercentBtnDisble(!(selectedPercentage));
    }, [selectedPercentage]); 

    const renderProductItem = (product) => {
        return (
          <TouchableOpacity style={styles.productItem} onPress={() => handleClick(product)}>
            <Text style={styles.productName}>{product.name}</Text>
            <Text style={styles.productQuantity}>{product.quantity}</Text>
            <Text style={styles.productPrice}>{product.price}</Text>
          </TouchableOpacity>
        );
      };
    
    const handleClick = (product) => {
      setModalVisible(true);
      setSelectedQuantity(product.price);
      setSeletedProductId(product.id);
    };

    const updatePrice = () => {
      products.forEach((product) => {
        if (product.id == seletedProductId) {
          product.price = selectedQuantity;
        }
      });

      setModalVisible(false);
      setProduct(products);
    }

    const stockList = async () => {

        setLoadingView(true);
        let formdata = new FormData();
        formdata.append('order_id',orderId);
        formdata.append('per_page',200);

        try {
            const response = await ApiService(url.stock_list, formdata, 'POST');

            if (response.code == '200') {
                setProduct(response.products);
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoadingView(false); 
        }
    }

    const handleQuantityChange = (inputValue) => {
      if (!isNaN(inputValue)) {
        const parsedValue = parseFloat(inputValue);
        if (parsedValue >= 0) {
          setSelectedQuantity(parsedValue);
          setFormErrors({ quantity: "" });
        } else {
          setSelectedQuantity("");
          setFormErrors({ quantity: "Please enter a valid quantity" });
        }
      } else {
        setSelectedQuantity("");
        setFormErrors({ quantity: "Please enter a valid quantity" });
      }
    };
    

    const handlePercentageChange = (inputValue) => {
      if (!isNaN(inputValue)) {
        const parsedValue = parseFloat(inputValue);
        if (parsedValue >= 0 && parsedValue <= 100) {
          setSelectedPercentage(parsedValue);
          setFormErrors({ quantity: "" });
        } else {
          setSelectedPercentage("");
          setFormErrors({ quantity: "Please enter a valid percentage (0-100)" });
        }
      } else {
        setSelectedPercentage("");
        setFormErrors({ quantity: "Please enter a valid percentage" });
      }
    };
    
    const updatePercentage = () => {
      const updatedData = products.map((item) => {
        const updatedPrice = item.price * (1 + parseInt(selectedPercentage, 10) / 100);
        return {
          ...item,
          price: Math.round(updatedPrice),
        };
      });
      setPercentModalVisible(false);
      setProduct(updatedData);
    };
    
    const requestStockAdd = async () => {

      setLoading(true); 
      setFormErrors({});
  
      let formdata = new FormData();

      products.forEach((product, index) => {
        formdata.append(`products[${index}][product_id]`, product.id);
        formdata.append(`products[${index}][quantity]`, product.quantity);
        formdata.append(`products[${index}][sale_price]`, product.price);
        formdata.append(`products[${index}][purchased_price]`, product.original_price);
      });

      try {
        const response = await ApiService(url.add_stock, formdata, 'POST');

        if (response.code == '200') {
          props.navigation.replace('OK',{
            'image' : require('../../assets/images/return_confirm.png'),
            'title' : 'အဆင်ပြေသည်',
            'btnTitle' : 'မူလသို့ ပြန်သွားရန်',
            'description' : 'စာရင်းစာမျက်နှာအသစ်ထည့်သွင်းမှုအောင်မြင်သည်။ ',
            'isBack' : true
          })
        }else if (response.code == '422') {
          setFormErrors(response.errors);
        }else if (response.code == '401' || response.code == '400') {
          showAlert({
            title: `Oops!`,
            message: response.message,
            alertType: 'error',
            dismissable: true,
          });
        }
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false); 
      }
  }
  return (
   <View 
        style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loadingView && <LoadingView/> }
        { loading && <Loading/> }
        <View style={{marginTop:10}}/>
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
          <ScrollView 
            keyboardShouldPersistTaps='handled'
            style={styles.container}> 
              <HeaderLabel title={t('new_listing')} />
              <View style={{marginTop:30}}/>
              <View style={styles.header}>
                    <Text style={styles.columnHeader1}>Name</Text>
                    <Text style={styles.columnHeader2}>Qty</Text>
                    <Text style={styles.columnHeader3}>Sale Price</Text>
                </View>
                {products.map((product) => (
                    <React.Fragment key={product.id}>
                    {renderProductItem(product)}
                    </React.Fragment>
                ))}
                 <Modal
                    visible={modalVisible}
                    onRequestClose={() => setModalVisible(false)}
                    animationType="slide"
                    transparent={true}>
                      <View style={styles.modalBackdrop}>
                      <View style={styles.modalContent}>
                          <View style={styles.modalHeader}>
                            <Text style={styles.modalHeaderText}>Enter Sale Price</Text>
                          </View>
                          <View style={styles.modalBody}>
                          <TextInput
                            keyboardType="numeric"
                              placeholder="Enter quantity"
                              value={selectedQuantity.toString()}
                              onChangeText={handleQuantityChange}
                              style={styles.input}
                          />
                          {formErrors.quantity && (
                              <ErrorMessage message={formErrors.quantity} />
                          )}
                          </View>
                          <View style={styles.modalFooter}>
                          <TouchableOpacity 
                              disabled={confirmBtnDisble}
                              onPress={() => updatePrice() } 
                              style={[styles.button, { opacity: confirmBtnDisble ? 0.7 : 1 }]}>                            
                              <Text style={styles.buttonText}>Edit</Text>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => closeModal() } style={{
                              padding: 10,
                              borderRadius: 5,
                              marginLeft: 10,
                          }}>
                              <Text style={styles.buttonText}>Close</Text>
                          </TouchableOpacity>
                          </View>
                      </View>
                      </View>
                </Modal>
          </ScrollView>

          <Modal
              visible={percentModalVisible}
              onRequestClose={() => setPercentModalVisible(false)}
              animationType="slide"
              transparent={true}>
                <View style={styles.modalBackdrop}>
                <View style={styles.modalContent}>
                    <View style={styles.modalHeader}>
                      <Text style={styles.modalHeaderText}>Enter Increase %</Text>
                    </View>
                    <View style={styles.modalBody}>
                    <TextInput
                      keyboardType="numeric"
                        placeholder="Enter percentage"
                        value={selectedPercentage.toString()}
                        onChangeText={(value) => handlePercentageChange(value)}
                        style={styles.input}
                    />
                    {formErrors.quantity && (
                        <ErrorMessage message={formErrors.quantity} />
                    )}
                    </View>
                    <View style={styles.modalFooter}>
                    <TouchableOpacity 
                        disabled={confirmPercentBtnDisble}
                        onPress={() => updatePercentage() } 
                        style={[styles.button, { opacity: confirmPercentBtnDisble ? 0.7 : 1 }]}>                            
                        <Text style={styles.buttonText}>Increase</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => closePercentModal() } style={{
                        padding: 10,
                        borderRadius: 5,
                        marginLeft: 10,
                    }}>
                        <Text style={styles.buttonText}>Close</Text>
                    </TouchableOpacity>
                    </View>
                </View>
                </View>
          </Modal>
          <View style={{
                flexDirection:'row',
                marginTop:15,marginBottom:isIos ? 15 : 0,
                justifyContent:'space-between',
                width:device.width - 40
            }}>
            <CButton title={'Increase %'} 
                    color={colors.mainLinkColor}
                    fontFamily={'Lexend-Medium'}
                    width={(device.width - 32)/2}
                    height={50}
                    backgroundColor={'#D3D3D3'}
                    action={() =>  setPercentModalVisible(true) }
            />
            <CButton title={'Add Stock'} 
                    color={colors.mainLinkColor}
                    fontFamily={'Lexend-Medium'}
                    width={(device.width - 32)/2}
                    height={50}
                    backgroundColor={'#F7EE25'}
                    action={() =>  requestStockAdd() }
            />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    modalBackdrop: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 5,
        padding: 20,
    },
    modalHeader: {
        borderBottomWidth: 0.2,
        borderColor: '#e0e0e0',
        paddingBottom: 15,
        marginBottom: 15,
    },
    modalFooter: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      borderColor: '#e0e0e0',
      marginTop: 10
    },
    modalHeaderText: {
        fontSize: 16,
        fontFamily: 'Lexend',
    },
    modalBody: {
        marginBottom: 10,
        marginTop: 10,
    },
    input: {
      height: 40,
      borderColor: '#e0e0e0',
      borderWidth: 1,
      paddingHorizontal: 10,
      borderRadius: 5,
      fontFamily:'Lexend'
  },
    header: {
        backgroundColor: '#F6F7F9',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 9,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        padding: 20,
        marginBottom: 10,
    },
    columnHeader1: {
        color: '#2A318B',
        fontSize: 16,
        fontFamily: 'Lexend-Medium',
        flex: 1,
        width: '33%',
        flexWrap: 'wrap',
    },
    columnHeader2: {
        color: '#2A318B',
        fontSize: 16,
        fontFamily: 'Lexend-Medium',
        flex: 1,
        width: '33%',
        flexWrap: 'wrap',
    },
    columnHeader3: {
      color: '#2A318B',
      fontSize: 16,
      fontFamily: 'Lexend-Medium',
      flex: 1,
      width: '33%',
      flexWrap: 'wrap',
  },
    productItem: {
        backgroundColor: '#F6F7F9',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 9,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
        padding: 20,
        marginBottom: 10,
    },
    productName: {
        flex: 1,
        color: '#000',
        fontSize: 14,
        fontFamily: 'Lexend',
        width: '33%',
        flexWrap: 'wrap',
    },
    productQuantity: {
        flex: 1,
        color: '#000',
        fontSize: 14,
        fontFamily: 'Lexend',
        width: '33%',
        flexWrap: 'wrap',
    },
    productPrice: {
        flex: 1,
        color: '#000',
        fontSize: 14,
        fontFamily: 'Lexend',
        width: '33%',
        flexWrap: 'wrap',
    },
    button: {
      backgroundColor: '#F7EE25',
      padding: 10,
      borderRadius: 5,
      marginLeft: 10,
    },
    buttonText: {
      color: '#2A318B',
      fontSize: 16,
      fontFamily: 'Lexend',
    },
  })

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((StockList));
