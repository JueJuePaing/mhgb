import React from 'react';
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image} from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import CButton from '../Components/CButton';
import { setToken } from '../../stores/actions';

const device = Dimensions.get('window');

const Success = (props) => {
    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const { image,title, btnTitle, description, token, login } = props.route.params;

    const goHome = () => {
        props.setToken(token);
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            <View style={{marginTop:10}}/>
            <NavigationHeader  action={() => props.navigation.goBack()} />
            <View style={styles.container}>
                <View style={styles.body}>
                    <Image source={image} style={styles.image}/>
                    <Text style={[styles.textHeader,{color:colors.mainTextColor}]}>{title}</Text>
                    <Text style={[styles.textBody]}>{description}</Text>
                </View>
                <View style={styles.footer}>
                    <CButton 
                        title={btnTitle} 
                        color={colors.mainLinkColor}
                        fontFamily={'Pyidaungsu-Bold'}
                        width={device.width - 32}
                        height={50}
                        backgroundColor={'#F7EE25'}
                        action={() => {
                            if (login) {
                                props.navigation.goBack();
                            } else {
                                goHome()
                            }
                        } }
                    />
                </View>
            </View>
        </View>
      );
}

const styles = StyleSheet.create({
    image: {
        width: device.width,
        height: 250,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    textHeader: {
        fontSize: 17,
        paddingTop: 40,
        textAlign:'center',
        fontFamily: 'Pyidaungsu',
        lineHeight: 32
    },
    textBody: {
        color: "gray",
        fontSize: 14.5,
        textAlign:'center',
        paddingTop: 16,
        fontFamily: 'Pyidaungsu',
        lineHeight: 24
    },
    container: {
        width:device.width - 32,
        flexDirection: 'column', 
        flexGrow: 1,     
        justifyContent: 'space-between'
    },
    body: {
        paddingTop: 30
    },
    footer: {
        marginTop: 15,
        marginBottom: 35,
        alignSelf: 'center'
    },
})

const mapstateToProps = state => {
    return {
        token:state.token,
    };
};
  
  const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        },
    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((Success));
