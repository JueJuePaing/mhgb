import React, { useState,useEffect } from 'react';
import NavigationHeader from '../Components/NavigationHeader';
import { Alert, Text, View, Dimensions,Image, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import GapItem from '../Components/GapItem';
import {
    AntDesign,
    MaterialCommunityIcons,
    FontAwesome,
    MaterialIcons,
    Octicons
} from '@expo/vector-icons';

const device = Dimensions.get('window');

import {
    NetPrinter,
    ColumnAlignment,
    COMMANDS,
    NetPrinterEventEmitter,
    RN_THERMAL_RECEIPT_PRINTER_EVENTS,
} from 'react-native-thermal-receipt-printer-image-qr';

const Setting = (props) => {
    const { colors } = useTheme();
    const [t] = useTranslation('common');

    React.useEffect(() => {

        const initPermissions = () => {
            NetPrinter.init().then(() => {
                console.log("initialized...")
            });
          };
          initPermissions();
      }, []);

      React.useEffect(() => {
     
        NetPrinterEventEmitter.addListener(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_SUCCESS,
          (printers) => {
            console.log("printers >> ", {printers});
            if (printers && printers.length > 0) {
              connectPrinter(printers[0].host);
              // setLoading(false);
              // setDevices(printers);
            }
          },
        );
        (async () => {
          const results = await NetPrinter.getDeviceList();
        })();

      return () => {
        NetPrinterEventEmitter.removeAllListeners(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_SUCCESS,
        );
        NetPrinterEventEmitter.removeAllListeners(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_ERROR,
        );
      };
    }, []);

    const handlePrintBillWithImage = async () => {
        NetPrinter.printImage(
          'https://media-cdn.tripadvisor.com/media/photo-m/1280/1b/3a/bd/b5/the-food-bill.jpg',
          {
            imageWidth: 610
            // imageHeight: 1000,
            // paddingX: 100
          },
        );
        NetPrinter.printBill('', {beep: false});
      };

      
    const connectPrinter = async (host) => {
      try {
          // if (connected) {
        // await NetPrinter.closeConn();
          // setConnected(!connected);
          // }

          const status = await NetPrinter.connectPrinter(
            host || '192.168.1.5',
            9100,
          );
  
          console.log('connect -> status', status);

          // printText();
          handlePrintBillWithImage()

          Alert.alert(
            'Connect successfully!',
            `Connected to ${status.host ?? 'Printers'} !`,
          );
          // setConnected(true);
        } catch (err) {
          Alert.alert('Connect failed!', `${err} !`);
        }
    }

    const BOLD_ON = COMMANDS.TEXT_FORMAT.TXT_BOLD_ON;
    const BOLD_OFF = COMMANDS.TEXT_FORMAT.TXT_BOLD_OFF;
    let orderList = [
      ["1. Skirt Palas Labuh Muslimah Fashion", "x2", "500$"],
      ["2. BLOUSE ROPOL VIRAL MUSLIMAH FASHION", "x4222", "500$"],
      [
        "3. Women Crew Neck Button Down Ruffle Collar Loose Blouse",
        "x1",
        "30000000000000$",
      ],
      ["4. Retro Buttons Up Full Sleeve Loose", "x10", "200$"],
      ["5. Retro Buttons Up", "x10", "200$"],
    ];
    let columnAlignment = [
      ColumnAlignment.LEFT,
      ColumnAlignment.CENTER,
      ColumnAlignment.RIGHT,
    ];
    let columnWidth = [46 - (7 + 12), 7, 12];
    const header = ["Product list", "Qty", "Price"];

    const printText = async () => {
      try {
      
        NetPrinter.printColumnsText(header, columnWidth, columnAlignment, [
          `${BOLD_ON}`,
          "",
          "",
        ]);
        for (let i in orderList) {
          NetPrinter.printColumnsText(orderList[i], columnWidth, columnAlignment, [
            `${BOLD_OFF}`,
            "",
            "",
          ]);
        }
        NetPrinter.printText('\n\n')
        NetPrinter.printBill(`<C>Thank you\n`);
  
  
        // Alert.alert('Printed', `Text printed to ${device.device_name || 'Unknown Device'}`);
      } catch (error) {
        Alert.alert('Error', 'Unable to print the text');
        console.log('Error printing text:', error);
      }
    };
      
  return (
   <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        <NavigationHeader 
                    hideBackButton={true}
                    title={t('Setting')}  
                    action={() => props.navigation.goBack()} />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
            <View style={{width:device.width-50}}>
                <View style={{flexDirection:'row',marginTop:10}}>
                        <View>
                        {
                            props.profile && props.profile.logo ? 
                            <Image
                                resizeMode='contain'
                                source={{ uri: props.profile.logo }}
                                style={{width:70,height:70,resizeMode:'cover',borderRadius:35,borderColor:'#fff',borderWidth:2}}/>
                            :
                            <Image 
                                resizeMode='contain'
                                source={require('../../assets/images/default_user.png')}
                                style={{width:70,height:70,resizeMode:'cover',borderRadius:35,borderColor:'#fff',borderWidth:2}}/>
                        }
                        </View>
                        <View style={{justifyContent:'center',marginLeft:15}}>
                            <Text style={{fontFamily:'Pyidaungsu',fontSize:16,color:colors.mainTextColor,width:device.width-150}}>
                                {props.profile?.name}
                            </Text>
                            <Text style={{fontFamily:'Pyidaungsu',fontSize:14,color:'gray',paddingTop:5}}>
                                {t('app_welcome')}
                            </Text>
                        </View>
                </View>
                <View style={{marginTop:30}}/>

                

                <TouchableOpacity onPress={() => console.log("")
                     ///i18n.changeLanguage('en')
                     }>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View style={{flexDirection:'row', alignItems: 'center'}}>
                        {/* <Image source={require('../../assets/images/acc_info.png')} 
                            style={{width:26,height:26,resizeMode:'contain'}}/> */}
                             <FontAwesome
                                name='language'
                                size={18}
                                color='#424245'
                                style={{width : 23}} />
                        <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                            {t('language')}
                        </Text>
                        </View>
                        <Image source={require('../../assets/images/right-arrow.png')} 
                            style={{width:20,height:20}}
                            tintColor={colors.mainTextColor}/>
                    </View>
                    <GapItem/>
                </TouchableOpacity>

               
            </View>

        </ScrollView>
    </View>
  );
}

export default Setting;