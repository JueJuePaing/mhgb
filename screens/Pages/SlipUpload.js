import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions,Image, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import CButton from '../Components/CButton';
import * as ImagePicker from 'expo-image-picker';
import ErrorMessage from '../Components/ErrorMessage';

import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import mime from 'mime';
import { showAlert } from 'react-native-customisable-alert';
import ImageLoad from 'react-native-image-placeholder';
import { connect } from 'react-redux';
import { setCartCount } from '../../stores/actions';

const device = Dimensions.get('window');

const SlipUpload = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [images, setImages] = useState([]);
    const [imageType, setImageType] = useState(null);
    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(false);

    const { defaultAddress, defaultPayment, paymentType } = props.route.params;

    useEffect(() => {
        if (paymentType === "cash_down") {
            if (images.length === 0) {
                setConfirmBtnDisble(true);
            } else {
                setConfirmBtnDisble(false);
            }
        } else {
            setConfirmBtnDisble(false);
        }
    }, [images, paymentType]); 

    const handleImagePicker = async () => {
        setFormErrors({});
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          console.log('Permission denied');
          return;
        }
    
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: false,
          quality: 1,
          selectionLimit : 5,
          allowsMultipleSelection : true
        });

    
        if (!result.canceled) {
            if (result.assets && result.assets.length > 0) {
                if (images.length + result.assets.length > 5) {
                    showAlert({
                        title: `Oops!`,
                        message: t('invalid_screenshot_count'),
                        alertType: 'error',
                        dismissable: true,
                    });
                } else {
                    const newImgs = result.assets.map(item => item.uri);
                    setImages([...images, ...newImgs]);
                }
            }
        }
    };

    const generateRandomString = () => {
        return Math.random().toString(36).substring(2, 8);
    }

    const requestOrder = async () => {
        setLoading(true); 
        let retry = true; 
        setFormErrors({});

        let formdata = new FormData();

        formdata.append('shipping_address', defaultAddress.id);
        formdata.append('payment_type_status', paymentType);
        if (paymentType === "cash_down") {
            formdata.append('payment_type', defaultPayment.id);
            formdata.append('multiple_screenshot[0]', {type: mime.getType(images[0]), uri: images[0], name: `${generateRandomString()}.${mime.getType(images[0]).split("/")[1]}`});
            if (images.length > 1) {
                formdata.append('multiple_screenshot[1]', {type: mime.getType(images[1]), uri: images[1], name: `${generateRandomString()}.${mime.getType(images[1]).split("/")[1]}`});
            }
            if (images.length > 2) {
                formdata.append('multiple_screenshot[2]', {type: mime.getType(images[2]), uri: images[2], name: `${generateRandomString()}.${mime.getType(images[2]).split("/")[1]}`});
            }
            if (images.length > 3) {
                formdata.append('multiple_screenshot[3]', {type: mime.getType(images[3]), uri: images[3], name: `${generateRandomString()}.${mime.getType(images[3]).split("/")[1]}`});
            }
            if (images.length > 4) {
                formdata.append('multiple_screenshot[4]', {type: mime.getType(images[4]), uri: images[4], name: `${generateRandomString()}.${mime.getType(images[4]).split("/")[1]}`});
            }
        }

  
        try {
            const response = await ApiService(url.place_order, formdata, 'POST');

            if (response.code == '200') {
                props.setCartCount(0);
                props.navigation.replace('OK',{
                    'image' : require('../../assets/images/order_confirm.png'),
                    'title' : t('thank_you'),
                    'btnTitle' : t('back_to_home'),
                    'description' : t('order_successful'),
                })
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            if (retry) {
                retry = false; 
                await requestOrder();
            }else{
                showAlert({
                    title: `Oops!`,
                    message: 'Netwok request failed.Please try again!',
                    alertType: 'error',
                    dismissable: true,
                  });
            }
          } finally {
            setLoading(false); 
        }
    }

    return (
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
            { loading && <Loading/> }
            <NavigationHeader title={t('Checkout')}  action={() => props.navigation.goBack()} />
            <View style={{marginTop:10}}/>
            <ScrollView showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}>
                    <View style={styles.container}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontFamily:'Pyidaungsu-Bold',color:colors.mainTextColor,fontSize:14}}>{t('shipping_address')}</Text>
                        </View>
                        <View 
                            style={{flexDirection:'row',marginTop:10}}>
                                <View style={styles.addressBox}>
                                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                            <ImageLoad source={require('../../assets/images/location.png')}
                                            style={{width:24,height:24,resizeMode:'contain'}} />  
                                            <View style={{width:200}}>
                                                <Text style={{fontFamily:'Pyidaungsu',color:colors.mainTextColor,fontSize:14}}>
                                                    {defaultAddress.name}
                                                </Text>
                                                <Text style={{fontFamily:'Lexend-Light',color:'gray',marginTop:5,fontSize:14}}>
                                                    {defaultAddress.phone_number}
                                                </Text>
                                                <Text style={{fontFamily:'Pyidaungsu',color:'gray',marginTop:5,fontSize:14}}>
                                                    {defaultAddress.ship_address}
                                                </Text>
                                            </View>
                                            <View></View>
                                        </View>
                                </View>
                        </View>

                        {paymentType === "cash_down" && <>
                            <View style={{marginTop:25}}/>
                            <CButton title={t('upload_screenshot')} 
                                    type={'file'}
                                    color={colors.mainLinkColor}
                                    fontFamily={'Pyidaungsu-Bold'}
                                    width={device.width - 32}
                                    height={50}
                                    backgroundColor={'#FFF'}
                                    txtSize={14}
                                    action={() =>  handleImagePicker() }/>

                            {images?.length > 0 && <>
                                {images.map((img, index) => {
                                    return <Image 
                                        key={index} 
                                        source={{ uri: img }} 
                                        style={{ width: '100%', height: 200,marginTop:20,resizeMode:'cover'}} />
                                })}
                            </>}
                            {formErrors.screenshot && (
                                <ErrorMessage message={formErrors.screenshot[0]} />
                            )}   
                        </>}
                    </View>
            </ScrollView>
            <View style={{marginTop:20,marginBottom:20}}>
                <CButton title={t('continue_order')} 
                        color={colors.mainLinkColor}
                        fontFamily={'Lexend-Medium'}
                        width={device.width - 32}
                        height={50}
                        backgroundColor={'#F7EE25'}
                        disabled={confirmBtnDisble}
                        action={() =>  
                            requestOrder() 
                        }
                />
            </View>
        </View>
      );
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32,
    },
    title: {
        fontFamily: 'Pyidaungsu-Bold',
        fontSize:16,
    },
    addressBox: {
        marginTop:1,width:device.width-50,
        borderColor: 'rgba(9, 15, 71, 0.1)',
        borderWidth: 1,
        borderRadius: 6,    
        padding: 15
    }
})

const mapstateToProps = state => {
    return {
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
      setCartCount: cart_count => {
        dispatch(setCartCount(cart_count))
      },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((SlipUpload));
