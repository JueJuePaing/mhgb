import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity,TextInput,FlatList } from 'react-native';
import {useTranslation} from "react-i18next";
import { setToken } from '../../stores/actions';
import { connect } from 'react-redux';
import HeaderLabel from '../Components/HeaderLabel';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import LoadingView from '../Components/LoadingView';
import Loading from '../Components/Loading';
import { closeAlert, showAlert } from 'react-native-customisable-alert';

const device = Dimensions.get('window');

const RemainStockOut = (props) => {
    const [t] = useTranslation('common');
    const [viewLoading,setViewLoading] = useState(false);
    const [stocks,setStocks] = useState([]);
    const [loadMore, setLoadMore] = useState(false);
    const [page, setPage] = useState(1);
    const [loading,setLoading] = useState(false);
    const [editableStock, setEditableStock] = useState(null);
    const [isEditing, setIsEditing] = useState(false);
    const [editableStockValue, setEditableStockValue] = useState('');

    useEffect(() => {
      setLoading(true);
      getStocks();
    }, [])

    useEffect(() => {
      if(page > 1){
          getStocks()
      }
    }, [page]);

    const handleLoadMore = () => {
      if(loadMore){
          setPage(page + 1)
      }
    }

    const getStocks = async() => {

        setViewLoading(true);
        let formdata = new FormData();
        formdata.append('page',page);
        formdata.append('per_page',16);

        try {
            const response = await ApiService(url.stocks_out, formdata, 'POST');

            if (response.code == '200') {
              if(response.next_pages != ''){
                setStocks(stocks.concat(response.stocks));
                setLoadMore(true);
              }else{
                setStocks(stocks.concat(response.stocks));
                setLoadMore(false);
              }

            }else if (response.code == '422') {
    
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setViewLoading(false);
            setLoading(false);
        }
    }

    const handleSaveStock = async(id) => {

      setLoading(true);
      let formdata = new FormData();
      formdata.append('product_id',id);
      formdata.append('quantity',editableStockValue);

      try {
          const response = await ApiService(url.stock_update, formdata, 'POST');
          if (response.code == '200') {

            const updatedStocks = stocks.map(stock => {
              if (stock.id === id) {
                return { ...stock, quantity: parseInt(editableStockValue, 10) };
              }
              return stock;
            });
            setStocks(updatedStocks);
            setEditableStock(null);
            setIsEditing(false);
          }else if (response.code == '422')
           {
            showAlert({
              title: `Oops!`,
              message: 'Quantity field format is wrong.',
              alertType: 'error',
              dismissable: true,
            });
          }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
          }
      } catch (error) {
          console.error(error);
      } finally {
          setLoading(false);
      }
    }

    const deleteStock = (id) => {
      showAlert({
            title:"Remove Stock",
            message: "Are you sure want to remove?",
            alertType: 'warning',
            onPress: () => handleDeleteStock(id)
      });
  }

    const handleDeleteStock = async(id) => {
      closeAlert();
      setLoading(true);
      let formdata = new FormData();
      formdata.append('product_id',id);
      
      try {
          const response = await ApiService(url.stock_delete, formdata, 'POST');
          if (response.code == '200') {
            const updatedStocks = stocks.filter(stock => stock.id !== id);
            setStocks(updatedStocks);

          }else if (response.code == '422')
           {
          }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
          }
      } catch (error) {
          console.error(error);
      } finally {
          setLoading(false);
      }
    }
    const handleCancelEdit = () => {
      setEditableStock(null);
      setIsEditing(false);
    }

    const handleEditStock = (id,quantity) => {
      setEditableStock(id);
      setIsEditing(true);
      setEditableStockValue(quantity.toString());
    };

  return (
   <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        { loading && <Loading/> }
        <View style={{marginTop:10}}/>
        <NavigationHeader title={''}  action={() => props.navigation.goBack()} />
          <View style={styles.container}> 
              <HeaderLabel title={t('remain_stockout')} />
              <View style={{marginTop:30}}/>
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                data={stocks}
                keyExtractor={(item) => item.id.toString()}
                ListFooterComponent={viewLoading && <LoadingView/>}
                onEndReached={handleLoadMore}
                renderItem={({ item }) => (
                    <View key={item.id} style={styles.orderCard}>
                    <View style={{flexDirection:'row',justifyContent:'space-between', marginTop: 10 }}>
                        <Text style={{ fontFamily: 'Lexend-Medium', fontSize: 14, color: '#2A318B'}}>
                        Name - {item.name}
                        </Text>
                        <View style={styles.buttonContainer}>
                          <View style={{ marginHorizontal: 5 }} />
                          <TouchableOpacity 
                            onPress={() => deleteStock(item.id)}
                            style={[styles.button, { backgroundColor: '#EB5757' }]}>
                            <Icon name="trash" color="#fff" size={15} />
                          </TouchableOpacity>
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 15 }}>
                      {isEditing && editableStock === item.id ? (
                        <>
                        <TextInput
                          style={[styles.input]}
                          value={editableStockValue}
                          onChangeText={text => setEditableStockValue(text)}
                          keyboardType="numeric"
                        />
                        <TouchableOpacity onPress={() => handleSaveStock(item.id)}
                         style={[editableStockValue.length == 0 && styles.disabledButton,{ justifyContent: 'center'}]}
                         disabled={editableStockValue.length === 0}>
                          <Text style={styles.buttonText}>Save</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => handleCancelEdit()}
                        style={{justifyContent:'center'}}>
                          <Text style={styles.buttonText}>{t('cancel')}</Text>
                        </TouchableOpacity>
                        </>
                      ) : (
                        <Text style={{ fontFamily: 'Lexend-Light', fontSize: 14, color: 'rgba(0, 0, 0, 0.56)' }}>
                          Stock - {item.quantity}
                        </Text>
                      )
                      }
                      
                    </View>
                    </View>
                )}
                />
          </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        width:device.width - 32,
    },
    buttonText: {
      fontFamily: 'Lexend-Medium',
      fontSize: 14,
      color: '#1987FB',
      alignSelf:'center',
      marginLeft: 15
    },
    orderCard: {
      marginLeft: 1,
      marginRight: 1,
      padding: 12,
      marginBottom: 20,
      backgroundColor: '#F6F7F9',
      borderRadius: 9,
      shadowColor: '#000',
      shadowOffset: {
      width: 0,
      height: 1,
      },
      shadowOpacity: 0.23,
      shadowRadius: 1.62,
      elevation: 1,
    },
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    button: {
      width: 25,
      height: 25,
      borderRadius: 5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    disabledButton : {
      opacity: 0.5
    },
    input: {
      width: 100,
      height: 40,
      borderWidth: 1,
      borderColor: '#ccc',
      borderRadius: 5,
      paddingHorizontal: 10,
      fontFamily: 'Lexend-Light',
      fontSize: 14,
      color: '#000',
    },
  })

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((RemainStockOut));
