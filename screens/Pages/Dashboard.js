import React, { useState, useContext, useEffect,useRef } from 'react'
import { connect } from 'react-redux';
import { StyleSheet, Image, Text, View, Dimensions, DeviceEventEmitter, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import Categories from '../Components/Categories';
import {useTranslation} from "react-i18next";
import NetInfo from '@react-native-community/netinfo';

import BrandList from '../Components/BrandList';
import HorizontalProducts from '../Components/HorizontalProducts';
import Spacer from '../Components/Spacer';
import { AppContext } from '../../context/AppContextProvider';

import { ApiService } from '../../network/service';
import url from '../../network/url';
import DashboardHeader from '../Components/DashboardHeader';
import { showAlert } from 'react-native-customisable-alert';
import { setNotiCount, setProfile, setShopProducts, setTaxPercent } from '../../stores/actions';
import * as Notifications from 'expo-notifications';
import ImageLoad from 'react-native-image-placeholder';
import { Carousel, Pagination } from 'react-native-snap-carousel';
import {
    Placeholder,
    PlaceholderMedia,
    Fade,
  } from "rn-placeholder";
import {
    NetPrinter,
    NetPrinterEventEmitter,
    RN_THERMAL_RECEIPT_PRINTER_EVENTS,
    USBPrinter
} from 'react-native-thermal-receipt-printer-image-qr';

const device = Dimensions.get('window');

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: true,
    }),
});

const Dashboard = (props) => {

    const {
        changeNetPrinterConnected,
        changeUsbConnected,
        changeProfileData
      } = useContext(AppContext);

    const [t] = useTranslation('common');

    const { colors } = useTheme();
    const [categories,setCategories] = useState([])
    const [products, setProducts] = useState([])
    const [bestSellingProducts, setBestSellingProducts] = useState([])
    const [specialProducts, setSpecialProducts] = useState([])
    const [brands, setBrands] = useState([]);
    const [banners, setBanners] = useState([]);
    const [notification, setNotification] = useState(false);
    const [index, setIndex] = useState(0)
    const [placeHolderLoading, setPlaceHolderLoading] = useState(false);
    const [isConnection, SetIsConnection] = useState(true);

    const notificationListener = useRef();
    const responseListener = useRef();
    const isCarousel = React.useRef(null)

    useEffect(() => {
        const unsubscribe = NetInfo.addEventListener(state => {
            handleConnectivityChange(state.isInternetReachable)
        });
        return unsubscribe;
      }, [])
    
      const handleConnectivityChange = isConnection => {
        SetIsConnection(isConnection);
      };

    useEffect(() => {
        USBPrinter.init().then(()=> {
            //list printers
            USBPrinter.getDeviceList().then((printers) => {
                if (printers && printers.length > 0)
                    connectUSBPrinter(printers[0])
            });
          })
    }, []);

    const connectUSBPrinter = async (printer) => {
        const status = await USBPrinter.connectPrinter(
            printer.vendor_id, 
            printer.product_id
          );
        if (status?.product_id) {
            changeUsbConnected(true);
        }
    }

    useEffect(() => {
        const initPrinterFunc = async () => {
          NetPrinter.init().then(() => {
              console.log("initialized Net printer...")
          });
          const results = await NetPrinter.getDeviceList();
        };
    
        initPrinterFunc();
    
        // Set an interval to run your function every 1 minutes
        const intervalId = setInterval(initPrinterFunc, 1 * 60 * 1000); 
    
        // Clean up the interval when the component unmounts
        return () => clearInterval(intervalId);
      }, []); // The empty dependency array ensures this effect runs only once

    useEffect(() => {
     
        NetPrinterEventEmitter.addListener(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_SUCCESS,
          (printers) => {
            if (printers && printers.length > 0 && printers[0].host) {
              connectNetPrinter(printers[0].host);
            }
          },
        );
        (async () => {
            const results = await NetPrinter.getDeviceList();
          })();

      return () => {
        NetPrinterEventEmitter.removeAllListeners(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_SUCCESS,
        );
        NetPrinterEventEmitter.removeAllListeners(
          RN_THERMAL_RECEIPT_PRINTER_EVENTS.EVENT_NET_PRINTER_SCANNED_ERROR,
        );
      };
    }, []);

    const connectNetPrinter = async (host) => {
        try {

            const status = await NetPrinter.connectPrinter(
              host || '',
              9100,
            );
    
            if (status) {
                changeNetPrinterConnected(true);
            }

            // showAlert({
            //     title: t('success'),
            //     message: t('printer_state_connected'),
            //     alertType: 'success',
            //     dismissable: true
            //   });
  
          } catch (err) {
            console.error(err);
          }
    }


    useEffect(()=> {
        const eventEmitter = DeviceEventEmitter.addListener(
            'dashboard_products_change',
            val => {
                getAllShopProducts();
                getTax();
            },
          );
          return () => eventEmitter;
    }, [])

    useEffect(() => {
        // for offline invoices
        getAllShopProducts();
        getTax();
    }, [])

    const getTax = async () => {
        const response = await ApiService(url.tax, [] , 'POST');
            if (response?.code == '200' && response?.tax_percentage) {
                props.setTaxPercent(response.tax_percentage);
            }
    }

    const getAllShopProducts = async () => {
        try {
            const response = await ApiService(url.shop_products, [] , 'POST');
            if (response.code == '200') {
                props.setShopProducts(response.products);
            }
        } catch (error) {
            console.error(error);
        } finally {
           
        }
    }

    useEffect(() => {

        getProfile();
        getCategories()
        getBrands()
        getBanner()
        getProducts()
        getBestSellingProducts();
        getSpecialProducts();
        getNotiCount()

        notificationListener.current = Notifications.addNotificationReceivedListener((notification) => {
            getNotiCount();
            setNotification(notification);
        });
        responseListener.current = Notifications.addNotificationResponseReceivedListener((response) => {
        });
        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current);
            Notifications.removeNotificationSubscription(responseListener.current);
        };

    }, []); 

    const getProfile = async () => {
        setPlaceHolderLoading(true);
        try {
            const response = await ApiService(url.account_detail, [], 'POST');
            if (response.code == '200') {
                props.setProfile(response.detail);
                changeProfileData(response.detail);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {

        }
    }

    const getBanner = async () => {
        try {
            const response = await ApiService(url.banners, [], 'POST');
            if (response.code == '200') {
                setBanners(response.banners);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const getCategories = async () => {
        try {
            const response = await ApiService(url.categories, [], 'POST');
            if (response.code == '200') {
                setCategories(response.categories);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const getProducts = async () => {
        try {
            const response = await ApiService(url.deal_of_weeks, [], 'POST');
            if (response.code == '200') {
                setProducts(response.products);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setPlaceHolderLoading(false);
        }
    }

    const getBestSellingProducts = async () => {
        try {
            const response = await ApiService(url.best_selling, [], 'POST');

            if (response.code == '200') {
                setBestSellingProducts(response.products);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setPlaceHolderLoading(false);
        }
    }

    const getSpecialProducts = async () => {
        try {
            const response = await ApiService(url.specialProducts, [], 'POST');

            if (response.code == '200') {
                setSpecialProducts(response.products);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
            setPlaceHolderLoading(false);
        }
    }

    const getBrands = async () => {
        try {
            const response = await ApiService(url.brands, [], 'POST');
            if (response.code == '200') {
                setBrands(response.brands);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const getNotiCount = async() => {
        try {
            const response = await ApiService(url.noti_unread_count, [], 'POST');
            if (response.code == '200') {
                props.setNotiCount(response.count);
            }else if (response.code == '422') {
                setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
                showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
                });
            }
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    const renderItem = (item) => {
        return (
            <>
            {
                placeHolderLoading ? 
                <Placeholder Animation={Fade} Left={props => (
                    <View style={{ flexDirection: 'row' }}>
                          <PlaceholderMedia
                              isRound={true}
                              style={[{ height: 150, width: device.width-32 }, props.style]}
                          />
                      </View>
                  )}>
                  </Placeholder>

                  :
                  <ImageLoad 
                        isShowActivity={false}
                        source={{uri:item.item}} 
                        style={styles.image}
                        resizeMode={'contain'}
              />
            }
            </>
        )
    }

    return (
    <View style={{flex:1,backgroundColor:'#F7FBFF',alignItems:'center',paddingBottom:5}}>
        {isConnection ? <ScrollView 
            showsVerticalScrollIndicator={false}
            style={{ flex: 1 }}
            contentContainerStyle={{
                flexGrow: 1,
                paddingBottom:30
            }}>
            <DashboardHeader
                placeHolderLoading={placeHolderLoading}
                navigation={props.navigation}/>
            <View style={styles.container}>
                <View style={{width:device.width-32}}>
                    <Spacer marginTop={10} /> 
                    <View style={{alignItems:'center'}}>
                    <Carousel
                        ref={isCarousel}
                        data={banners}
                        renderItem={renderItem}
                        sliderWidth={device.width}
                        itemWidth={device.width - 32}
                        onSnapToItem={(index) => setIndex(index)}
                        layout="default"
                        loop
                        />
                    {
                        banners && banners.length < 2 &&
                        <View style={{ marginBottom:15 }}/>
                    }
                    {
                        banners &&
                        <Pagination
                        dotsLength={banners.length}
                        activeDotIndex={index}
                        carouselRef={isCarousel}
                        dotStyle={{
                            width: 6,
                            height: 6,
                            borderRadius: 3,
                            backgroundColor: 'rgba(0, 0, 0, 0.92)',
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                        tappableDots={true}
                        />
                    }
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={[styles.lable,{color:colors.mainTextColor}]}>
                        {t('top_categories')}
                    </Text>
                    <TouchableOpacity onPress={() => props.navigation.navigate('CategoryList', {back : true})}>
                        <Text style={{color:colors.inputActiveColor,fontFamily:'Lexend',fontSize:14}}>
                            {t('more')}
                        </Text>
                    </TouchableOpacity>
                    </View>
                    <Categories 
                        categories={categories} 
                        device={device} 
                        navigation={props.navigation}
                    />
                    <Spacer marginTop={20} /> 
                    
                    {/* {products && products.length > 0 && <>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={[styles.lable,{color:colors.mainTextColor}]}>
                                {t('deals_of_week')}
                            </Text>
                            <TouchableOpacity onPress={() => props.navigation.navigate('ProductList')}>
                                <Text style={{color:colors.inputActiveColor,fontFamily:'Lexend',fontSize:14}}>
                                    {t('more')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <HorizontalProducts 
                            placeHolderLoading={placeHolderLoading}
                            products={products} 
                            device={device} 
                            navigation={props.navigation}
                        />
                        <Spacer marginTop={20} /> 
                    </>} */}

                    {bestSellingProducts && bestSellingProducts.length > 0 && <>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={[styles.lable,{color:colors.mainTextColor}]}>
                                {t('best_selling_products')}
                            </Text>
                            <TouchableOpacity onPress={() => props.navigation.navigate('CategoryList')}>
                                <Text style={{color:colors.inputActiveColor,fontFamily:'Lexend',fontSize:14}}>
                                    {t('more')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <HorizontalProducts 
                            placeHolderLoading={placeHolderLoading}
                            products={bestSellingProducts} 
                            device={device} 
                            navigation={props.navigation}
                        />
                        <Spacer marginTop={20} /> 
                    </>}

                    {specialProducts && specialProducts.length > 0 && <>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={[styles.lable,{color:colors.mainTextColor}]}>
                                {t('special_products')}
                            </Text>
                            <TouchableOpacity onPress={() => props.navigation.navigate('CategoryList')}>
                                <Text style={{color:colors.inputActiveColor,fontFamily:'Lexend',fontSize:14}}>
                                    {t('more')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <HorizontalProducts 
                            placeHolderLoading={placeHolderLoading}
                            products={specialProducts} 
                            device={device} 
                            navigation={props.navigation}
                        />
                    </>}

                    {/* <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={[styles.lable,{color:colors.mainTextColor}]}>
                            {t('features_brands')}
                        </Text>
                        <TouchableOpacity onPress={() => props.navigation.navigate('BrandList')}>
                            <Text style={{color:colors.inputActiveColor,fontFamily:'Lexend',fontSize:14}}>
                                {t('more')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <BrandList 
                        placeHolderLoading={placeHolderLoading}
                        brands={brands} 
                        device={device} 
                        navigation={props.navigation}
                    />  */}
                </View>
            </View>
        </ScrollView> :  <View style={styles.offlineContainer}>
      <Image source={require('../../assets/images/no_internet.png')} 
            resizeMode={'contain'} 
            style={{width:125,height:125}}
          />
      <Text style={styles.offlineText}>{t('no_internet_connection')}</Text>
      <TouchableOpacity>
      <Text style={styles.tryOfflineText}>
        {t('try_again')}
      </Text>
      </TouchableOpacity>
    </View>}
    </View>
  );
}

const mapstateToProps = state => {
    return {
        
    }
};
  
const mapDispatchToProps = dispatch => {
  return {
      setNotiCount: noti_count => {
        dispatch(setNotiCount(noti_count))
    },
    setProfile: profile => {
        dispatch(setProfile(profile))
    },
    setShopProducts: products => {
        dispatch(setShopProducts(products))
      },
      setTaxPercent: tax => {
          dispatch(setTaxPercent(tax))
        },
  };
};
  
const styles = StyleSheet.create({
    container: {
      alignItems:'center',
      marginTop:5
    },
    lable : {
        fontFamily:'Lexend-Medium',
        fontSize:14,
        marginBottom:20,
        marginTop: 6
    },
    footer: {
      marginTop: 15,
      marginBottom: 15,
      alignSelf: 'center'
    },
    searchInput: {
        width:device.width,
        backgroundColor:'#fff',
        height: 50,
        borderWidth: 1,
        borderColor:'#fff',
        borderRadius:56,
        fontSize:14,
        fontFamily:'Pyidaungsu',
        color: '#000',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        paddingLeft: 40,
    },
    image: {
        width: '100%',
        height: 150,
        borderRadius: 10,
    },
    offlineContainer: {
        flex: 1,
        //backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
      },
      offlineText: { 
        marginTop:50,
        fontFamily: 'Pyidaungsu',
        fontSize : 15
      },
      tryOfflineText: {
        marginTop:10,
        fontFamily: 'Pyidaungsu',
        fontSize : 15,
        color: '#1987FB'
      }
})

export default connect(mapstateToProps,mapDispatchToProps)((Dashboard));

