import React, { useState,useEffect } from 'react'
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Platform, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView, Image } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import ErrorMessage from '../Components/ErrorMessage';
import PasswordShowHide from '../Components/PasswordShowHide';
import * as ImagePicker from 'expo-image-picker';
import mime from "mime";
import { showAlert } from 'react-native-customisable-alert';
import * as Notifications from 'expo-notifications';
import { isDevice } from 'expo-device';
import { connect } from 'react-redux';

const device = Dimensions.get('window');

const Register = (props) => {
  
  const { colors } = useTheme();
  const [t] = useTranslation('common');
  const [shopName,setShopName] = useState('');
  const [phoneNumber,setPhoneNumber] = useState('');
  const [address,setAddress] = useState('');
  const [password,setPassword] = useState('');

  const [firebaseToken, setFirebaseToken] = useState('');

  const [shopNameFocused,setShopNameFocused] = useState(false);
  const [phoneNumberFocused,setPhoneNumberFocused] = useState(false);
  const [addressFocused,setAddressFocused] = useState(false);
  const [passwordFocused,setPasswordFocused] = useState(false);
  const [secureTextEntry,setSecureTextEntry] = useState(true);

  const [formErrors, setFormErrors] = useState({});
  const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);

  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(null);

  let phoneNumberRef = React.createRef();
  let addressRef = React.createRef();
  let passwordRef = React.createRef();

  const toggleSecureTextEntry = () => {
    setSecureTextEntry(prevState => !prevState);
  };

  const clearFormFields = () => {
    setImage('');
    setShopName('');
    setPhoneNumber('');
    setAddress('');
    setPassword('');
  }

  const generateRandomString = () => {
    return Math.random().toString(36).substring(2, 8);
  }

  const requestRegisterOTP = async () => {

    setLoading(true); 
    let retry = true; 
    setFormErrors({});

    let formdata = new FormData();
    formdata.append('phone_number',phoneNumber);
    formdata.append('shop_name',shopName);
    // formdata.append('address',address);
    formdata.append('password',password);
    image && formdata.append('image', {type: mime.getType(image), uri:image, name: `${generateRandomString()}.${mime.getType(image).split("/")[1]}`});

    try {
      const response = await ApiService(url.register, formdata, 'POST');

      if (response.code == '200') {
        if (response.is_otp_enable) {
          clearFormFields();
          props.navigation.navigate('VerifyOtp', {
            registerUserData: formdata,
            token: response.token,
          });
        } else {
          formdata.append('otp','000000');
          formdata.append('token',response.token);
          formdata.append('firebase_token', firebaseToken);

          try {
            const response = await ApiService(url.register_verify, formdata, 'POST');

            if (response.code == '200') {
              props.navigation.replace('Success',{
                'image' : require('../../assets/images/account_confirm.png'),
                'title' : t('register_success_title'),
                'btnTitle' : t('back_btn'),
                'description' : t('account_wait_for_approve'),
                'token' : response.token,
                'login' : true
              })
            }else if (response.code == '422') {
              setFormErrors(response.errors);
            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false); 
          }



        }

 
       

       
      }else if (response.code == '422') {
        setFormErrors(response.errors);
      }else if (response.code == '401' || response.code == '400') {
        showAlert({
          title: `Oops!`,
          message: response.message,
          alertType: 'error',
          dismissable: true,
        });
      }
    } catch (error) {
      if (retry) {
          retry = false; 
          await requestRegisterOTP();
      }else{
          showAlert({
              title: `Oops!`,
              message: 'Netwok request failed.Please try again!',
              alertType: 'error',
              dismissable: true,
            });
      }
    } finally {
      setLoading(false); 
    }
  }

  const handleImagePicker = async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      console.log('Permission denied');
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      quality: 0.5
    });

    if (!result.canceled) {
        const { uri } = result.assets[0];
        setImage(uri);
    }
  };


  useEffect(() => {

    registerForPushNotificationsAsync().then((token) => setFirebaseToken(token));

  }, []);


  async function registerForPushNotificationsAsync() {
    let token;
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    if (isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
      token = (await Notifications.getDevicePushTokenAsync()).data;
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    return token;
  }

  useEffect(() => {
    setConfirmBtnDisble(!(shopName && phoneNumber && password));
  }, [shopName, phoneNumber, address, password]);    
  
  return (
    <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
        { loading && <Loading/> }
        <NavigationHeader  action={() => props.navigation.goBack()} />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{
            flexGrow: 1,
          }}>
        <View style={styles.container}>
            <View style={styles.body}>

            <HeaderLabel title={t('create_account')} />
            <View style={{marginTop:30}}/>

            <TouchableOpacity 
                onPress={() => handleImagePicker() }
                style={styles.imageContainer}>
                <View style={styles.iconBackground}>
                    {
                      image ?
                      <Image 
                        source={{ uri: image }}
                        resizeMode='contain'
                        style={{ width: 90, height: 90,borderRadius: 45}} />
                      : 
                      <Image 
                        tintColor='gray'
                        resizeMode='contain'
                        source={require('../../assets/images/default_upload.png')}
                        style={{ width: 90, height: 90}} />
                    }
                   
                </View>
            </TouchableOpacity>

            <View style={{marginTop:25}}/> 
            <InputLabel title={t('shop_name')} /> 
            <TextInput 
                keyboardType="default"
                maxLength={50}
                onChangeText={(value) => setShopName(value) }
                onFocus={() => setShopNameFocused(true)}
                onBlur={() => setShopNameFocused(false)}
                value={shopName}
                selectionColor={colors.inputActiveColor}
                onSubmitEditing={() =>  phoneNumberRef.current.focus()}
                style={[styles.input,{
                  borderBottomColor: shopNameFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                }]} 
              />

            {formErrors.shop_name && (
                <ErrorMessage message={formErrors.shop_name[0]} />
            )}
            <View style={{marginTop:25}}/> 
            
            <InputLabel title={t('phone_number')} /> 
            <TextInput 
                keyboardType="numeric"
                ref={phoneNumberRef}
                maxLength={50}
                onChangeText={(value) => setPhoneNumber(value) }
                onFocus={() => setPhoneNumberFocused(true)}
                onBlur={() => setPhoneNumberFocused(false)}
                value={phoneNumber}
                selectionColor={colors.inputActiveColor}
                onSubmitEditing={() =>  addressRef.current.focus()}
                style={[styles.input,{
                  borderBottomColor: phoneNumberFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                }]} 
              />

            {formErrors.phone_number && (
                <ErrorMessage message={formErrors.phone_number[0]} />
            )}

            {/* <View style={{marginTop:25}}/>  

            <InputLabel title={t('address')} />
            <TextInput 
                keyboardType="default"
                ref={addressRef}
                maxLength={90}
                onChangeText={(value) => setAddress(value) }
                onFocus={() => setAddressFocused(true)}
                onBlur={() => setAddressFocused(false)}
                value={address}
                selectionColor={colors.inputActiveColor}
                onSubmitEditing={() =>  passwordRef.current.focus()}
                style={[styles.input,{
                  borderBottomColor: addressFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                }]}
            />

            {formErrors.address && (
                <ErrorMessage message={formErrors.address[0]} />
            )} */}
            <View style={{marginTop:25}}/>    

            <InputLabel title={t('password')} />
            <View>
            <TextInput 
                keyboardType="default"
                ref={passwordRef}
                maxLength={50}
                onChangeText={(value) => setPassword(value) }
                onFocus={() => setPasswordFocused(true)}
                onBlur={() => setPasswordFocused(false)}
                value={password}
                secureTextEntry={secureTextEntry}
                selectionColor={colors.inputActiveColor}
                style={[styles.input,{
                  borderBottomColor: passwordFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                }]} 
              />
              {
                password != '' && 
                <PasswordShowHide
                      secureTextEntry={secureTextEntry}
                      toggleSecureTextEntry={toggleSecureTextEntry}
                      tintColor={colors.textColor}
                />
              }
            </View>

            {formErrors.password && (
                <ErrorMessage message={formErrors.password[0]} />
            )}

            <View style={{marginTop:25}}/>
              <CButton 
                  title={t('sign_up')} 
                  color={colors.mainLinkColor}
                  fontFamily={'Pyidaungsu-Bold'}
                  width={device.width - 32}
                  height={50}
                  disabled={confirmBtnDisble}
                  backgroundColor={'#F7EE25'}
                  action={() =>  requestRegisterOTP() }
                />
            </View>

            <View style={styles.footer}>
                <TouchableOpacity onPress={()=> props.navigation.navigate('Login') }>
                    <Text style={{fontSize:15,color:'gray',fontFamily:'Pyidaungsu'}}>
                        {t('already_have_account')}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width:device.width - 32,
    flexDirection: 'column', 
    flexGrow: 1,     
    justifyContent: 'space-between'
  },
  footer: {
    marginTop: 25,
    marginBottom: 25,
    alignSelf: 'center'
  },
  input: {
    height: 35,
    borderBottomWidth: 1,
    borderRadius:8,
    fontSize:16,
    fontFamily:'Pyidaungsu',
    color: '#000',
    borderColor:'#000'
  },
})

const mapstateToProps = state => {
  return {
      
  }
};
const mapDispatchToProps = dispatch => {
  return {
      setProfile: profile => {
        dispatch(setProfile(profile))
    },
  };
};

export default connect(mapstateToProps,mapDispatchToProps)((Register));
