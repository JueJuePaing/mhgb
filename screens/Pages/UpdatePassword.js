import React, { useState,useEffect } from 'react';
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import ErrorMessage from '../Components/ErrorMessage';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import { showAlert } from "react-native-customisable-alert";
import PasswordShowHide from '../Components/PasswordShowHide';

const device = Dimensions.get('window');

const UpdatePassword = (props) => {

    const { token,phone_number } = props.route.params;
    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [password,setPassword] = useState('');
    const [passwordFocused,setPasswordFocused] = useState(false);
    const [confirmPassword,setConfirmPassword] = useState('');
    const [confirmPasswordFocused,setConfirmPasswordFocused] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);
    const [secureTextEntry,setSecureTextEntry] = useState(true);
    const [confirmSecureTextEntry,setConfirmSecureTextEntry] = useState(true);

    let passwordRef = React.createRef();
    let confirmPasswordRef = React.createRef();

    useEffect(() => {
      setConfirmBtnDisble(!(password && confirmPassword));
    }, [password,confirmPassword]); 

    const toggleSecureTextEntry = () => {
      setSecureTextEntry(prevState => !prevState);
    };

    const toggleConfirmSecureTextEntry = () => {
      setConfirmSecureTextEntry(prevState => !prevState);
    };

    const requestUpdatePassword = async () => {
        setLoading(true); 
        setFormErrors({});
        
        let formdata = new FormData();
        formdata.append('phone_number',phone_number);
        formdata.append('password',password);
        formdata.append('password_confirmation',confirmPassword);
        formdata.append('token',token);

        try {
          const response = await ApiService(url.update_password, formdata, 'POST');
          
          if (response.code == '200') {
            showAlert({
              title: t('success'),
              message: t('update_password_successful'),
              alertType: 'success',
              dismissable: false,
              onPress: () => {
                props.navigation.reset({
                  index: 0,
                  routes: [{ name: 'Login' }],
              })
            }
            });
          }else if (response.code == '422') {
            setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false); 
        }
      }

    return(
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
            { loading && <Loading/> }
            <NavigationHeader skipBackScreens={2}/>
            <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                contentContainerStyle={{
                flexGrow: 1,
                }}>
                <View style={styles.container}>
                <HeaderLabel title={t('forgot_password')} />
                <View style={{marginTop:30}}/>
                <InputLabel title={t('password')} />
                <View>
                  <TextInput 
                      keyboardType="default"
                      ref={passwordRef}
                      maxLength={50}
                      onChangeText={(value) => setPassword(value) }
                      onFocus={() => setPasswordFocused(true)}
                      onBlur={() => setPasswordFocused(false)}
                      value={password}
                      secureTextEntry={secureTextEntry}
                      selectionColor={colors.inputActiveColor}
                      onSubmitEditing={() =>  confirmPasswordRef.current.focus()}
                      style={[styles.input,{
                        borderBottomColor: passwordFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                      }]} 
                    />
                    {
                      password != '' && 
                      <PasswordShowHide
                          secureTextEntry={secureTextEntry}
                          toggleSecureTextEntry={toggleSecureTextEntry}
                          tintColor={colors.textColor}
                      />
                    }
                </View>
                {formErrors.password && (
                    <ErrorMessage message={formErrors.password[0]} />
                )}

                <View style={{marginTop:25}}/>    
                <InputLabel title={t('confirmPassword')} />
                <View>
                <TextInput 
                    keyboardType="default"
                    ref={confirmPasswordRef}
                    maxLength={50}
                    onChangeText={(value) => setConfirmPassword(value) }
                    onFocus={() => setConfirmPasswordFocused(true)}
                    onBlur={() => setConfirmPasswordFocused(false)}
                    value={confirmPassword}
                    secureTextEntry={confirmSecureTextEntry}
                    selectionColor={colors.inputActiveColor}
                    style={[styles.input,{
                        borderBottomColor: confirmPasswordFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                    }]} 
                    />
                  {
                    confirmPassword != '' && 
                    <PasswordShowHide
                        secureTextEntry={confirmSecureTextEntry}
                        toggleSecureTextEntry={toggleConfirmSecureTextEntry}
                        tintColor={colors.textColor}
                    />
                  }
                </View>
                <View style={{marginTop:25}}/>
                  <CButton 
                      title={t('continue')} 
                      color={colors.mainLinkColor}
                      fontFamily={'Pyidaungsu-Bold'}
                      width={device.width - 32}
                      height={50}
                      backgroundColor={'#F7EE25'}
                      disabled={confirmBtnDisble}
                      action={() => requestUpdatePassword() }
                  />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:16,
        fontFamily:'Pyidaungsu',
        color: '#000',
        borderColor:'#000'
    },
  })

const mapstateToProps = state => {
    return {
        
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {

    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((UpdatePassword));