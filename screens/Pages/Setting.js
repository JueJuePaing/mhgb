import React, { useState, useContext } from 'react';
import NavigationHeader from '../Components/NavigationHeader';
import {  Text, View, Dimensions,Image, TouchableOpacity, ScrollView } from 'react-native';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import GapItem from '../Components/GapItem';
import {
    AntDesign,
    FontAwesome,
    Octicons
} from '@expo/vector-icons';

import {
    setToken,
    setProfile,
    setCartCount,
    setNotiCount,
    setShopProducts,
    clearReceipts
} from '../../stores/actions';
import { connect } from 'react-redux';
import { showAlert,closeAlert } from "react-native-customisable-alert";
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Language from './new/language';
import { AppContext } from '../../context/AppContextProvider';

const device = Dimensions.get('window');
const width = Dimensions.get('window').width;

const Setting = (props) => {

    const {changeProfileData, profileData} = useContext(AppContext);


    const { colors } = useTheme();
    const [t] = useTranslation('common');

    const navigation = props.navigation;

    const [
        showLan,
        setShowLan
    ] = useState(false);
 
    const confirmLogout = async () => {
        try {
            const response = await ApiService(url.logout, [], 'POST');
            if (response.code == '200') {
                closeAlert();
                props.setToken(null);
                props.setProfile(null);
                changeProfileData(null);
                props.setNotiCount(null);
                props.setCartCount(null);
                props.setShopProducts(null);
                props.clearReceipts();
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }],
                });
            }else if (response.code == '422') {

            }else if (response.code == '401' || response.code == '400') {
              showAlert({
                title: `Oops!`,
                message: response.message,
                alertType: 'error',
                dismissable: true,
              });
            }
          } catch (error) {
            console.error(error);
          } finally {
        }
    }

    const logout = () => {
        showAlert({
              title: t('logout_title'),
              message: t('logout_confirm'),
              alertType: 'warning',
              onPress: () => confirmLogout()
        });
    }

  return (
   <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',paddingBottom:5}}>
        <NavigationHeader 
                    hideBackButton={true}
                    title={t('Setting')}  />
        <ScrollView showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1,
          }}>
            <View style={{width:device.width-36}}>
                <View style={{flexDirection:'row'}}>
                        <View>
                        {
                            profileData && profileData.logo ? 
                            <Image
                                resizeMode='contain'
                                source={{ uri: profileData.logo }}
                                style={{width:70,height:70,resizeMode:'cover',borderRadius:35,borderColor:'#fff',borderWidth:2}}/>
                            :
                            <Image 
                                resizeMode='contain'
                                source={require('../../assets/images/default_user.png')}
                                style={{width:70,height:70,resizeMode:'cover',borderRadius:35,borderColor:'#fff',borderWidth:2}}/>
                        }
                        </View>
                        <View style={{justifyContent:'center',marginLeft:15}}>
                            <Text style={{fontFamily:'Pyidaungsu',fontSize:16,color:colors.mainTextColor,width:device.width-150}}>
                                {profileData?.name}
                            </Text>
                            <Text style={{fontFamily:'Pyidaungsu',fontSize:13,color:'gray',paddingTop:5}}>
                                {t('app_welcome')}
                            </Text>
                        </View>
                </View>
                <View style={{marginTop:20}}/>

                <TouchableOpacity 
                    onPress={() => navigation.navigate('Home')}
                    style={{
                        alignSelf: 'center', 
                        width: width - 60, 
                        height: 40,
                        backgroundColor: '#F7EE25', 
                        borderRadius: 30, 
                        justifyContent: 'center', 
                        alignItems: 'center'
                    }}>
                    <Text style={{
                        fontSize: 14,
                        textAlign: 'center', textAlignVertical: 'center',fontFamily:'Lexend-Medium',
                        color : colors.mainLinkColor
                    }}>{t('store')}</Text>
                </TouchableOpacity>

                <View style={{marginTop:30}}/>


                <TouchableOpacity onPress={() => navigation.navigate('Profile') }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            {/* <Image source={require('../../assets/images/acc_info.png')} 
                                style={{width:26,height:26,resizeMode:'contain'}}/> */}
                            <AntDesign
                                name='infocirlceo'
                                size={20}
                                color='#424245'
                                style={{width : 23}} />
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('account_detail')}
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>



                <TouchableOpacity onPress={() => setShowLan(true)}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View style={{flexDirection:'row', alignItems: 'center'}}>
                             <FontAwesome
                                name='language'
                                size={18}
                                color='#424245'
                                style={{width : 23}} />
                        <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                            {t('language')}
                        </Text>
                        </View>
                        <Image source={require('../../assets/images/right-arrow.png')} 
                            style={{width:20,height:20}}
                            tintColor={colors.mainTextColor}/>
                    </View>
                    <GapItem/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('RequestItem') }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            <AntDesign
                                name='questioncircleo'
                                size={20}
                                color='#424245'
                                style={{width : 23}} />
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('request_item')} 
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('SaveOrder') }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            {/* <Image source={require('../../assets/images/history.png')} 
                                style={{width:26,height:26,resizeMode:'contain'}}/> */}
                            <Octicons
                                name='history'
                                size={19}
                                color='#424245' 
                                style={{width : 23}}/>
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('order_history')}
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={() => navigation.navigate('ReturnItem') }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            {/* <Image source={require('../../assets/images/setting.png')} 
                                style={{width:26,height:26,resizeMode:'contain'}}/> */}
                            <FontAwesome
                                name='exchange'
                                size={15}
                                color='#424245'
                                style={{width : 23}} />
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('return_item')} 
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('ProfitMargin') }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            <FontAwesome
                                name='dollar'
                                size={18}
                                color='#424245'
                                style={{width : 23}} />
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('profit_margin')}
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('LatestTransactions', {saved : true}) }>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <View style={{flexDirection:'row', alignItems: 'center'}}>
                        {/* <Image source={require('../../assets/images/acc_info.png')} 
                            style={{width:26,height:26,resizeMode:'contain'}}/> */}
                             <AntDesign
                                name='profile'
                                size={18}
                                color='#424245'
                                style={{width : 23}} />
                        <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                            {t('save_order')}
                        </Text>
                        </View>
                        <Image source={require('../../assets/images/right-arrow.png')} 
                            style={{width:20,height:20}}
                            tintColor={colors.mainTextColor}/>
                    </View>
                    <GapItem/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => logout() }>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row', alignItems: 'center'}}>
                            {/* <Image source={require('../../assets/images/setting.png')} 
                                style={{width:26,height:26,resizeMode:'contain'}}/> */}
                                 <AntDesign
                                name='logout'
                                size={18}
                                color='#424245'
                                style={{width : 23}} />
                            <Text style={{fontFamily:'Pyidaungsu',alignSelf:'center',marginLeft:15,fontSize:15,color:colors.secondaryTextColor}}>
                                {t('logout')}
                            </Text>
                            </View>
                            <Image source={require('../../assets/images/right-arrow.png')} 
                                style={{width:20,height:20}}
                                tintColor={colors.mainTextColor}/>
                        </View>
                        <GapItem/>
                </TouchableOpacity>

               

            </View>

            {showLan && <Language
                closeHandler={()=> setShowLan(false)} />}
        </ScrollView>
    </View>
  );
}

const mapstateToProps = state => {
    return {
    }
};
  
const mapDispatchToProps = dispatch => {
    return {
        setToken: token => {
            dispatch(setToken(token))
        },
        setProfile: data => {
          dispatch(setProfile(data))
        },
        setCartCount: data => {
            dispatch(setCartCount(data))
        },
          setNotiCount: data => {
            dispatch(setNotiCount(data))
          },
          setShopProducts: data => {
            dispatch(setShopProducts(data))
          },
          clearReceipts: () => {
            dispatch(clearReceipts())
          },
    };
};

export default connect(mapstateToProps,mapDispatchToProps)((Setting));
