import React, { useState,useEffect } from 'react';
import { connect } from 'react-redux';
import NavigationHeader from '../Components/NavigationHeader';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import CButton from '../Components/CButton';
import { useTheme } from '@react-navigation/native';
import {useTranslation} from "react-i18next";
import InputLabel from '../Components/InputLabel';
import HeaderLabel from '../Components/HeaderLabel';
import { setProfile, setToken } from '../../stores/actions';
import PasswordShowHide from '../Components/PasswordShowHide';
import ErrorMessage from '../Components/ErrorMessage';
import { ApiService } from '../../network/service';
import url from '../../network/url';
import Loading from '../Components/Loading';
import { showAlert } from "react-native-customisable-alert";

const device = Dimensions.get('window');

const ForgotPassword = (props) => {

    const { colors } = useTheme();
    const [t] = useTranslation('common');
    const [phoneNumber,setPhoneNumber] = useState('');
    const [phoneNumberFocused,setPhoneNumberFocused] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [confirmBtnDisble,setConfirmBtnDisble] = useState(true);

    useEffect(() => {
      setConfirmBtnDisble(!(phoneNumber));
    }, [phoneNumber]);

    const requestOTP = async () => {
        setLoading(true); 
        setFormErrors({});
    
        let formdata = new FormData();
        formdata.append('phone_number',phoneNumber);
        try {
          const response = await ApiService(url.forgot_password, formdata, 'POST');
          if (response.code == '200') {
            props.navigation.navigate('ForgotPasswordVerifyOtp',{
              phone_number : phoneNumber,
              token: response.token
            });


            // let formdata = new FormData();
            // formdata.append('otp','000000');
            // formdata.append('token', response.token);
            // formdata.append('phone_number', phoneNumber);

            // try {
            //   const response = await ApiService(url.forgot_password_otp_verify, formdata, 'POST');
            //   if (response.code == '200') {
            //     props.navigation.replace('UpdatePassword',{
            //       phone_number: phoneNumber,
            //       token : response.token
            //     });
            //   }else if (response.code == '422') {
            //     setFormErrors(response.errors);
            //   }else if (response.code == '401' || response.code == '400') {
            //     showAlert({
            //       title: `Oops!`,
            //       message: response.message,
            //       alertType: 'error',
            //       dismissable: true,
            //     });
            //   }
            // } catch (error) {
            //   console.error(error);
            // } finally {
            //   setLoading(false); 
            // }
            
          }else if (response.code == '422') {
            setFormErrors(response.errors);
          }else if (response.code == '401' || response.code == '400') {
            showAlert({
              title: `Oops!`,
              message: response.message,
              alertType: 'error',
              dismissable: true,
            });
          }
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false); 
        }
      }

    return(
        <View style={{flex:1,backgroundColor:'#fff',alignItems:'center'}}>
            { loading && <Loading/> }
            <NavigationHeader  action={() => props.navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}
                keyboardShouldPersistTaps='handled'
                contentContainerStyle={{
                flexGrow: 1,
                }}>
                <View style={styles.container}>
                    <HeaderLabel title={t('forgot_password')} />
                    <View style={{marginTop:30}}/>
                    <InputLabel title={t('phone_number')} /> 
                    <TextInput 
                        keyboardType="default"
                        maxLength={50}
                        onChangeText={(value) => setPhoneNumber(value) }
                        value={phoneNumber}
                        selectionColor={colors.inputActiveColor}
                        style={[styles.input,{
                        borderBottomColor: phoneNumberFocused ? colors.inputActiveColor : 'rgba(9, 15, 71, 0.1)'
                        }]} 
                    />
                    {formErrors.phone_number && (
                        <ErrorMessage message={formErrors.phone_number[0]} />
                    )}
                </View>
                <View style={{marginTop:25}}/>
                  <CButton 
                      title={t('continue')} 
                      color={colors.mainLinkColor}
                      fontFamily={'Pyidaungsu-Bold'}
                      width={device.width - 32}
                      height={50}
                      backgroundColor={'#F7EE25'}
                      disabled={confirmBtnDisble}
                      action={() => requestOTP() }
                  />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width:device.width - 32
    },
    input: {
        height: 35,
        borderBottomWidth: 1,
        borderRadius:8,
        fontSize:16,
        fontFamily:'Pyidaungsu',
        color: '#000',
        borderColor:'#000'
    },
  })

const mapstateToProps = state => {
    return {
        
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {

    };
  };
  
  export default connect(mapstateToProps,mapDispatchToProps)((ForgotPassword));