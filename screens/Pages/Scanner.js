import React, { useState, useEffect } from 'react';
import { Text, Image, View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import {useTranslation} from "react-i18next";
import { ApiService } from '../../network/service';
import url from '../../network/url';
import { showAlert } from 'react-native-customisable-alert';
import { Ionicons } from '@expo/vector-icons';
import { ResourceStore, t } from 'i18next';

const device = Dimensions.get('window');

export default function Scanner(props) {
  const [t] = useTranslation('common');
  const [hasPermission, setHasPermission] = useState(null);
  const [loading,setLoading] = useState(false);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    
    const getBarCodeScannerPermissions = async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    };

    getBarCodeScannerPermissions();
  }, []);

  const reScan = () => {
      props.navigation.goBack();
  }
  const stockList = async (orderId) => {
    setScanned(true);
    setLoading(true);
    let formdata = new FormData();
    formdata.append('order_id',orderId);

    try {
        const response = await ApiService(url.stock_list, formdata, 'POST');
        if (response.code == '200') {          
          props.navigation.replace('AddStock', {scanProducts : response.products});
        }else{

          setScanned(true);
          showAlert({
            title: `Oops!`,
              message: 'Please scan correct QR code',
              alertType: 'error',
              dismissable: true,
              onPress: () => reScan() 
          });
        }
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false); 
    }
  }

  const handleBarCodeScanned = ({ type, data }) => {
    stockList(data);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View  style={styles.container2}>

      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.closeButton}
        onPress={() => props.navigation.goBack()}>
        <Ionicons   
          name='arrow-back'
          size={25}
          color='#fff' />

      </TouchableOpacity>

      <View style={{backgroundColor: 'transparent', height: 10000, flex: 1}} />

      <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={styles.cameraContainer}
          barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
        />

        <View
          style={{position: 'absolute', left: 0, right: 0, top: 0, bottom: 0}}>
          <View
            style={{
              backgroundColor: '#000',
              opacity: 0.5,
            }}></View>
          <View
            style={{
              height: device.height / 2.5, //3.5
              backgroundColor: '#000',
              opacity: 0.5,
            }}
          />
           {/* <View
            style={{
              height: device.height / 2.97, //3.5
              backgroundColor: '#000',
              opacity: 0.5,
              position: 'absolute',
              bottom: 0,
              width : device.width,
              zIndex: 5
            }}
          /> */}
          <View
            style={{
              backgroundColor: '#000',
              opacity: 0.5,
            }}></View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              // marginTop: hp(10),
            }}>
            <View
              style={{
                height: device.height * 0.3,
                width: device.width * 0.2,
                backgroundColor: '#000', // '#000',
                opacity: 0.5,
              }}
            />
            <View style={{height: device.height * 0.3, width: device.width * 0.6}}>
              <View style={{position: 'absolute', left: 0, top: 0}}>
                <View
                  style={{
                    height: 2,
                    width: 45,
                    backgroundColor: '#318AFF',
                  }}
                />
                <View
                  style={{height: 45, width: 2, backgroundColor: '#318AFF'}}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  right: 1,
                  top: -1,
                  transform: [{rotate: '90deg'}],
                }}>
                <View
                  style={{
                    height: 2,
                    width: 45,
                    backgroundColor: '#318AFF',
                  }}
                />
                <View
                  style={{height: 45, width: 2, backgroundColor: '#318AFF'}}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  left: 1,
                  bottom: -1,
                  transform: [{rotateZ: '-90deg'}],
                }}>
                <View
                  style={{
                    height: 2,
                    width: 45,
                    backgroundColor: '#318AFF',
                  }}
                />
                <View
                  style={{height: 45, width: 2, backgroundColor: '#318AFF'}}
                />
              </View>
              <View
                style={{
                  position: 'absolute',
                  right: 0,
                  bottom: 0,
                  transform: [{rotateZ: '180deg'}],
                }}>
                <View
                  style={{
                    height: 2,
                    width: 45,
                    backgroundColor: '#318AFF',
                  }}
                />
                <View
                  style={{height: 45, width: 2, backgroundColor: '#318AFF'}}
                />
              </View>
              {/* <Animated.View
                style={{
                  width: wp(53),
                  alignSelf: 'center',
                  height: 1,
                  borderRadius: hp(50),
                  marginTop: 5,
                  backgroundColor: '#5EA4FF',
                  transform: [
                    {
                      translateY: top.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 210],
                      }),
                    },
                  ],
                }}></Animated.View> */}
            </View>
            <View
              style={{
                height: device.height * 0.3,
                width: device.width * 0.2,
                backgroundColor: '#000', // '#000',
                opacity: 0.5,
              }}
            />
          </View>

          <Text style={styles.scanTxt}>
            {t('scan_qr')}
          </Text>
          <View
            style={{
              flex: 1,
              backgroundColor: 'black',
              opacity: 0.5,
              alignItems: 'center',
            }}>
            </View>
        </View>
    </View>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#262625',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scanner: {
    width: device.width * 0.8,
    height: device.height * 0.43,
    width: '100%', // Set width to 100% to fill its parent (barcodeContainer)
    height: '100%', 
    // flex: 1
    // width: device.width * 0.8,
    // height: device.height * 0.43,
  },
  barcodeContainer: {
    width: device.width * 0.8,
    height: device.height * 0.43,
    overflow: 'hidden',
    borderRadius: 10,
    position: 'absolute', // relative
    // backgroundColor: 'red',
    alignItems: 'center',
  justifyContent: 'center'  
},
  closeButton: {
    position: 'absolute',
    top: 50,
    left: 10,
    padding: 10,
    borderColor: '#fff',
    zIndex: 5
  },
  closeButton2: {
    position: 'absolute',
    top: 50,
    left: 10,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  closeButtonText: {
    color: '#333',
    fontFamily: 'Lexend'
  },
  scanTxt: {
    marginTop : device.height * 0.75,
    fontFamily: 'Lexend-Medium',
    fontSize : 16,
    color: 'red',
    position: 'absolute',
    alignSelf: 'center',
    color: '#F7EE25'
  },


  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footerContent: {
    flexDirection: 'row',
    width: device.width,
    marginTop: device.height * 0.185,
    justifyContent: 'space-around',
    // backgroundColor: 'green',
  },
  // cameraContainer: {
  //   height: device.height,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  topContainer: {
    color: '#000',
    marginTop: device.height * 0.8,
  },
  buttonTouchable: {
    width: device.width,
    justifyContent: 'center',
    paddingVertical: device.height * 0.05,
    alignItems: 'center',
    backgroundColor: 'green',
  },
  cameraContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width:device.width,
    height: device.height,
    // position: 'absolute',
    backgroundColor: 'transparent',
  },
  left: {
    position: 'absolute',
    marginTop: device.height * 0.06,
    paddingLeft: device.height * 0.02
  },

  qrCodeContent: {
    backgroundColor: '#000',
    width: device.height * 0.06,
    height: device.height * 0.06,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: device.height * 0.5,
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: device.height * 0.0155,
    color: '#fff',
    marginTop: 5
  },
  title: {
    color: '#FFF',
    fontFamily: 'Poppins-Regular',
    fontSize: device.height * 0.022,
    marginTop: device.height * 0.02
  },
  scanTitleContainer: {
    width: device.width,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: device.height * 0.1,
    position: 'absolute',
  },
  scanImageLogo: {
    width: device.height * 0.07,
    height: device.height * 0.07,
  },
});
