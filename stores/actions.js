export const BEARER_TOKEN = 'BEARER_TOKEN';
export const FIREBASE_TOKEN = 'FIREBASE_TOKEN';
export const PROFILE = 'PROFILE';
export const CART_COUNT = 'CART_COUNT';
export const NOTI_COUNT = 'NOTI_COUNT';
export const SHOP_PRODUCTS = 'SHOP_PRODUCTS';
export const TAX_PERCENT = 'TAX_PERCENT';
export const ORDER_RECEIPT = 'ORDER_RECEIPT';
export const REMOVE_RECEIPT = 'REMOVE_RECEIPT';
export const CLEAR_RECEIPT = 'CLEAR_RECEIPT';
export const DECREASE_PRODUCT_REMAIN_QTY = 'DECREASE_PRODUCT_REMAIN_QTY';
export const UPDATE_RECEIPT = 'UPDATE_RECEIPT';

export function setToken(token) {
    return { type: BEARER_TOKEN, token }
}
export function setFirebaseToken(firebase_token) {
    return { type: FIREBASE_TOKEN, firebase_token }
}
export function setProfile(profile) {
    return { type: PROFILE, profile }
}
export function setCartCount(cart_count) {
    return { type: CART_COUNT, cart_count }
}
export function setNotiCount(noti_count) {
    return { type: NOTI_COUNT, noti_count }
}
export function setShopProducts(products) {
    return { type: SHOP_PRODUCTS, products }
}
export function setTaxPercent(taxPercent) {
    return { type: TAX_PERCENT, taxPercent }
}
export function addOrderReceipt(receipt) {
    return { type: ORDER_RECEIPT, receipt }
}
export function removeOrderReceipt(id) {
    return { type: REMOVE_RECEIPT, id }
}
export function clearReceipts() {
    return { type: CLEAR_RECEIPT }
}
export function updateReceipts(receipts) {
    return { type: UPDATE_RECEIPT, receipts }
}
export function decreaseProductRemainQty(products) {
    return { type : DECREASE_PRODUCT_REMAIN_QTY, products }
}
