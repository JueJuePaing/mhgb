import {
    BEARER_TOKEN,
    FIREBASE_TOKEN,
    PROFILE,
    CART_COUNT,
    NOTI_COUNT,
    SHOP_PRODUCTS,
    TAX_PERCENT,
    ORDER_RECEIPT,
    REMOVE_RECEIPT,
    CLEAR_RECEIPT,
    DECREASE_PRODUCT_REMAIN_QTY,
    UPDATE_RECEIPT
  } from './actions';
  
  const initialState = {
    token : null,
    products : [],
    taxPercent : 0.001,
    receipts : []
  }
  
  export function App(state = initialState, action) {
    switch (action.type) {
      case BEARER_TOKEN: 
        return Object.assign({}, state, { token: action.token });
      case FIREBASE_TOKEN: 
        return Object.assign({}, state, { token: action.token });
      case PROFILE: 
        return Object.assign({}, state, { profile: action.profile });
      case CART_COUNT: 
        return Object.assign({}, state, { cart_count: action.cart_count });
      case NOTI_COUNT: 
        return Object.assign({}, state, { noti_count: action.noti_count });
      case SHOP_PRODUCTS: 
        return Object.assign({}, state, { products: action.products });

      case DECREASE_PRODUCT_REMAIN_QTY:
        const data2QuantityMap = {};
        action.products?.forEach(item => {
          data2QuantityMap[item.id] = item.count ? item.count : item.quantity;
        });
      
        const updatedData = state.products.map(item => {
          const updatedItem = { ...item };

          if (data2QuantityMap[item.id]) {
            updatedItem.remain_quantity -= data2QuantityMap[item.id];
          }
        
          return updatedItem;
        });

        return Object.assign({}, state, { products: updatedData });
   
      case TAX_PERCENT: 
        return Object.assign({}, state, { taxPercent: action.taxPercent });
      case ORDER_RECEIPT: 
        const newReceipt = {
          ...action.receipt,
          id: state.receipts.length + 1,
        };

        // Create a new array by spreading the current receipts array and adding the new receipt
        return Object.assign({}, state, { receipts: [...state.receipts, newReceipt] });
      case REMOVE_RECEIPT:
        const updatedReceipts = state.receipts.filter(receipt => receipt.id !== action.id);
        return Object.assign({}, state, { receipts: updatedReceipts });
      case CLEAR_RECEIPT: 
        return Object.assign({}, state, { receipts: [] });
      case UPDATE_RECEIPT: 
        return Object.assign({}, state, { receipts: action.receipts });
      default:
        return state
    }
  }
  